package net.minecraft.src;

import java.lang.reflect.Array;
import java.util.ArrayDeque;
import net.minecraft.block.state.IBlockState;

public class CacheObjectArray
{
    private static ArrayDeque<int[]> arrays = new ArrayDeque<int[]>();
    private static int maxCacheSize = 10;

    private static synchronized int[] allocateArray(int p_allocateArray_0_)
    {
        int[] aint = arrays.pollLast();

        if (aint == null || aint.length < p_allocateArray_0_)
        {
            aint = new int[p_allocateArray_0_];
        }

        return aint;
    }

    public static synchronized void freeArray(int[] p_freeArray_0_)
    {
        if (arrays.size() < maxCacheSize)
        {
            arrays.add(p_freeArray_0_);
        }
    }

}
