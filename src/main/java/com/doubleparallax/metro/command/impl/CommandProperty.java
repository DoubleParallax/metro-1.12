package com.doubleparallax.metro.command.impl;

import com.doubleparallax.metro.command.Command;
import com.doubleparallax.metro.command.exception.CommandException;
import com.doubleparallax.metro.property.Property;
import com.doubleparallax.metro.property.PropertyHandler;
import com.doubleparallax.metro.property.impl.Module;
import com.doubleparallax.metro.property.impl.Settings;
import com.doubleparallax.metro.util.ChatHelper;
import com.doubleparallax.metro.util.ClassHelper;

import java.util.Arrays;
import java.util.Map;

public class CommandProperty extends Command<CommandProperty> {
    
    private Property property;
    
    public CommandProperty(Property property) {
        this.property = property;
        instantiate(property.getId(), property.getName(), property.getDesc(), property.getAlias(), null, this);
    }
    
    @Override
    public void exec(CommandProperty command, Map<String, String> args) throws CommandException {
        if (args.isEmpty() && property instanceof Module) {
            Module module = (Module) property;
            ChatHelper.sendMessage(String.format("%s %s", module.getName(), (module.enabled = !module.enabled) ? "enabled" : "disabled"));
            return;
        }
        for (Map.Entry<String, String> entry : args.entrySet()) {
            String key = entry.getKey();
            
            boolean questionable = key.endsWith(PropertyHandler.get(Settings.class).consoleViewValue);
            if (questionable) {
                key = key.substring(0, key.length() - 1);
                
                if (key.length() == 0) {
                    ChatHelper.sendMessage("");
                    ChatHelper.sendMessage(String.format("%s Options", property.getName()));
                    for (Property property : property.getGlobalChildren().values())
                        ChatHelper.sendMessage(Arrays.deepToString(property.getAlias()));
                    ChatHelper.sendMessage("");
                    continue;
                }
            }
            
            Property child = property.getChild(key);
            if (child == null) {
                ChatHelper.sendMessage("Unknown command");
                continue;
            }
            Object value = child.getFieldValue();
            if (entry.getValue() == null || value == null) {
                boolean isBool = ClassHelper.isRecursive(child.getField().getType(), true, Boolean.class, Boolean.TYPE);
                if (questionable || !isBool)
                    ChatHelper.sendMessage(value == null ? String.format("%s is a container property", child.getName()) : String.format("%s: %s", child.getName(), value));
                else if (value != null) {
                    child.setFieldValue(!((Boolean) value));
                    ChatHelper.sendMessage(String.format("%s set to %s", child.getName(), child.getFieldValue()));
                }
                continue;
            }
            value = ClassHelper.parseTo(child.getField().getType(), value, entry.getValue());
            if (value == null) {
                ChatHelper.sendMessage(String.format("Error with new value %s, returning to old value", entry.getValue()));
                continue;
            }
            child.setFieldValue(value);
            ChatHelper.sendMessage(String.format("%s set to %s", child.getName(), value));
        }
    }
}
