package com.doubleparallax.metro.command.impl;

import com.doubleparallax.metro.authenticate.account.Account;
import com.doubleparallax.metro.command.Command;
import com.doubleparallax.metro.command.exception.CommandException;
import com.doubleparallax.metro.imixin.IMinecraftMixin;
import com.doubleparallax.metro.util.ChatHelper;

import java.util.Map;

public class CommandLogin extends Command<CommandLogin> {
    
    public CommandLogin() {
        instantiate("login", this);
    }
    
    @Override
    public void exec(CommandLogin command, Map<String, String> args) throws CommandException {
        String username, password;
        username = password = null;
        for (Map.Entry<String, String> entry : args.entrySet()) {
            if (isArgument("username", entry.getKey())) {
                username = entry.getValue();
            } else if (isArgument("password", entry.getKey())) {
                password = entry.getValue();
            }
        }
        ChatHelper.sendMessage(username == null ? "Please enter a username" : password == null ? String.format("Setting username to %s", username) : String.format("Logging into %s", username));
        Account account = new Account().setLogin(username).setPassword(password);
        account.authenticate(e -> ((IMinecraftMixin) mc).setSession(account.getStoredSession().getSession()));
    }
}
