package com.doubleparallax.metro.command;

import com.doubleparallax.metro.command.exception.CommandException;

import java.util.Map;

public interface IExec<T extends Command> {
    
    void exec(T command, Map<String, String> args) throws CommandException;
    
}
