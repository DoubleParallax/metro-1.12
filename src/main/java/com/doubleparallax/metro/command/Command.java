package com.doubleparallax.metro.command;

import com.doubleparallax.metro.command.bean.CommandBean;
import com.doubleparallax.metro.command.exception.CommandException;
import com.doubleparallax.metro.util.Helper;
import com.doubleparallax.metro.util.JsonHelper;
import com.doubleparallax.metro.util.io.FileHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class Command<T extends Command> extends Argument implements Helper, IExec<T> {
    
    private static final String DIRECTORY = "locale/command";
    
    protected List<Argument> arguments = new ArrayList<>();
    protected transient IExec<T> exec;
    
    public T instantiate(String id) {
        return instantiate(id, this);
    }
    
    public T instantiate(String id, IExec<T> exec) {
        this.id = id;
        
        String json = FileHelper.readResource(FileHelper.procRes("%s/%s.json", DIRECTORY, id));
        json = JsonHelper.compileJson(json, DIRECTORY);
        
        if (json == null)
            return cast();
        
        CommandBean bean;
        try {
            bean = JsonHelper.OBJECT_MAPPER.readValue(json, CommandBean.class);
            if (bean == null)
                throw new IOException();
            return instantiate(id, bean, exec);
        } catch (IOException e) {
            System.err.printf("Failed to load command locale for %s\n", id);
        }
        
        return cast();
    }
    
    private T instantiate(String id, CommandBean bean, IExec<T> exec) {
        return instantiate(id, bean.getName(), bean.getDesc(), bean.getAlias(), bean.getArguments().stream().map(Argument::new).collect(Collectors.toList()), exec);
    }
    
    public T instantiate(String id, String name, String desc, String[] alias, List<Argument> arguments, IExec<T> exec) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.alias = alias;
        this.arguments = arguments;
        this.exec = exec;
        return cast();
    }
    
    public String getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public String getDesc() {
        return desc;
    }
    
    public String[] getAlias() {
        return alias;
    }
    
    public List<Argument> getArguments() {
        return arguments;
    }
    
    public IExec getExec() {
        return exec;
    }
    
    public void exec(Map<String, String> arguments) throws CommandException {
        if (exec != null)
            exec.exec(cast(), arguments);
    }
    
    public boolean isArgument(String id, String in) {
        for (Argument argument : arguments)
            if (id == null || argument.id.equalsIgnoreCase(id))
                for (String alias : argument.alias)
                    if (alias.equalsIgnoreCase(in))
                        return true;
        return false;
    }
    
    private T cast() {
        return (T) this;
    }
    
}
