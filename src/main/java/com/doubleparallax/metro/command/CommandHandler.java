package com.doubleparallax.metro.command;

import com.doubleparallax.metro.Client;
import com.doubleparallax.metro.command.exception.CommandException;
import com.doubleparallax.metro.command.exception.UnknownCommandException;
import com.doubleparallax.metro.command.impl.CommandProperty;
import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.EventDescriptor;
import com.doubleparallax.metro.event.IEventListener;
import com.doubleparallax.metro.event.impl.EventChat;
import com.doubleparallax.metro.property.PropertyHandler;
import com.doubleparallax.metro.property.impl.Settings;
import com.doubleparallax.metro.util.ClassHelper;
import com.doubleparallax.metro.util.Helper;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@EventDescriptor(EventChat.class)
public enum CommandHandler implements Helper, IEventListener {
    INSTANCE;
    
    private static final Map<String, Command> COMMANDS = new HashMap<>();
    
    public void init() {
        register();
        
        new Reflections(Client.class.getPackage().getName().replaceAll("package ", ""))
                .getSubTypesOf(Command.class).stream()
                .filter(c -> !Modifier.isAbstract(c.getModifiers()) && !ClassHelper.isRecursive(c, CommandProperty.class))
                .forEach(c -> {
                    Command command;
                    try {
                        command = c.newInstance();
                    } catch (IllegalAccessException | InstantiationException e) {
                        System.err.printf("Failed to initialize command %s\n", c.getName());
                        return;
                    }
                    register(command);
                });
    }
    
    public void register(Command command) {
        COMMANDS.put(command.getId(), command);
    }
    
    @Override
    public void onEvent(Event event) {
        if (event instanceof EventChat && event.getCall() == EventChat.Call.SEND_PRE) {
            EventChat e = (EventChat) event;
            String message = e.getMessage();
            
            Settings settings = PropertyHandler.get(Settings.class);
            if (!settings.console)
                return;
            
            if (message.startsWith(settings.consoleNegate)) {
                e.setMessage(message.substring(settings.consoleNegate.length()));
            } else {
                if (message.startsWith(settings.consolePrefix)) {
                    String[] parsed = message.split("(?<=[^\\\\]);(?=(?:[^\"]*\"[^\"]*\")*[^\"]*)");
                    
                    for (String s : parsed) {
                        String commandString = (s.contains(" ") ? s.substring(1, s.indexOf(" ")) : s.substring(1)).toLowerCase();
                        Command command = COMMANDS.containsKey(commandString)
                                ? COMMANDS.get(commandString)
                                : COMMANDS.values().stream()
                                .filter(c -> c.getAlias() != null && Arrays.asList(c.getAlias()).contains(commandString))
                                .findFirst().orElse(null);
                        
                        try {
                            if (command != null) {
                                Map<String, String> arguments = new LinkedHashMap<>();
                                
                                List<String> tokens = new ArrayList<>();
                                Matcher regex = Pattern.compile("(\".*?[^\\\\\"]\"|[^ ]+)").matcher(s);
                                while (regex.find())
                                    tokens.add(regex.group(1));
                                
                                if (!tokens.isEmpty())
                                    tokens.remove(0);
                                
                                for (int i = 0; i < tokens.size(); i++) {
                                    String token = tokens.get(i);
                                    String next = i != tokens.size() - 1 ? tokens.get(i + 1) : null;
                                    boolean hasValue = !(next == null || next.startsWith(settings.consoleShortPrefix) || next.startsWith(settings.consoleLongPrefix));
                                    String value = hasValue ? tokens.get(i + 1).replace("\\\"", "\"").replaceAll("^\"|\"$", "") : null;
                                    if (token.startsWith(settings.consoleLongPrefix)) {
                                        arguments.put(token.substring(settings.consoleLongPrefix.length()), value);
                                        if (hasValue)
                                            i++;
                                    } else if (token.startsWith(settings.consoleShortPrefix)) {
                                        String[] args = token.substring(settings.consoleShortPrefix.length()).split("(?<=[^\\.])(?=[^\\.])");
                                        if (args.length == 0)
                                            continue;
                                        if (args.length != 1)
                                            for (String a : args)
                                                arguments.put(a, null);
                                        else
                                            arguments.put(args[0], value);
                                    }
                                }
                                
                                command.exec(arguments);
                            } else
                                throw new UnknownCommandException(commandString);
                        } catch (CommandException ex) {
                            System.err.printf("Error running command %s: %s\n", s, ex.getMessage());
                        }
                    }
                    
                    event.setCancelled(true);
                }
            }
        }
    }
    
}
