package com.doubleparallax.metro.command;

import com.doubleparallax.metro.command.bean.ArgumentBean;

public class Argument {
    
    protected String id, name, desc;
    protected String[] alias = new String[0];
    
    public Argument() {}
    
    public Argument(ArgumentBean bean) {
        id = bean.getId();
        name = bean.getName();
        desc = bean.getDesc();
        alias = bean.getAlias();
    }
    
}
