package com.doubleparallax.metro.command.bean;

public class ArgumentBean {
    
    private String id, name, desc;
    private String[] alias;
    
    public String getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public String getDesc() {
        return desc;
    }
    
    public String[] getAlias() {
        return alias;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public void setAlias(String[] alias) {
        this.alias = alias;
    }
    
}
