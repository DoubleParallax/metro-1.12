package com.doubleparallax.metro.command.bean;

import java.util.List;

public class CommandBean extends ArgumentBean {
    
    private List<ArgumentBean> arguments;
    
    public List<ArgumentBean> getArguments() {
        return arguments;
    }
    
    public void setArguments(List<ArgumentBean> arguments) {
        this.arguments = arguments;
    }
    
}
