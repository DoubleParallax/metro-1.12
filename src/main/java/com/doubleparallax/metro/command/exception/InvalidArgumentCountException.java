package com.doubleparallax.metro.command.exception;

public class InvalidArgumentCountException extends CommandException {
    
    public InvalidArgumentCountException(int givenArgs, int minArgs, int maxArgs) {
        super("Invalid argument count [%s], requires [%s] arguments", givenArgs, (minArgs != maxArgs ? maxArgs != -1 ? String.format("%s to %s", minArgs,
                maxArgs) : String.format("%s or more", minArgs) : maxArgs));
    }
    
}
