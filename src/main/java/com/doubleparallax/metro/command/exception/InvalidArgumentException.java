package com.doubleparallax.metro.command.exception;

public class InvalidArgumentException extends CommandException {
    
    public InvalidArgumentException(String requires, Object... args) {
        super("Invalid argument: %s", String.format(requires, args));
    }
    
}
