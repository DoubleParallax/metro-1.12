package com.doubleparallax.metro.command.exception;

public class UnknownCommandException extends CommandException {
    
    public UnknownCommandException(String in) {
        super("Cannot find the [%s] command", in);
    }
    
}
