package com.doubleparallax.metro.command.exception;

public class CommandException extends Exception {
    
    public CommandException(String message, Object... args) {
        super(String.format(message, args));
    }
    
}
