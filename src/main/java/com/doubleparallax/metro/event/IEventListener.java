package com.doubleparallax.metro.event;

public interface IEventListener {
    
    default void register() {
        EventHandler.register(this);
    }
    
    default void deregister() {
        EventHandler.deregister(this);
    }
    
    default void onEvent(Event event) {}
}
