package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;

public abstract class AbstractEventInput<T extends AbstractEventInput> extends Event<T, Event.Call> {
    
    protected int key;
    protected boolean state;
    
    public T setKey(int key) {
        this.key = key;
        return self;
    }
    
    public T setState(boolean state) {
        this.state = state;
        return self;
    }
    
    public int getKey() {
        return key;
    }
    
    public boolean getState() {
        return state;
    }
    
    public static class Keyboard extends AbstractEventInput<Keyboard> {
        private char character;
        
        public Keyboard setCharacter(char character) {
            this.character = character;
            return this;
        }
        
        public char getCharacter() {
            return character;
        }
    }
    
    public static class Mouse extends AbstractEventInput<Mouse> {
        private int x, y, dx, dy, wheel;
        
        @Override
        public Mouse setKey(int key) {
            super.setKey(key - 100);
            return this;
        }
        
        public Mouse setX(int x) {
            this.x = x;
            return this;
        }
        
        public Mouse setY(int y) {
            this.y = y;
            return this;
        }
        
        public Mouse setDX(int dx) {
            this.dx = dx;
            return this;
        }
        
        public Mouse setDY(int dy) {
            this.dy = dy;
            return this;
        }
        
        public Mouse setWheel(int wheel) {
            this.wheel = wheel;
            return this;
        }
        
        public int getX() {
            return x;
        }
        
        public int getY() {
            return y;
        }
        
        public int getDx() {
            return dx;
        }
        
        public int getDy() {
            return dy;
        }
        
        public int getWheel() {
            return wheel;
        }
    }
    
}
