package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;

public class EventWindowResize extends Event<EventWindowResize, Event.Call> {
    
    private int width, height;
    
    public int getWidth() {
        return width;
    }
    
    public int getHeight() {
        return height;
    }
    
    public EventWindowResize setWidth(int width) {
        this.width = width;
        return this;
    }
    
    public EventWindowResize setHeight(int height) {
        this.height = height;
        return this;
    }
    
}
