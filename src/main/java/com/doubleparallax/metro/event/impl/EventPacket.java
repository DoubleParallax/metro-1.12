package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;
import net.minecraft.network.Packet;

public class EventPacket extends Event<EventPacket, EventPacket.Call> {
    
    public enum Call {
        SEND_PRE, SEND_POST, RECEIVE_PRE, RECEIVE_POST
    }
    
    private Packet packet;
    
    public EventPacket setPacket(Packet packet) {
        this.packet = packet;
        return this;
    }
    
    public Packet getPacket() {
        return packet;
    }
    
}
