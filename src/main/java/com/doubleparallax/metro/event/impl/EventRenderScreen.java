package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;

public class EventRenderScreen extends Event<EventRenderScreen, EventRenderScreen.Call> {
    
    public enum Call {
        OVERLAY_PRE, OVERLAY_POST, GUI_PRE, GUI_POST, SCREEN_PRE, SCREEN_POST
    }
    
}
