package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;

public abstract class AbstractEventPlayer<T extends AbstractEventPlayer> extends Event<T, Event.Call> {
    
    protected EntityPlayerSP player;
    
    public T setPlayer(EntityPlayerSP player) {
        this.player = player;
        return self;
    }
    
    public EntityPlayerSP getPlayer() {
        return player;
    }
    
    public static class Update extends AbstractEventPlayer<Update> {
    }
    
    public static class Walking extends AbstractEventPlayer<Walking> {
    }
    
    public static class Mining extends AbstractEventPlayer<Mining> {
        private BlockPos pos;
        private EnumFacing facing;
        private boolean isMining;
        
        public Mining setPos(BlockPos pos) {
            this.pos = pos;
            return this;
        }
        
        public Mining setFacing(EnumFacing facing) {
            this.facing = facing;
            return this;
        }
        
        public Mining setMining(boolean isMining) {
            this.isMining = isMining;
            return this;
        }
        
        public BlockPos getPos() {
            return pos;
        }
        
        public EnumFacing getFacing() {
            return facing;
        }
        
        public boolean isMining() {
            return isMining;
        }
    }
    
    public static abstract class Block<T extends Block> extends AbstractEventPlayer<T> {
        public static class Break extends Block<Break> {
        }
        
        public static class Place extends Block<Place> {
            private EnumHand hand;
            
            public Place setHand(EnumHand hand) {
                this.hand = hand;
                return this;
            }
            
            public EnumHand getHand() {
                return hand;
            }
        }
    }
    
}
