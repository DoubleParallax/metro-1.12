package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;

public class EventTick extends Event<EventTick, EventTick.Call> {
    
    public enum Call {
        OUTER_PRE, OUTER_POST, INNER_PRE, INNER_POST
    }
    
}
