package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;

public class EventChat extends Event<EventChat, EventChat.Call> {
    
    public enum Call {
        SEND_PRE, SEND_POST, RECEIVE_PRE, RECEIVE_POST
    }
    
    private String message;
    
    public String getMessage() {
        return message;
    }
    
    public EventChat setMessage(String message) {
        this.message = message;
        return this;
    }
    
}
