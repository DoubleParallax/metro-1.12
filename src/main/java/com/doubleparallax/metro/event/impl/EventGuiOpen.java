package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;
import net.minecraft.client.gui.GuiScreen;

public class EventGuiOpen extends Event<EventGuiOpen, Event.Call> {
    
    private GuiScreen before, after;
    
    public GuiScreen getBefore() {
        return before;
    }
    
    public GuiScreen getAfter() {
        return after;
    }
    
    public EventGuiOpen setBefore(GuiScreen before) {
        this.before = before;
        return this;
    }
    
    public EventGuiOpen setAfter(GuiScreen after) {
        this.after = after;
        return this;
    }
    
}
