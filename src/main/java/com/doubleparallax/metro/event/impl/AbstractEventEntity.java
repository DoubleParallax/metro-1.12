package com.doubleparallax.metro.event.impl;

import com.doubleparallax.metro.event.Event;
import net.minecraft.entity.Entity;

public abstract class AbstractEventEntity<T extends AbstractEventEntity> extends Event<T, Event.Call> {
    
    private Entity entity;
    
    public T setEntity(Entity entity) {
        this.entity = entity;
        return self;
    }
    
    public Entity getEntity() {
        return entity;
    }
    
    public static class NetVelocity extends AbstractEventEntity<NetVelocity> {
        private double x, y, z;

        public NetVelocity setX(double x) {
            this.x = x;
            return this;
        }

        public NetVelocity setY(double y) {
            this.y = y;
            return this;
        }

        public NetVelocity setZ(double z) {
            this.z = z;
            return this;
        }
        
        public double getX() {
            return x;
        }
        
        public double getY() {
            return y;
        }
        
        public double getZ() {
            return z;
        }
    }
    
}
