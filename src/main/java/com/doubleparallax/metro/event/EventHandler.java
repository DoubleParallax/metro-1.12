package com.doubleparallax.metro.event;

import com.doubleparallax.metro.event.impl.AbstractEventInput;
import com.doubleparallax.metro.event.impl.EventTick;
import com.doubleparallax.metro.property.impl.Module;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EventHandler {
    
    private static final List<IEventListener> LISTENERS = new ArrayList<>();
    
    public static void register(IEventListener listener) {
        LISTENERS.add(listener);
    }
    
    public static void deregister(IEventListener listener) {
        LISTENERS.remove(listener);
    }
    
    public static <T extends Event> T onEvent(T event) {
        LISTENERS.stream().filter(l -> {
            if ((event instanceof AbstractEventInput || event instanceof EventTick) && l instanceof Module)
                return true;
            if (l.getClass().isAnnotationPresent(EventDescriptor.class)) {
                EventDescriptor descriptor = l.getClass().getAnnotation(EventDescriptor.class);
                return descriptor.value().length == 0 || Arrays.asList(descriptor.value()).contains(event.getClass());
            }
            return false;
        }).forEach(l -> l.onEvent(event));
        return event;
    }
    
}
