package com.doubleparallax.metro.event;

public abstract class Event<T extends Event, U extends Enum> {
    
    public enum Call {
        PRE, POST
    }
    
    protected T self;
    private U call;
    private boolean cancelled;
    
    public Event() {
        self = (T) this;
    }
    
    public U getCall() {
        return call;
    }
    
    public boolean isCancelled() {
        return cancelled;
    }
    
    public T setCall(U call) {
        this.call = call;
        return setCancelled(false);
    }
    
    public T setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
        return self;
    }
    
    public T cancel() {
        return setCancelled(true);
    }
    
    public T fire() {
        return EventHandler.onEvent(self);
    }
}
