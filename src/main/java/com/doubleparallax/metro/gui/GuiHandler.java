package com.doubleparallax.metro.gui;

import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.EventDescriptor;
import com.doubleparallax.metro.event.IEventListener;
import com.doubleparallax.metro.event.impl.EventGuiOpen;
import com.doubleparallax.metro.event.impl.EventRenderScreen;
import com.doubleparallax.metro.event.impl.EventTick;
import com.doubleparallax.metro.event.impl.EventWindowResize;
import com.doubleparallax.metro.gui.ui.Component;
import com.doubleparallax.metro.gui.ui.UI;
import com.doubleparallax.metro.gui.ui.impl.deluge.UID;
import com.doubleparallax.metro.gui.ui.impl.metro.UIM;
import com.doubleparallax.metro.property.PropertyHandler;
import com.doubleparallax.metro.property.impl.module.Overhaul;
import com.doubleparallax.metro.util.Helper;
import com.doubleparallax.metro.util.TimeHelper;
import com.doubleparallax.metro.util.render.RenderHelper;
import com.doubleparallax.metro.util.render.TextureHelper;
import com.doubleparallax.metro.util.render.alert.AlertHandler;
import com.doubleparallax.metro.util.render.geometry.Rect;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@EventDescriptor({EventGuiOpen.class, EventRenderScreen.class, EventTick.class, EventWindowResize.class})
public enum GuiHandler implements Helper, IEventListener, TimeHelper.ITimer {
    INSTANCE;
    
    public enum ThemeOverride {
        METRO(UIM.class), DELUGE(UID.class);
        
        private Class<? extends UI> _class;
        
        ThemeOverride(Class<? extends UI> _class) {
            this._class = _class;
        }
        
        private final Map<Class<? extends GuiScreen>[], Class<? extends UI>> OVERRIDES = new HashMap<>();
        
        public void put(Class<? extends GuiScreen>[] classes, Class<? extends UI> ui) {
            OVERRIDES.put(classes, ui);
        }
        
        public static ThemeOverride get(Class<? extends UI> _class) {
            for (ThemeOverride theme : values())
                if (theme._class == _class)
                    return theme;
            return METRO;
        }
    }
    
    //private static final Map<Class<? extends GuiScreen>[], Class<? extends UI>> OVERRIDES = new HashMap<>();
    private static final List<UI> UIS = new CopyOnWriteArrayList<>();
    private static UI next;
    
    public void init() {
        List<Class<? extends UI>> uiClasses = Arrays.stream(ThemeOverride.values()).map(t -> t._class).collect(Collectors.toList());
        
        for (Class<? extends UI> ui : uiClasses)
            new Reflections(ui.getPackage().getName().replaceAll("package ", "")).getSubTypesOf(ui).stream()
                    .filter(c -> !Modifier.isAbstract(c.getModifiers()) && c.isAnnotationPresent(GuiDescriptor.class))
                    .forEach(c -> {
                        GuiDescriptor descriptor = c.getAnnotation(GuiDescriptor.class);
                        ThemeOverride.get(c).put(descriptor.value(), c);
                    });
        
        register();
        TimeHelper.register(this);
    }
    
    public boolean hasUI() {
        return !UIS.isEmpty();
    }
    
    public <T extends UI> T getUI(int index) {
        return (T) UIS.get(index);
    }
    
    public void removeUI(UI ui) {
        UIS.remove(ui);
    }
    
    public int getUIIndex(UI ui) {
        return UIS.indexOf(ui);
    }
    
    public boolean containsOverride(ThemeOverride theme, GuiScreen gui) {
        for (Class<? extends GuiScreen>[] classes : theme.OVERRIDES.keySet())
            if (Arrays.asList(classes).contains(gui.getClass()))
                return true;
        return false;
    }
    
    public Class<? extends GuiScreen>[] getOverride(ThemeOverride theme, GuiScreen gui) {
        for (Class<? extends GuiScreen>[] classes : theme.OVERRIDES.keySet())
            if (Arrays.asList(classes).contains(gui.getClass()))
                return classes;
        return null;
    }
    
    public void openUI(UI ui) {
        GuiDescriptor anno = ui.getClass().getAnnotation(GuiDescriptor.class);
        if (!anno.popup())
            UIS.forEach(UI::close);
        ui.init(Display.getWidth(), Display.getHeight());
        if (!hasUI() || anno.popup()) {
            UIS.add(0, ui);
        } else
            next = ui;
    }

    @Override
    public void _step() {
        UIS.forEach(UI::_step);
        if (!hasUI() && next != null) {
            next.getVisible().setInterpolated(0);
            UIS.add(next);
            next = null;
        }
        AlertHandler._step();
    }
    
    @Override
    public void onEvent(Event event) {
        if (event instanceof EventGuiOpen) {
            EventGuiOpen e = (EventGuiOpen) event;
            switch (e.getCall()) {
                case POST:
                    GuiScreen gui = e.getAfter();
                    ThemeOverride theme = PropertyHandler.get(Overhaul.class).theme;
                    if (gui != null && containsOverride(theme, gui)) {
                        Class<? extends UI> uiClass = theme.OVERRIDES.get(getOverride(theme, gui));
                        try {
                            int openIndex = -1;
                            for (UI ui : UIS)
                                if (ui.getGuiParent() == gui) {
                                    openIndex = UIS.indexOf(ui);
                                    break;
                                }
                            if (openIndex == -1) {
                                UI ui = uiClass.newInstance().setGuiCurrent(gui).setGuiParent(e.getBefore());
                                openUI(ui);
                            } else {
                                int size = UIS.size() - openIndex;
                                for (int i = 0; i < size; i++) {
                                    UIS.get(0).close();
                                }
                            }
                            return;
                        } catch (InstantiationException | IllegalAccessException e1) {
                            e1.printStackTrace();
                        }
                    }
                    UIS.forEach(UI::close);
                    next = null;
                    break;
            }
        } else if (event instanceof EventRenderScreen) {
            EventRenderScreen e = (EventRenderScreen) event;
            switch (e.getCall()) {
                case GUI_PRE:
                    if (renderCustomGui())
                        e.cancel();
                    break;
                case SCREEN_POST:
                    RenderHelper.preRender();
                    
                    if (renderCustomGui()) {
                        if (mc.world == null)
                            renderBackground();
                        
                        for (int i = UIS.size() - 1; i >= 0; i--) {
                            UI ui = getUI(i);
                            GuiDescriptor descriptor = ui.getClass().getAnnotation(GuiDescriptor.class);
                            
                            if (i < UIS.size())
                                Rect.draw_Xy2_Col(0, 0, Display.getWidth(), Display.getHeight(), 0x80000000, .3 * ui.getVisible().getValue());
                            
                            GlStateManager.pushMatrix();
                            RenderHelper.preRender();
                            
                            double shift = 1 + ((1 - ui.getVisible().getValue()) * .1);
                            GlStateManager.translate(Display.getWidth() / 2., Display.getHeight() / 2., 0);
                            GlStateManager.scale(shift, shift, shift);
                            GlStateManager.translate(-Display.getWidth() / 2., -Display.getHeight() / 2., 0);
                            
                            if (descriptor != null && descriptor.bordered()) {
                                double minX, minY, maxX, maxY, padding;
                                minX = Display.getWidth();
                                minY = Display.getHeight();
                                maxX = maxY = 0;
                                padding = 10;
                                for (Component component : ui.getChildren()) {
                                    minX = Math.min(component.getRelX() - padding, minX);
                                    minY = Math.min(component.getRelY() - padding, minY);
                                    maxX = Math.max(component.getRelX() + component.getRelW() + padding, maxX);
                                    maxY = Math.max(component.getRelY() + component.getRelH() + padding, maxY);
                                }
                                Rect.draw_Xy2_Col(minX, minY, maxX, maxY, 0xEE405060, ui.getVisible().getValue());
                                Rect.draw_Xy2_Col_Mod(minX, minY, maxX, maxY, 0xFF607080, GL11.GL_LINE_LOOP, ui.getVisible().getValue());
                            }
                            
                            ui.render(ui.getVisible().getValue());
                            
                            RenderHelper.postRender();
                            GlStateManager.popMatrix();
                        }
                    }
                    
                    AlertHandler.render(1);
                    
                    RenderHelper.postRender();
                    
                    break;
            }
        } else if (event instanceof EventTick) {
            EventTick e = (EventTick) event;
            switch (e.getCall()) {
                case OUTER_PRE:
                    if (renderCustomGui())
                        UIS.forEach(UI::update);
                    break;
            }
        } else if (event instanceof EventWindowResize) {
            EventWindowResize e = (EventWindowResize) event;
            switch (e.getCall()) {
                case POST:
                    UIS.forEach(u -> u.init(e.getWidth(), e.getHeight()));
                    if (next != null)
                        next.init(e.getWidth(), e.getHeight());
                    break;
            }
        }
    }
    
    private boolean renderCustomGui() {
        return hasUI();
    }
    
    private void renderBackground() {
        float imageWidth = 1920;
        float imageHeight = 1080;
        
        float ratio = Math.min(imageWidth / Display.getWidth(), imageHeight / Display.getHeight());
        
        float width = (imageWidth / ratio) / 2.f;
        float height = (imageHeight / ratio) / 2.f;
        
        float x = Display.getWidth() / 2.f;
        float y = Display.getHeight() / 2.f;
        
        Rect.draw_Res_Xy2(TextureHelper.Textures.BACKGROUND.getResource(), x - width, y - height, x + width, y + height, 1);
    }
    
}
