package com.doubleparallax.metro.gui.ui.action;

public class KeyboardEventArgs implements EventArgs {
    
    private boolean state;
    private int key;
    private char character;
    
    public KeyboardEventArgs(boolean state, int key, char character) {
        this.state = state;
        this.key = key;
        this.character = character;
    }
    
    public boolean getState() {
        return state;
    }
    
    public int getKey() {
        return key;
    }
    
    public char getCharacter() {
        return character;
    }
    
}
