package com.doubleparallax.metro.gui.ui.impl.metro;

import com.doubleparallax.metro.Client;
import com.doubleparallax.metro.authenticate.account.AccountHandler;
import com.doubleparallax.metro.gui.GuiDescriptor;
import com.doubleparallax.metro.gui.ui.action.MouseEventArgs;
import com.doubleparallax.metro.gui.ui.components.metro.ImageView;
import com.doubleparallax.metro.gui.ui.components.metro.Scrollable;
import com.doubleparallax.metro.gui.ui.components.metro.TextView;
import com.doubleparallax.metro.util.URLHelper;
import com.doubleparallax.metro.util.render.TextureHelper;
import com.doubleparallax.metro.util.render.alert.AlertHelper;
import com.doubleparallax.metro.util.render.font.DFont;
import com.doubleparallax.metro.util.render.font.FontHelper;
import com.doubleparallax.metro.util.render.font.Icon;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiWorldSelection;
import org.lwjgl.opengl.Display;

@GuiDescriptor(value = {GuiMainMenu.class}, bordered = false)
public class UIMMainMenu extends UIM {

    @Override
    public void init(int width, int height) {
        super.init(width, height);

        Button[] buttons = {
                new Button(Icon.get("singleplayer").toString(), () -> mc.displayGuiScreen(new GuiWorldSelection(getGuiCurrent()))),
                new Button(Icon.get("multiplayer").toString(), () -> mc.displayGuiScreen(new GuiMultiplayer(getGuiCurrent()))),
                new Button(Icon.get("realms").toString(), () -> {
                }),
                new Button(Icon.get("accounts").toString(), () -> {
                }),
                new Button(Icon.get("options").toString(), () -> mc.displayGuiScreen(new GuiOptions(getGuiCurrent(), mc.gameSettings))),
                new Button(Icon.get("quit").toString(), mc::shutdown)
        };

        boolean smaller = Math.min(Display.getWidth(), Display.getHeight()) < 790;
        int small = smaller ? 64 : 128;
        int large = small * 2;

        DFont iconFont = (smaller ? FontHelper.Fonts.ICON_24 : FontHelper.Fonts.ICON_48).getFont();

        double pad = smaller ? 8 : 16;
        double pad2 = pad * 2;

        Scrollable buttonScrollable = new Scrollable(this, pad, pad, small, Display.getHeight() - pad2)
                .setDirection(Scrollable.Direction.VERTICAL)
                .setOutline(0x40000000, 0x40000000, 2)
                .setBackground(0x40000000, 0x40000000);

        for (int i = 0; i < buttons.length; i++) {
            Button b = buttons[i];
            new TextView(buttonScrollable, 0, i * small, small, small) {
                @Override
                public void onMouseDown(MouseEventArgs args) {
                    if (isInteractable() && args.getButton() == 0)
                        b.run();
                }
            }.setFont(iconFont)
                    .setLabel(b.getLabel())
                    .setHAlign(DFont.Horizontal.CENTER)
                    .setVAlign(DFont.Vertical.MIDDLE)
                    .setShadow(true)
                    .setOutline(0x004080FF, 0x804080FF, 2)
                    .setBackground(0x004080FF, 0x804080FF)
                    .setForeground(0x80FFFFFF, -1);
        }

        Scrollable accountScrollable = new Scrollable(this, Display.getWidth() - pad - small, pad, small, Display.getHeight() - pad2)
                .setDirection(Scrollable.Direction.VERTICAL)
                .setOutline(0x40000000, 0x40000000, 2)
                .setBackground(0x40000000, 0x40000000);

        AccountHandler.getAccountList().forEach(a -> {
            int index = AccountHandler.getAccountList().indexOf(a);
            new ImageView(accountScrollable, 0, index * small, small, small) {
                @Override
                public void onMouseDown(MouseEventArgs args) {
                    if (isInteractable() && args.getButton() == 0)
                        a.authenticate(e -> {});
                }
            }.setResource((smaller ? TextureHelper.Textures.DEFAULT_HEAD_128 : TextureHelper.Textures.DEFAULT_HEAD_256).getResource())
                    .setTexture((a.getUsername().isEmpty() ? null : String.format("https://visage.surgeplay.com/head/%s/%s", large - 32, a.getDisplay())))
                    .setOutline(0x004080FF, 0x804080FF, 2)
                    .setBackground(0x004080FF, 0x804080FF)
                    .setForeground(0x80FFFFFF, -1);
        });

        new ImageView(this, (Display.getWidth() / 2.) - small, (Display.getHeight() / 2.) - small, large, large) {
            @Override
            public void onMouseDown(MouseEventArgs args) {
                if (isInteractable() && args.getButton() == 0)
                    if (!URLHelper.openBrowser("https://doubleparallax.com"))
                        AlertHelper.warning("Failed to open %s home page", Client.INSTANCE.PROJECT);
            }
        }.setResource((smaller ? TextureHelper.Textures.LOGO_128 : TextureHelper.Textures.LOGO_256).getResource())
                .setForeground(0x80FFFFFF, -1);
    }
    
    /*@Override
    public void onAction(String index, Component component) throws InteractionCancellationException {
        super.onAction(index, component);
        
        if (component == null)
            return;
        
        int[] indices = component.getIndices();
        if (component instanceof TextView && indices.length == 5 && indices[1] == 0) {
            switch (indices[3]) {
                case 0:
                    mc.displayGuiScreen(new GuiWorldSelection(getGuiCurrent()));
                    break;
                case 1:
                    mc.displayGuiScreen(new GuiMultiplayer(getGuiCurrent()));
                    break;
                case 2:
                    //mc.displayGuiScreen(new GuiScreenRealmsProxy(new RealmsScreen()));
                    break;
                case 3:
                    //mc.displayGuiScreen(new GuiAccounts());
                    break;
                case 4:
                    mc.displayGuiScreen(new GuiOptions(getGuiCurrent(), mc.gameSettings));
                    break;
                case 5:
                    mc.shutdown();
                    break;
            }
        } else if (component instanceof ImageView && indices.length == 5 && indices[1] == 1) {
            AccountHandler.get(indices[3]).authenticate(e -> {
                if (e)
                    ;//TooltipHelper.add(component, "Successfully logged in", component.getX() + (component.getW() / 2.), component.getY() - 4, TooltipHelper.Orientation.TOP);
            });
        }
    }*/
}
