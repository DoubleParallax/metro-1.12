package com.doubleparallax.metro.gui.ui.components.metro;

import com.doubleparallax.metro.gui.ui.Component;
import com.doubleparallax.metro.gui.ui.components.AbstractTextView;
import com.doubleparallax.metro.util.render.ColorHelper;
import com.doubleparallax.metro.util.render.RenderHelper;

public class TextView extends AbstractTextView {

    public TextView(Component parent, double x, double y, double w, double h) {
        super(parent, x, y, w, h);
    }
    
    @Override
    public void render(double alpha) {
        renderDefault(alpha);

        double x = hAlign.getAlign(getRelX(), getRelW());
        double y = vAlign.getAlign(getRelY(), getRelH());
        
        RenderHelper.preRender();
        int c = ColorHelper.blend(this.foreground[0], this.foreground[1], (float) getHovered().getValue());
        font.drawString(label, x, y, ColorHelper.relativeAlpha(c, alpha), shadow, hAlign, vAlign);
        RenderHelper.postRender();
        
        super.render(alpha);
    }
    
}
