package com.doubleparallax.metro.gui.ui.components.metro;

import com.doubleparallax.metro.gui.exception.InteractionCancellationException;
import com.doubleparallax.metro.gui.ui.Component;
import com.doubleparallax.metro.gui.ui.action.MouseEventArgs;
import com.doubleparallax.metro.util.render.ClippingHelper;
import com.doubleparallax.metro.util.render.geometry.Rect;
import net.minecraft.util.math.MathHelper;

public class Scrollable extends Component {
    
    public enum Direction {
        HORIZONTAL, VERTICAL
    }
    
    protected Direction direction;
    protected double scroll;
    
    public Scrollable(Component parent, double x, double y, double w, double h) {
        super(parent, x, y, w, h);
    }
    
    public Scrollable setDirection(Direction direction) {
        this.direction = direction;
        return this;
    }
    
    public Scrollable setScroll(double scroll) {
        this.scroll = scroll;
        return this;
    }
    
    @Override
    public void update() {
        limitScroll();
        
        for (Component child : CHILDREN) {
            switch (direction) {
                case HORIZONTAL:
                    child.setX(child.getRelIX() + scroll);
                    break;
                case VERTICAL:
                    child.setY(child.getRelIY() + scroll);
                    break;
            }
        }
        
        super.update();
    }
    
    @Override
    public void mouse(MouseEventArgs args) throws InteractionCancellationException {
        super.mouse(args);
        
        if (isHovered() && args.getWheel() != 0) {
            scroll += args.getWheel() * (direction == Direction.HORIZONTAL ? getAverageWidth() : getAverageHeight());
            limitScroll();
        }
    }
    
    @Override
    public void render(double alpha) {
        renderDefault(alpha);

        ClippingHelper.Depth.start();
        ClippingHelper.Depth.mask();
        Rect.draw_Xy2_Col(getRelX(), getRelY(), getRelX() + getRelW(), getRelY() + getRelH(), background[0], 0.5);
        ClippingHelper.Depth.render();
    
        CHILDREN.stream().filter(c ->
                c.getRelX() < getRelX() + getRelW() &&
                c.getRelY() < getRelY() + getRelH() &&
                c.getRelX() + c.getRelW() > getRelX() &&
                c.getRelY() + c.getRelH() > getRelY()
        ).forEach(c -> c.render(alpha));
        
        ClippingHelper.Depth.end();
    }
    
    private double getAverageWidth() {
        double w = 0;
        for (Component child : CHILDREN)
            w += child.getRelW();
        return w / CHILDREN.size();
    }
    
    private double getAverageHeight() {
        double h = 0;
        for (Component child : CHILDREN)
            h += child.getRelH();
        return h / CHILDREN.size();
    }
    
    private void limitScroll() {
        Double begin, end;
        begin = end = null;
        
        for (Component child : CHILDREN) {
            double a = direction == Direction.HORIZONTAL ? child.getRelX() : child.getRelY();
            double b = a + (direction == Direction.HORIZONTAL ? child.getRelW() : child.getRelH());
            
            if (begin == null || a < begin)
                begin = a;
            if (end == null || b > end)
                end = b;
        }
        
        if (begin == null)
            scroll = 0;
        else {
            double a = end - begin;
            double b = direction == Direction.HORIZONTAL ? getRelW() : getRelH();
            scroll = a > b ? MathHelper.clamp(scroll, -(a - b), 0) : 0;
        }
    }
    
}
