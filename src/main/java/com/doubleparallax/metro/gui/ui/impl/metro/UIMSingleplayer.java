package com.doubleparallax.metro.gui.ui.impl.metro;

import com.doubleparallax.metro.gui.GuiDescriptor;
import com.doubleparallax.metro.gui.ui.action.MouseEventArgs;
import com.doubleparallax.metro.gui.ui.components.metro.Scrollable;
import com.doubleparallax.metro.gui.ui.components.metro.TextView;
import com.doubleparallax.metro.gui.ui.components.metro.listview.ListViewItemWorld;
import com.doubleparallax.metro.imixin.IGuiScreenMixin;
import com.doubleparallax.metro.mixin.accessor.IGuiWorldSelectionAccessor;
import com.doubleparallax.metro.util.render.ColorHelper;
import com.doubleparallax.metro.util.render.font.DFont;
import com.doubleparallax.metro.util.render.font.FontHelper;
import com.doubleparallax.metro.util.render.font.Icon;
import net.minecraft.client.AnvilConverterException;
import net.minecraft.client.gui.GuiCreateWorld;
import net.minecraft.client.gui.GuiErrorScreen;
import net.minecraft.client.gui.GuiListWorldSelectionEntry;
import net.minecraft.client.gui.GuiWorldSelection;
import net.minecraft.client.resources.I18n;
import net.minecraft.world.storage.ISaveFormat;
import net.minecraft.world.storage.WorldSummary;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@GuiDescriptor({GuiWorldSelection.class})
public class UIMSingleplayer extends UIM {

    private Scrollable scrollable;

    @Override
    public void init(int width, int height) {
        super.init(width, height);

        double w = 300 + getPadding(2);
        double h = 40 + getPadding(2);

        double listH = 340;

        double startX = (Display.getWidth() / 2.) - w;
        double startY = (Display.getHeight() / 2.) - ((listH + h) / 2.);

        DFont font = FontHelper.Fonts.CALIBRI_16.getFont();
        DFont iconFont = FontHelper.Fonts.ICON_14.getFont();

        scrollable = new Scrollable(this, startX + getPadding(), startY + getPadding(), (w * 2) - getPadding(2), listH - getPadding(2))
                .setDirection(Scrollable.Direction.VERTICAL)
                .setOutline(0x40000000, 0x40000000, 2)
                .setBackground(0x40000000, 0x40000000);

        List<GuiListWorldSelectionEntry> worlds = getWorlds();
        Icon.Icons[] worldOptions = {
                Icon.Icons.CLOSE,
                Icon.Icons.DUPLICATE,
                Icon.Icons.PENCIL,
                Icon.Icons.CHECKMARK
        };
        for (int i = 0; worlds != null && i < worlds.size(); i++) {
            ListViewItemWorld item = new ListViewItemWorld(scrollable, 0, 80 * i, scrollable.getRelW(), 80) {
                @Override
                public void onSubmit() {
                    getValue().joinWorld();
                }
            }.setValue(worlds.get(i))
                    .setForeground(0x004080FF, 0x804080FF);
            for (int j = 0; j < worldOptions.length; j++) {
                int index = j;
                Icon.Icons option = worldOptions[index];
                new TextView(item, item.getRelW() - 35 - (32 * index), 3, 32, item.getRelH() - 6) {
                    @Override
                    public void onMouseDown(MouseEventArgs args) {
                        if (isInteractable() && args.getButton() == 0)
                            switch (index) {
                                case 0:
                                    item.getValue().deleteWorld();
                                    break;
                                case 1:
                                    item.getValue().recreateWorld();
                                    break;
                                case 2:
                                    item.getValue().editWorld();
                                    break;
                                case 3:
                                    item.getValue().joinWorld();
                                    break;
                            }
                    }
                }.setFont(iconFont)
                        .setLabel(option.toString())
                        .setHAlign(DFont.Horizontal.CENTER)
                        .setVAlign(DFont.Vertical.MIDDLE)
                        .setForeground(0x80FFFFFF, 0x80202020)
                        .setBackground(ColorHelper.blend(0xFF4080FF, 0xFF202020, .5f), ColorHelper.blend(0xFF4080FF, 0xFFE0E0E0, .25f));
            }
        }

        Object[][] buttons = {
                {startX, startY + listH, w, h, new Button(I18n.format("selectWorld.create"), () -> mc.displayGuiScreen(new GuiCreateWorld(getGuiCurrent())))},
                {startX + w, startY + listH, w, h, new Button(I18n.format("gui.cancel"), () -> ((IGuiScreenMixin) getGuiCurrent()).onKeyTyped('\0', Keyboard.KEY_ESCAPE))},
        };

        for (Object[] btn : buttons) {
            double _x = (double) btn[0] + getPadding();
            double _y = (double) btn[1] + getPadding();
            double _w = (double) btn[2] - getPadding(2);
            double _h = (double) btn[3] - getPadding(2);

            Button button = (Button) btn[4];

            new TextView(this, _x, _y, _w, _h) {
                @Override
                public void onMouseDown(MouseEventArgs args) {
                    if (isInteractable() && args.getButton() == 0)
                        button.run();
                }
            }.setFont(font)
                    .setLabel(button.getLabel())
                    .setHAlign(DFont.Horizontal.CENTER)
                    .setVAlign(DFont.Vertical.MIDDLE)
                    .setShadow(true)
                    .setOutline(0x40000000, 0x804080FF, 2)
                    .setBackground(0x40000000, 0x804080FF)
                    .setForeground(0x80FFFFFF, -1);
        }
    }
    
    /*@Override
    public void onAction(String index, Component component) throws InteractionCancellationException {
        super.onAction(index, component);
        
        if (component == null)
            return;
        
        int[] indices = component.getIndices();
        if (component instanceof AbstractTextView) {
            switch (indices.length) {
                case 3:
                    ListViewItemWorld item = (ListViewItemWorld) scrollable.getChildren().stream().filter(i -> ((ListViewItem) i).isSelected()).findFirst().orElse(null);
                    switch (indices[1]) {
                        case 0:
                            if (item != null)
                                item.getValue().joinWorld();
                            break;
                        case 2:
                            if (item != null)
                                item.getValue().editWorld();
                            break;
                        case 3:
                            if (item != null)
                                item.getValue().deleteWorld();
                            break;
                        case 4:
                            if (item != null)
                                item.getValue().recreateWorld();
                            break;
                        case 1:
                        case 5:
                            List<GuiButton> buttonList = ((IGuiScreenMixin) getGuiCurrent()).getButtonList();
                            ((IGuiScreenMixin) getGuiCurrent()).onActionPerformed(buttonList.get(indices[1]));
                            break;
                    }
                    break;
            }
        } else if (component instanceof ListViewItemWorld && indices.length == 4) {
            ListViewItemWorld item = (ListViewItemWorld) component;
            item.getValue().joinWorld();
        }
    }*/

    private List<GuiListWorldSelectionEntry> getWorlds() {
        ISaveFormat isaveformat = this.mc.getSaveLoader();
        List<WorldSummary> worlds;

        try {
            worlds = isaveformat.getSaveList();
        } catch (AnvilConverterException e) {
            mc.displayGuiScreen(new GuiErrorScreen(I18n.format("selectWorld.unable_to_load"), e.getMessage()));
            return null;
        }

        Collections.sort(worlds);

        List<GuiListWorldSelectionEntry> list = new ArrayList<>();
        if (mc.currentScreen instanceof GuiWorldSelection)
            for (WorldSummary worldsummary : worlds)
                list.add(new GuiListWorldSelectionEntry(((IGuiWorldSelectionAccessor) mc.currentScreen).getSelectionList(), worldsummary, this.mc.getSaveLoader()));

        return list;
    }
}
