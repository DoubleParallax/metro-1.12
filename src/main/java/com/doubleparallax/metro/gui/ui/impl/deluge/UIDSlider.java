package com.doubleparallax.metro.gui.ui.impl.deluge;

import com.doubleparallax.metro.gui.GuiDescriptor;
import com.doubleparallax.metro.gui.ui.UI;
import net.minecraft.client.gui.GuiOptionSlider;

@GuiDescriptor(value = {}, bordered = false)
public class UIDSlider extends UID {
    
    private UI previous;
    private GuiOptionSlider slider;
    
    public UIDSlider(UI previous, GuiOptionSlider slider) {
        this.previous = previous;
        this.slider = slider;
    }
    
    @Override
    public void init(int width, int height) {
        super.init(width, height);
    
        /*float min = slider.options.getValueMin();
        float max = slider.options.getValueMax();
        float dif = max - min;
        
        double r = Math.min(Math.hypot(Display.getWidth(), Display.getHeight()) / 4., 512);
        RadialMenu menu = new RadialMenu(this, 0, Display.getWidth() / 2., Display.getHeight() / 2., r);
        Segment<Character> segment = new Segment<>(slider.displayString, Icon.get(String.format("options.%s", slider.options.ordinal())).getCharacter()).setClamp(0, (360. / dif) * (mc.gameSettings.getOptionFloatValue(slider.options) - min));
        addChild(menu.addSegment(segment.setMouseDragged(e -> {
            float dir = (float) MathHelper.wrapValue(MathHelper.getDirection(menu.getX(), menu.getY(), InputHelper.getX(), InputHelper.getY()), 0, 360);
            float val = slider.options.snapToStepClamp(((dif / 360.f) * dir) + min);
            segment.setClamp(0, (360. / dif) * (val - min));
            mc.gameSettings.setOptionFloatValue(slider.options, val);
            segment.setLabel(mc.gameSettings.getKeyBinding(slider.options));
        })));*/
    }
    
}
