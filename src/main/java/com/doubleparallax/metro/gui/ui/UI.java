package com.doubleparallax.metro.gui.ui;

import com.doubleparallax.metro.gui.GuiHandler;
import com.doubleparallax.metro.gui.exception.InteractionCancellationException;
import com.doubleparallax.metro.gui.ui.action.KeyboardEventArgs;
import com.doubleparallax.metro.gui.ui.action.MouseEventArgs;
import com.doubleparallax.metro.imixin.IGuiScreenMixin;
import com.doubleparallax.metro.util.Helper;
import net.minecraft.client.gui.GuiScreen;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

public abstract class UI<T extends GuiScreen, U extends GuiScreen, V extends UI> extends Component implements Helper {
    
    protected static final int ID_CLOSE = Keyboard.KEY_ESCAPE;
    protected static final String ID_SUBMIT = "submit";
    
    private T guiCurrent;
    private U guiParent;
    private V uiParent;
    
    public UI() {
        super(null, 0, 0, 0, 0);
        uiParent = !GuiHandler.INSTANCE.hasUI() ? null : GuiHandler.INSTANCE.getUI(0);
        setVisible(true);
        setHovered(true);
    }
    
    public T getGuiCurrent() {
        return guiCurrent;
    }
    
    public U getGuiParent() {
        return guiParent;
    }
    
    public V getUiParent() {
        return uiParent;
    }
    
    public UI setGuiCurrent(T guiCurrent) {
        this.guiCurrent = guiCurrent;
        return this;
    }
    
    public UI setGuiParent(U guiParent) {
        this.guiParent = guiParent;
        return this;
    }
    
    public void setUIParent(V uiParent) {
        this.uiParent = uiParent;
    }
    
    public void init(int width, int height) {
        CHILDREN.clear();
        setVisible(true);
        getVisible().setInterpolated(0);
    }
    
    public void close() {
        getVisible().setValue(false);
    }

    public abstract void finish();

    @Override
    public void onKeyDown(KeyboardEventArgs args) throws InteractionCancellationException {
        super.onKeyDown(args);
        if (args.getKey() == ID_CLOSE) {
            if (getGuiCurrent() != null)
                ((IGuiScreenMixin) getGuiCurrent()).onKeyTyped('\0', Keyboard.KEY_ESCAPE);
            else
                GuiHandler.INSTANCE.openUI(getUiParent());
            throw new InteractionCancellationException();
        } else if (args.getKey() == Keyboard.KEY_TAB)
            throw new InteractionCancellationException();
    }
    
    @Override
    public void update() {
        setHovered(GuiHandler.INSTANCE.getUIIndex(this) == 0 && isVisible());
        
        while (Mouse.next())
            try {
                mouse(new MouseEventArgs(Mouse.getEventButtonState(), Mouse.getEventButton(), Mouse.getEventX(), Mouse.getEventY(), Mouse.getEventDX(), Display.getHeight() - Mouse.getEventDY() - 1, Mouse.getEventDWheel() / 120));
            } catch (InteractionCancellationException e) {
            }
        
        while (Keyboard.next())
            try {
                keyboard(new KeyboardEventArgs(Keyboard.getEventKeyState(), Keyboard.getEventKey(), Keyboard.getEventCharacter()));
            } catch (InteractionCancellationException e) {
            }
        
        CHILDREN.forEach(Component::update);
    }
    
    @Override
    public void _step() {
        super._step();
        if (!isVisible() && getVisible().getValue() < .01)
            GuiHandler.INSTANCE.removeUI(this);
    }
    
}
