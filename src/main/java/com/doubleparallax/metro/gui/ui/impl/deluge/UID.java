package com.doubleparallax.metro.gui.ui.impl.deluge;

import com.doubleparallax.metro.gui.GuiHandler;
import com.doubleparallax.metro.gui.exception.InteractionCancellationException;
import com.doubleparallax.metro.gui.ui.UI;
import com.doubleparallax.metro.gui.ui.action.MouseEventArgs;

public abstract class UID extends UI {
    
    @Override
    public void mouse(MouseEventArgs args) throws InteractionCancellationException {
        if (args.getState() && args.getButton() == 1) {
            finish();
            throw new InteractionCancellationException();
        }
        super.mouse(args);
    }

    @Override
    public void finish() {
        GuiHandler.INSTANCE.openUI(getUiParent());
    }
    
}
