package com.doubleparallax.metro.gui.ui.components;

import com.doubleparallax.metro.gui.ui.Component;
import com.doubleparallax.metro.util.render.font.DFont;

public abstract class AbstractTextView extends Component {
    
    protected DFont font;
    protected String label;
    protected boolean shadow;
    protected DFont.Horizontal hAlign = DFont.Horizontal.LEFT;
    protected DFont.Vertical vAlign = DFont.Vertical.TOP;

    public AbstractTextView(Component parent, double x, double y, double w, double h) {
        super(parent, x, y, w, h);
    }

    public <T extends AbstractTextView> T setFont(DFont font) {
        this.font = font;
        return cast();
    }
    
    public <T extends AbstractTextView> T setLabel(String label) {
        this.label = label;
        return cast();
    }
    
    public <T extends AbstractTextView> T setShadow(boolean shadow) {
        this.shadow = shadow;
        return cast();
    }
    
    public <T extends AbstractTextView> T setHAlign(DFont.Horizontal hAlign) {
        this.hAlign = hAlign;
        return cast();
    }
    
    public <T extends AbstractTextView> T setVAlign(DFont.Vertical vAlign) {
        this.vAlign = vAlign;
        return cast();
    }
}
