package com.doubleparallax.metro.gui.ui.components.metro;

import com.doubleparallax.metro.gui.ui.Component;
import com.doubleparallax.metro.util.render.ColorHelper;
import com.doubleparallax.metro.util.render.RenderHelper;
import com.doubleparallax.metro.util.render.geometry.Rect;
import net.minecraft.util.ResourceLocation;

public class ImageView extends Component {
    
    private ResourceLocation resource;
    private String url;

    public ImageView(Component parent, double x, double y, double w, double h) {
        super(parent, x, y, w, h);
    }
    
    public <T extends ImageView> T setResource(ResourceLocation resource) {
        this.resource = resource;
        return cast();
    }
    
    public <T extends ImageView> T setTexture(String url) {
        this.url = url;
        return cast();
    }
    
    @Override
    public void render(double alpha) {
        renderDefault(alpha);

        RenderHelper.preRender();
        Rect.draw_Res_Url_Xy2_Col(resource, url, getRelX(), getRelY(), getRelX() + getRelW(), getRelY() + getRelH(), ColorHelper.blend(foreground[0], foreground[1], (float) getHovered().getValue()), alpha);
        RenderHelper.postRender();
        
        super.render(alpha);
    }
    
}
