package com.doubleparallax.metro.gui.ui.action;

public class MouseEventArgs implements EventArgs {
    
    private boolean state;
    private int button, x, y, dx, dy, wheel;
    
    public MouseEventArgs(boolean state, int button, int x, int y, int dx, int dy, int wheel) {
        this.state = state;
        this.button = button;
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
        this.wheel = wheel;
    }
    
    public boolean getState() {
        return state;
    }
    
    public int getButton() {
        return button;
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
    
    public int getDx() {
        return dx;
    }
    
    public int getDy() {
        return dy;
    }
    
    public int getWheel() {
        return wheel;
    }
    
}
