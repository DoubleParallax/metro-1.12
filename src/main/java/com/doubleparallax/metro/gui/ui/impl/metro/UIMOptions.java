package com.doubleparallax.metro.gui.ui.impl.metro;

import com.doubleparallax.metro.gui.GuiDescriptor;
import com.doubleparallax.metro.gui.exception.InteractionCancellationException;
import com.doubleparallax.metro.gui.ui.Component;
import com.doubleparallax.metro.gui.ui.action.MouseEventArgs;
import com.doubleparallax.metro.gui.ui.components.metro.Slider;
import com.doubleparallax.metro.gui.ui.components.metro.TextView;
import com.doubleparallax.metro.imixin.IGuiOptionSliderMixin;
import com.doubleparallax.metro.imixin.IGuiScreenMixin;
import com.doubleparallax.metro.imixin.IGuiScreenOptionsSoundsButtonMixin;
import com.doubleparallax.metro.mixin.accessor.IGuiOptionsRowListAccessor;
import com.doubleparallax.metro.mixin.accessor.IGuiOptionsRowListRowAccessor;
import com.doubleparallax.metro.mixin.accessor.IGuiVideoSettingsAccessor;
import com.doubleparallax.metro.util.render.font.DFont;
import com.doubleparallax.metro.util.render.font.FontHelper;
import net.minecraft.client.gui.*;
import org.lwjgl.opengl.Display;

import java.util.Arrays;
import java.util.List;

@GuiDescriptor(value = {GuiIngameMenu.class, GuiShareToLan.class, GuiOptions.class, GuiCustomizeSkin.class, GuiScreenOptionsSounds.class, GuiVideoSettings.class, ScreenChatOptions.class}, popup = true)
public class UIMOptions extends UIM {

    @Override
    public void init(int width, int height) {
        super.init(width, height);

        List<GuiButton> buttonList = ((IGuiScreenMixin) getGuiCurrent()).getButtonList();

        if (getGuiCurrent() instanceof GuiVideoSettings)
            ((IGuiOptionsRowListAccessor) ((IGuiVideoSettingsAccessor) getGuiCurrent()).getOptionsRowList()).getOptions().forEach((r) -> buttonList.addAll(Arrays.asList(((IGuiOptionsRowListRowAccessor) r).getButtonA(), ((IGuiOptionsRowListRowAccessor) r).getButtonB())));

        double padding = 2;
        double padding2 = padding * 2;

        double w = 250;
        double h = 40;

        double startX = (Display.getWidth() / 2.) - w;
        double startY = (Display.getHeight() / 2.) - (Math.ceil(buttonList.size() / 2.) * (h / 2.));

        DFont font = FontHelper.Fonts.CALIBRI_16.getFont();

        buttonList.forEach(b -> {
            int index = buttonList.indexOf(b);
            double x = startX + (w * (index % 2));
            double y = startY + (h * (Math.floor(index / 2.)));
            double _w = (index % 2 == 0 && index == buttonList.size() - 1) ? w * (2 - (index % 2)) : w;

            Component component = null;
            if (b instanceof GuiOptionSlider)
                component = new Slider(this, x + padding, y + padding, _w - padding2, h - padding2).setOutline(true).setOption(((IGuiOptionSliderMixin) b).getOptions()).setForeground(0x40000000, 0x804080FF);
            else if (b instanceof IGuiScreenOptionsSoundsButtonMixin)
                component = new Slider(this, x + padding, y + padding, _w - padding2, h - padding2).setOutline(true).setSound(((IGuiScreenOptionsSoundsButtonMixin) b).getSoundCategory()).setForeground(0x40000000, 0x804080FF);

            if (component != null && component instanceof Slider)
                new TextView(component, 0, 0, component.getRelW(), component.getRelH())
                        .setFont(font)
                        .setLabel(b.displayString)
                        .setHAlign(DFont.Horizontal.CENTER)
                        .setVAlign(DFont.Vertical.MIDDLE)
                        .setShadow(true)
                        .setForeground(0x80FFFFFF, -1);
            else
                new TextView(this, x + padding, y + padding, _w - padding2, h - padding2) {
                    @Override
                    public void onMouseDown(MouseEventArgs args) throws InteractionCancellationException {
                        if (isInteractable() && args.getButton() == 0) {
                            List<GuiButton> buttonList = ((IGuiScreenMixin) getGuiCurrent()).getButtonList();
                            GuiButton button = buttonList.get(index);
                            ((IGuiScreenMixin) getGuiCurrent()).onActionPerformed(button);
                            setLabel(button.displayString);
                        }
                    }
                }.setFont(font)
                        .setLabel(b.displayString)
                        .setHAlign(DFont.Horizontal.CENTER)
                        .setVAlign(DFont.Vertical.MIDDLE)
                        .setShadow(true)
                        .setOutline(0x40000000, 0x804080FF, 2)
                        .setBackground(0x40000000, 0x804080FF)
                        .setForeground(0x80FFFFFF, -1);
        });
    }
    
    /*@Override
    public void onAction(String index, Component component) throws InteractionCancellationException {
        super.onAction(index, component);
        
        if (component == null)
            return;
        
        int[] indices = component.getIndices();
        if (component instanceof AbstractTextView) {
            switch (indices.length) {
                case 3:
                    List<GuiButton> buttonList = ((IGuiScreenMixin) getGuiCurrent()).getButtonList();
                    GuiButton button = buttonList.get(indices[1]);
                    ((IGuiScreenMixin) getGuiCurrent()).onActionPerformed(button);
                    ((AbstractTextView) component).setLabel(button.displayString);
                    break;
            }
        }
    }*/
}