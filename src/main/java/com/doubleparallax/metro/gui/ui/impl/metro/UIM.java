package com.doubleparallax.metro.gui.ui.impl.metro;

import com.doubleparallax.metro.gui.GuiHandler;
import com.doubleparallax.metro.gui.ui.UI;

public abstract class UIM extends UI {

    protected class Button {
        private String label;
        private Runnable run;

        public Button(String label, Runnable run) {
            this.label = label;
            this.run = run;
        }

        public String getLabel() {
            return label;
        }

        public void run() {
            run.run();
        }
    }

    private static final double PADDING = 2;

    protected double getPadding() {
        return PADDING;
    }

    protected double getPadding(double multiplier) {
        return getPadding() * multiplier;
    }

    @Override
    public void finish() {
        GuiHandler.INSTANCE.openUI(getUiParent());
    }

}
