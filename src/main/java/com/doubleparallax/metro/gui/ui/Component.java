package com.doubleparallax.metro.gui.ui;

import com.doubleparallax.metro.gui.exception.InteractionCancellationException;
import com.doubleparallax.metro.gui.ui.action.KeyboardEventArgs;
import com.doubleparallax.metro.gui.ui.action.MouseEventArgs;
import com.doubleparallax.metro.util.Helper;
import com.doubleparallax.metro.util.InputHelper;
import com.doubleparallax.metro.util.MathHelper;
import com.doubleparallax.metro.util.TimeHelper;
import com.doubleparallax.metro.util.animation.Animation;
import com.doubleparallax.metro.util.render.ColorHelper;
import com.doubleparallax.metro.util.render.RenderHelper;
import com.doubleparallax.metro.util.render.geometry.Rect;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Component implements TimeHelper.ITimer, Helper {

    public enum Position {
        RELATIVE,
        ABSOLUTE;
    }

    protected Component parent;

    protected final List<Animation> ANIMATIONS = new ArrayList<>();
    protected final List<Component> CHILDREN = new CopyOnWriteArrayList<>();

    protected int[] foreground = {0, 0},
                    background = {0, 0},
                    outline = {0, 0, 0};

    protected double renderPadding;

    private Position position = Position.ABSOLUTE;
    private Animation
            x = new Animation().addTo(ANIMATIONS),
            y = new Animation().addTo(ANIMATIONS),
            w = new Animation().addTo(ANIMATIONS),
            h = new Animation().addTo(ANIMATIONS),
            iX = new Animation().addTo(ANIMATIONS),
            iY = new Animation().addTo(ANIMATIONS),
            enabled = new Animation(true).addTo(ANIMATIONS),
            visible = new Animation(true).addTo(ANIMATIONS),
            hovered = new Animation().addTo(ANIMATIONS),
            dragged = new Animation().addTo(ANIMATIONS);
    
    public Component(Component parent, double x, double y, double w, double h) {
        this.parent = parent;
        if (parent != null) {
            parent.getChildren().add(this);
            position = Position.RELATIVE;
        }
        this.x.reset(x);
        this.y.reset(y);
        this.w.reset(w);
        this.h.reset(h);

        iX.reset(x);
        iY.reset(y);
    }
    
    public Component getParent() {
        return parent;
    }
    
    public Position getPosition() {
        return position;
    }

    public Animation getX() {
        return x;
    }

    public Animation getY() {
        return y;
    }

    public Animation getW() {
        return w;
    }

    public Animation getH() {
        return h;
    }

    public double getRelX() {
        return (getPosition() == Position.ABSOLUTE ? 0 : getParent().getRelX()) + x.getValue();
    }
    
    public double getRelY() {
        return (getPosition() == Position.ABSOLUTE ? 0 : getParent().getRelY()) + y.getValue();
    }
    
    public double getRelW() {
        return w.getValue();
    }
    
    public double getRelH() {
        return h.getValue();
    }

    public double getRelIX() {
        return iX.getValue();
    }

    public double getRelIY() {
        return iY.getValue();
    }
    
    public Animation getEnabled() {
        return enabled;
    }
    
    public Animation getVisible() {
        return visible;
    }
    
    public Animation getHovered() {
        return hovered;
    }
    
    public boolean isEnabled() {
        return enabled.getValueB();
    }
    
    public boolean isVisible() {
        return visible.getValueB();
    }
    
    public boolean isHovered() {
        return hovered.getValueB();
    }

    public boolean isInteractable() {
        return isVisible() && isEnabled() && isHovered();
    }
    
    public List<Component> getChildren() {
        return CHILDREN;
    }

    public <T extends Component> T setX(double x) {
        this.x.setValue(x);
        return cast();
    }

    public <T extends Component> T setY(double y) {
        this.y.setValue(y);
        return cast();
    }

    public <T extends Component> T setW(double w) {
        this.w.setValue(w);
        return cast();
    }

    public <T extends Component> T seH(double h) {
        this.h.setValue(h);
        return cast();
    }

    public <T extends Component> T setForeground(int color1, int color2) {
        foreground[0] = color1;
        foreground[1] = color2;
        return cast();
    }

    public <T extends Component> T setBackground(int background1, int background2) {
        background[0] = background1;
        background[1] = background2;
        return cast();
    }

    public <T extends Component> T setOutline(int outline1, int outline2, int width) {
        outline[0] = outline1;
        outline[1] = outline2;
        outline[2] = width > 0 ? (int) MathHelper.clamp(width, 0, 10) : width;
        return cast();
    }

    public <T extends Component> T setRenderPadding(double renderPadding) {
        this.renderPadding = renderPadding;
        return cast();
    }
    
    public <T extends Component> T setEnabled(boolean enabled) {
        this.enabled.setValue(enabled);
        return cast();
    }
    
    public <T extends Component> T setVisible(boolean visible) {
        this.visible.setValue(visible);
        return cast();
    }
    
    public <T extends Component> T setHovered(boolean hovered) {
        if (this.hovered.getValueB() != hovered) {
            this.hovered.setValue(hovered);
            if (hovered)
                onMouseEnter();
            else
                onMouseLeave();
        }
        return cast();
    }

    protected <T extends Component> T cast() {
        return (T) this;
    }

    public void onMouseEnter() {
    }

    public void onMouseLeave() {
    }

    public void onMouseDown(MouseEventArgs args) throws InteractionCancellationException {
    }

    public void onMouseUp(MouseEventArgs args) throws InteractionCancellationException {
    }

    public void onMouseDragged(MouseEventArgs args) throws InteractionCancellationException {
    }

    public void onKeyDown(KeyboardEventArgs args) throws InteractionCancellationException {
    }

    public void onKeyUp(KeyboardEventArgs args) throws InteractionCancellationException {
    }

    public void onSubmit() throws InteractionCancellationException {}

    public void update() {
        setHovered((parent == null || parent.isHovered()) && InputHelper.isMouseInside(getRelX(), getRelY(), getRelX() + getRelW(), getRelY() + getRelH()));
        CHILDREN.forEach(Component::update);
    }
    
    public void mouse(MouseEventArgs args) throws InteractionCancellationException {
        for (Component child : CHILDREN)
            child.mouse(args);
        
        if (args.getState())
            onMouseDown(args);
        else if (args.getButton() != -1)
            onMouseUp(args);
        if (args.getButton() == 0)
            dragged.setValue(args.getState());
        if (dragged.getValueB())
            onMouseDragged(args);
    }
    
    public void keyboard(KeyboardEventArgs args) throws InteractionCancellationException {
        for (Component child : CHILDREN)
            child.keyboard(args);
        if (args.getState())
            onKeyDown(args);
        else
            onKeyUp(args);
    }

    protected void renderDefault(double alpha) {
        RenderHelper.preRender();

        double x1 = getRelX() + renderPadding;
        double y1 = getRelY() + renderPadding;
        double x2 = getRelX() + getRelW() - renderPadding;
        double y2 = getRelY() + getRelH() - renderPadding;

        float hovered = (float) getHovered().getValue();

        int background = ColorHelper.blend(this.background[0], this.background[1], hovered);
        int outline = ColorHelper.blend(this.outline[0], this.outline[1], hovered);

        Rect.draw_Xy2_Col(x1, y1, x2, y2, background, alpha);

        if (this.outline[2] > 0) {
            GlStateManager.glLineWidth(this.outline[2]);
            Rect.draw_Xy2_Col_Mod(x1, y1, x2, y2, outline, GL11.GL_LINE_LOOP, alpha);
        }

        RenderHelper.postRender();
    }
    
    public void render(double alpha) {
        CHILDREN.forEach(c -> c.render(alpha));
    }

    @Override
    public void _step() {
        ANIMATIONS.forEach(Animation::_step);
        CHILDREN.forEach(Component::_step);
    }

}
