package com.doubleparallax.metro.gui.ui.impl.deluge;

import com.doubleparallax.metro.gui.GuiDescriptor;
import com.doubleparallax.metro.gui.GuiHandler;
import com.doubleparallax.metro.gui.screen.GuiModules;
import com.doubleparallax.metro.gui.ui.components.deluge.RadialMenu;
import com.doubleparallax.metro.gui.ui.components.deluge.Segment;
import com.doubleparallax.metro.property.Property;
import com.doubleparallax.metro.property.impl.Module;
import com.doubleparallax.metro.util.render.font.Icon;
import org.lwjgl.opengl.Display;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

@GuiDescriptor(value = {GuiModules.class}, bordered = false)
public class UIDModules extends UID {
    
    @Override
    public void init(int width, int height) {
        super.init(width, height);
        
        double r = Math.min(Math.hypot(Display.getWidth(), Display.getHeight()) / 4., 512);
        
        RadialMenu menu = new RadialMenu(this, Display.getWidth() / 2., Display.getHeight() / 2., r);
        menu.addSegment(Arrays.stream(Module.Category.values())
                .map(p -> new Segment<>(p.toString(), Icon.get(String.format("module.category.%s", p.ordinal())).toString())
                        .setMouseDown(e -> e.getButton() == 0, e -> {
                            Collection<? extends Property> properties = p.getModules();
                            if (!properties.isEmpty())
                                GuiHandler.INSTANCE.openUI(new UIDProperties(properties));
                        })).collect(Collectors.toList()).toArray(new Segment[0])
        );
    }

    @Override
    public void finish() {
        mc.displayGuiScreen(null);
    }
    
}
