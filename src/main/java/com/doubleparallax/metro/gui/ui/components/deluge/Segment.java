package com.doubleparallax.metro.gui.ui.components.deluge;

import com.doubleparallax.metro.gui.ui.action.Action;
import com.doubleparallax.metro.gui.ui.action.EventArgs;
import com.doubleparallax.metro.gui.ui.action.KeyboardEventArgs;
import com.doubleparallax.metro.gui.ui.action.MouseEventArgs;

import java.util.function.Predicate;

public class Segment<T> {
    
    private Action<MouseEventArgs> mouseDown, mouseUp, mouseDragged;
    private Action<KeyboardEventArgs> keyDown, keyUp;
    
    private Predicate<MouseEventArgs> mousePredicate;
    private Predicate<KeyboardEventArgs> keyboardPredicate;
    
    private String label;
    private T value;
    private double[] clamp = {0, 360};
    
    public Segment(String label, T value) {
        this.label = label;
        this.value = value;
        
        mouseDown = mouseUp = mouseDragged = getDefaultMouseAction();
        keyDown = keyUp = getDefaultKeyboardAction();
    }
    
    public Action<? extends EventArgs> getMouseDown(MouseEventArgs args) {
        return mousePredicate == null || mousePredicate.test(args) ? mouseDown : getDefaultMouseAction();
    }
    
    public Action<? extends EventArgs> getMouseUp(MouseEventArgs args) {
        return mousePredicate == null || mousePredicate.test(args) ? mouseUp : getDefaultMouseAction();
    }
    
    public Action<? extends EventArgs> getMouseDragged(MouseEventArgs args) {
        return mousePredicate == null || mousePredicate.test(args) ? mouseDragged : getDefaultMouseAction();
    }
    
    public Action<? extends EventArgs> getKeyDown(KeyboardEventArgs args) {
        return keyboardPredicate == null || keyboardPredicate.test(args) ? keyDown : getDefaultKeyboardAction();
    }
    
    public Action<? extends EventArgs> getKeyUp(KeyboardEventArgs args) {
        return keyboardPredicate == null || keyboardPredicate.test(args) ? keyUp : getDefaultKeyboardAction();
    }
    
    public String getLabel() {
        return label;
    }
    
    public T getValue() {
        return value;
    }
    
    public double[] getClamp() {
        return clamp;
    }
    
    public Segment<T> setMouseDown(Action<MouseEventArgs> mouseDown) {
        return setMouseDown(null, mouseDown);
    }
    
    public Segment<T> setMouseUp(Action<MouseEventArgs> mouseUp) {
        return setMouseUp(null, mouseUp);
    }
    
    public Segment<T> setMouseDragged(Action<MouseEventArgs> mouseDragged) {
        return setMouseDragged(null, mouseDragged);
    }
    
    public Segment<T> setKeyDown(Action<KeyboardEventArgs> keyDown) {
        return setKeyDown(null, keyDown);
    }
    
    public Segment<T> setKeyUp(Action<KeyboardEventArgs> keyUp) {
        return setKeyUp(null, keyUp);
    }
    
    public Segment<T> setMouseDown(Predicate<MouseEventArgs> mousePredicate, Action<MouseEventArgs> mouseDown) {
        this.mousePredicate = mousePredicate;
        this.mouseDown = mouseDown;
        return this;
    }
    
    public Segment<T> setMouseUp(Predicate<MouseEventArgs> mousePredicate, Action<MouseEventArgs> mouseUp) {
        this.mousePredicate = mousePredicate;
        this.mouseUp = mouseUp;
        return this;
    }
    
    public Segment<T> setMouseDragged(Predicate<MouseEventArgs> mousePredicate, Action<MouseEventArgs> mouseDragged) {
        this.mousePredicate = mousePredicate;
        this.mouseDragged = mouseDragged;
        return this;
    }
    
    public Segment<T> setKeyDown(Predicate<KeyboardEventArgs> keyboardPredicate, Action<KeyboardEventArgs> keyDown) {
        this.keyboardPredicate = keyboardPredicate;
        this.keyDown = keyDown;
        return this;
    }
    
    public Segment<T> setKeyUp(Predicate<KeyboardEventArgs> keyboardPredicate, Action<KeyboardEventArgs> keyUp) {
        this.keyboardPredicate = keyboardPredicate;
        this.keyUp = keyUp;
        return this;
    }
    
    public Segment<T> setLabel(String label) {
        this.label = label;
        return this;
    }
    
    public Segment<T> setValue(T value) {
        this.value = value;
        return this;
    }
    
    public Segment<T> setClamp(double start, double end) {
        clamp[0] = start;
        clamp[1] = end;
        return this;
    }
    
    protected Action<MouseEventArgs> getDefaultMouseAction() {
        return e -> {};
    }
    
    protected Action<KeyboardEventArgs> getDefaultKeyboardAction() {
        return e -> {};
    }
}
