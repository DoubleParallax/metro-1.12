package com.doubleparallax.metro.gui.ui.components.deluge;

import com.doubleparallax.metro.gui.exception.InteractionCancellationException;
import com.doubleparallax.metro.gui.ui.Component;
import com.doubleparallax.metro.gui.ui.action.KeyboardEventArgs;
import com.doubleparallax.metro.gui.ui.action.MouseEventArgs;
import com.doubleparallax.metro.property.PropertyHandler;
import com.doubleparallax.metro.property.impl.Settings;
import com.doubleparallax.metro.util.InputHelper;
import com.doubleparallax.metro.util.MathHelper;
import com.doubleparallax.metro.util.animation.Animation;
import com.doubleparallax.metro.util.render.ColorHelper;
import com.doubleparallax.metro.util.render.RenderHelper;
import com.doubleparallax.metro.util.render.font.DFont;
import com.doubleparallax.metro.util.render.font.FontHelper;
import com.doubleparallax.metro.util.render.geometry.Donut;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class RadialMenu extends Component {
    
    private final List<Segment> SEGMENTS = new ArrayList<>();
    
    private Animation selectedSegment = new Animation().setAngular(true).addTo(ANIMATIONS);
    private int[] color = {0xA0D0D0D0, 0xA00366D8};
    private boolean dragging;
    
    public RadialMenu(Component parent, double x, double y, double r) {
        super(parent, x, y, r * 2, r * 2);
    }
    
    public RadialMenu addSegment(Segment... segments) {
        Collections.addAll(SEGMENTS, segments);
        SEGMENTS.sort(Comparator.comparing(Segment::getLabel));
        selectedSegment.setWrap(segments.length);
        return this;
    }
    
    @Override
    public void update() {
        super.update();
        
        if (SEGMENTS.isEmpty())
            return;
        
        double size = 360. / SEGMENTS.size();
        selectedSegment.setValue(Math.round(MathHelper.getDirection(getRelX(), getRelY(), InputHelper.getX(), InputHelper.getY()) / size));
    }
    
    @Override
    public void mouse(MouseEventArgs args)
            throws InteractionCancellationException {
        setHovered(true);
        super.mouse(args);
        if (!SEGMENTS.isEmpty()) {
            Segment segment = SEGMENTS.get((int) MathHelper.wrapValue(selectedSegment.getAbsoluteValue(), 0, SEGMENTS.size()));
            if (args.getState())
                segment.getMouseDown(args).onAction(args);
            else if (args.getButton() != -1)
                segment.getMouseUp(args).onAction(args);
            if (args.getButton() == 0)
                dragging = args.getState();
            if (dragging)
                segment.getMouseDragged(args).onAction(args);
            throw new InteractionCancellationException();
        }
    }
    
    @Override
    public void keyboard(KeyboardEventArgs args) throws InteractionCancellationException {
        super.keyboard(args);
        if (!SEGMENTS.isEmpty()) {
            for (Segment s : SEGMENTS) {
                if (args.getState())
                    s.getKeyDown(args).onAction(args);
                else
                    s.getKeyUp(args).onAction(args);
            }
            throw new InteractionCancellationException();
        }
    }
    
    @Override
    public void render(double alpha) {
        if (SEGMENTS.isEmpty())
            return;
        
        RenderHelper.preRender();
        GL11.glEnable(GL11.GL_LINE_SMOOTH);
        GlStateManager.disableTexture2D();
        
        double outer = getRelW() / 2.;
        double inner = outer / 1.7;
        
        Donut.draw_Xyr_Col_Mod(getRelX(), getRelY(), outer, inner, color[0], GL11.GL_TRIANGLE_STRIP, alpha);
        
        Segment selected = SEGMENTS.get((int) MathHelper.wrapValue(selectedSegment.getValue(), 0, SEGMENTS.size()));
        
        double angle = selectedSegment.getValue() * (360. / selectedSegment.getWrap());
        GlStateManager.pushMatrix();
        GlStateManager.translate(getRelX(), getRelY(), 0);
        GlStateManager.rotate((float) angle, 0, 0, 1);
        GlStateManager.translate(-getRelX(), -getRelY(), 0);
        
        double size = 180. / SEGMENTS.size();
        
        Donut.draw_Xyr_Cla_Col_Mod(getRelX(), getRelY(), outer, inner, SEGMENTS.size() == 1 ? selected.getClamp()[0] : -size, SEGMENTS.size() == 1 ? selected.getClamp()[1] : size, color[1], GL11.GL_TRIANGLE_STRIP, alpha);
        Donut.draw_Xyr_Cla_Col_Mod(getRelX(), getRelY(), outer, inner, SEGMENTS.size() == 1 ? selected.getClamp()[0] : -size, SEGMENTS.size() == 1 ? selected.getClamp()[1] : size, color[1], GL11.GL_LINE_STRIP, alpha);
        
        GlStateManager.popMatrix();
        
        PropertyHandler.get(Settings.class).animationSpeed = 2;
        
        Donut.draw_Xyr_Col_Mod(getRelX(), getRelY(), outer, inner, color[0], GL11.GL_LINE_STRIP, alpha);
        
        double dif = (inner + ((outer - inner) / 2.)) / 2.;
        
        double interpolated = MathHelper.wrapValue(selectedSegment.getValue(), 0, SEGMENTS.size());

        DFont font = (inner < 256 ? FontHelper.Fonts.ICON_24 : FontHelper.Fonts.ICON_48).getFont();
        for (Segment segment : SEGMENTS) {
            angle = (360. / SEGMENTS.size()) * SEGMENTS.indexOf(segment);
            double rad = Math.toRadians(angle);
            double x = getRelX() + (Math.cos(rad) * dif);
            double y = getRelY() + (Math.sin(rad) * dif);
            double diff = Math.min(Math.max(0, Math.abs(interpolated - SEGMENTS.indexOf(segment))), 1);
            diff = Math.min(Math.abs((interpolated - SEGMENTS.size()) - SEGMENTS.indexOf(segment)), diff);
            font.drawString(segment.getValue(), x, y, ColorHelper.relativeAlpha(ColorHelper.blend(0xFFD0D0D0, 0xFF404040, (float) diff), alpha), false, DFont.Horizontal.CENTER, DFont.Vertical.MIDDLE);
        }
        
        font = (inner < 256 ? FontHelper.Fonts.CALIBRI_16 : FontHelper.Fonts.CALIBRI_32).getFont();
        font.drawString(SEGMENTS.get((int) MathHelper.wrapValue(selectedSegment.getAbsoluteValue(), 0, SEGMENTS.size())).getLabel(), getRelX(), getRelY(), ColorHelper.relativeAlpha(-1, alpha), true, DFont.Horizontal.CENTER, DFont.Vertical.MIDDLE);
        
        super.render(alpha);
        
        RenderHelper.postRender();
    }
}
