package com.doubleparallax.metro.gui.ui.components.metro;

import com.doubleparallax.metro.gui.exception.InteractionCancellationException;
import com.doubleparallax.metro.gui.ui.Component;
import com.doubleparallax.metro.gui.ui.action.MouseEventArgs;
import com.doubleparallax.metro.gui.ui.components.AbstractTextView;
import com.doubleparallax.metro.imixin.IGuiScreenOptionsSoundsMixin;
import com.doubleparallax.metro.property.Property;
import com.doubleparallax.metro.property.PropertyDescriptor;
import com.doubleparallax.metro.util.InputHelper;
import com.doubleparallax.metro.util.MathHelper;
import com.doubleparallax.metro.util.animation.Animation;
import com.doubleparallax.metro.util.render.ColorHelper;
import com.doubleparallax.metro.util.render.RenderHelper;
import com.doubleparallax.metro.util.render.geometry.Rect;
import net.minecraft.client.gui.GuiScreenOptionsSounds;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.SoundCategory;
import org.lwjgl.opengl.GL11;

public class Slider extends Component {
    
    private enum Type {
        GAMESETTING, SOUND, PROPERTY
    }
    
    private double padding;
    private boolean outline;
    private Animation dragging = new Animation(false).addTo(ANIMATIONS);
    
    private Type type;
    private GameSettings.Options option;
    private SoundCategory sound;
    private Property property;
    
    public Slider(Component parent, double x, double y, double w, double h) {
        super(parent, x, y, w, h);
    }
    
    public Slider setPadding(double padding) {
        this.padding = padding;
        return this;
    }
    
    public Slider setOutline(boolean outline) {
        this.outline = outline;
        return this;
    }
    
    public Slider setOption(GameSettings.Options option) {
        this.option = option;
        type = Type.GAMESETTING;
        return this;
    }
    
    public Slider setSound(SoundCategory sound) {
        this.sound = sound;
        type = Type.SOUND;
        return this;
    }
    
    public Slider setProperty(Property property) {
        this.property = property;
        type = Type.PROPERTY;
        return this;
    }
    
    private double getValue() {
        switch (type) {
            case GAMESETTING:
                return option.normalizeValue(mc.gameSettings.getOptionFloatValue(option));
            case SOUND:
                return mc.gameSettings.getSoundLevel(sound);
            case PROPERTY:
                PropertyDescriptor descriptor = property.getField().getAnnotation(PropertyDescriptor.class);
                double span = descriptor.range()[1] - descriptor.range()[0];
                return (1. / span) * property.<Double>getFieldValue();
        }
        return 0;
    }
    
    @Override
    public void update() {
        super.update();
        if (dragging.getValueB()) {
            double drag = MathHelper.clamp(InputHelper.getX() - getRelX(), 0, getRelW()) / getRelW();
            String label = null;
            switch (type) {
                case GAMESETTING:
                    mc.gameSettings.setOptionFloatValue(option, option.denormalizeValue((float) drag));
                    label = mc.gameSettings.getKeyBinding(option);
                    break;
                case SOUND:
                    if (mc.currentScreen instanceof GuiScreenOptionsSounds) {
                        mc.gameSettings.setSoundLevel(sound, (float) drag);
                        label = String.format("%s: %s", I18n.format("soundCategory." + sound.getName()), ((IGuiScreenOptionsSoundsMixin) mc.currentScreen).displayString(sound));
                    }
                    break;
                case PROPERTY:
                    PropertyDescriptor descriptor = property.getField().getAnnotation(PropertyDescriptor.class);
                    double range = descriptor.range()[1] - descriptor.range()[0];
                    property.setFieldValue(MathHelper.step(drag * range, descriptor.range()[2]));
                    label = String.format("%s: %s", property.getName(), property.getFieldValue());
                    break;
            }
            if (label != null && !CHILDREN.isEmpty() && CHILDREN.get(0) instanceof AbstractTextView)
                ((AbstractTextView) CHILDREN.get(0)).setLabel(label);
        }
    }
    
    @Override
    public void mouse(MouseEventArgs args) throws InteractionCancellationException {
        super.mouse(args);
        if (args.getButton() == 0)
            dragging.setValue((isHovered() && args.getState()) || (args.getState() && dragging.getValueB()));
    }
    
    @Override
    public void render(double alpha) {
        RenderHelper.preRender();
        double x1 = getRelX() + padding;
        double y1 = getRelY() + padding;
        double x2 = getRelX() + getRelW() - padding;
        double y2 = getRelY() + getRelH() - padding;
        float hovered = (float) getHovered().getValue() + (float) dragging.getValue();
        int color = ColorHelper.blend(this.foreground[0], this.foreground[1], hovered);
        Rect.draw_Xy2_Col(x1, y1, x2, y2, color, alpha);
        GlStateManager.glLineWidth(2);
        
        double w = 4;
        double s = 2;
        double value = getValue() * (x2 - x1 - ((w + s) * 2));
        int handleColor = ColorHelper.blend(this.foreground[1], 0x80FFFFFF, hovered);
        Rect.draw_Xy2_Col(x1 + s + value, y1 + s, x1 + s + value + (w * 2), y2 - s, handleColor, alpha * (0.75 + (hovered / 4.)));
        Rect.draw_Xy2_Col_Mod(x1 + s + value, y1 + s, x1 + s + value + (w * 2), y2 - s, handleColor, GL11.GL_LINE_LOOP, alpha * (0.75 + (hovered / 4.)));
        
        if (outline)
            Rect.draw_Xy2_Col_Mod(x1, y1, x2, y2, color, GL11.GL_LINE_LOOP, alpha);
        RenderHelper.postRender();
        
        super.render(alpha);
    }
    
}
