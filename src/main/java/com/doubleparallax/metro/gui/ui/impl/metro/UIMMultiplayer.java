package com.doubleparallax.metro.gui.ui.impl.metro;

import com.doubleparallax.metro.event.IEventListener;
import com.doubleparallax.metro.gui.GuiDescriptor;
import com.doubleparallax.metro.gui.ui.action.MouseEventArgs;
import com.doubleparallax.metro.gui.ui.components.metro.Scrollable;
import com.doubleparallax.metro.gui.ui.components.metro.TextView;
import com.doubleparallax.metro.gui.ui.components.metro.listview.ListViewItemServer;
import com.doubleparallax.metro.imixin.IGuiScreenMixin;
import com.doubleparallax.metro.mixin.accessor.IGuiMultiplayerAccessor;
import com.doubleparallax.metro.mixin.accessor.IServerSelectionListAccessor;
import com.doubleparallax.metro.util.render.ColorHelper;
import com.doubleparallax.metro.util.render.font.DFont;
import com.doubleparallax.metro.util.render.font.FontHelper;
import com.doubleparallax.metro.util.render.font.Icon;
import net.minecraft.client.gui.*;
import net.minecraft.client.multiplayer.GuiConnecting;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.multiplayer.ServerList;
import net.minecraft.client.resources.I18n;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

import java.util.List;

@GuiDescriptor({GuiMultiplayer.class})
public class UIMMultiplayer extends UIM implements IEventListener {

    private Scrollable scrollable;

    @Override
    public void init(int width, int height) {
        super.init(width, height);

        double w = 300 + getPadding(2);
        double h = 40 + getPadding(2);

        double listH = 340;

        double startX = (Display.getWidth() / 2.) - w;
        double startY = (Display.getHeight() / 2.) - ((listH + h) / 2.);

        DFont font = FontHelper.Fonts.CALIBRI_16.getFont();
        DFont iconFont = FontHelper.Fonts.ICON_14.getFont();

        scrollable = new Scrollable(this, startX + getPadding(), startY + getPadding(), (w * 2) - getPadding(2), listH - getPadding(2))
                .setDirection(Scrollable.Direction.VERTICAL)
                .setOutline(0x40000000, 0x40000000, 2)
                .setBackground(0x40000000, 0x40000000);

        List<ServerListEntryNormal> servers = ((IServerSelectionListAccessor) ((IGuiMultiplayerAccessor) getGuiCurrent()).getServerListSelector()).getServerListInternet();
        Icon.Icons[] serverOptions = {
                Icon.Icons.CLOSE,
                Icon.Icons.PENCIL,
                Icon.Icons.CHECKMARK
        };
        for (int i = 0; servers != null && i < servers.size(); i++) {
            ListViewItemServer item = new ListViewItemServer(scrollable, 0, 80 * i, scrollable.getRelW(), 80) {
                @Override
                public void onSubmit() {
                    mc.displayGuiScreen(new GuiConnecting(getGuiCurrent(), mc, getValue().getServerData()));
                }
            }.setValue(servers.get(i))
                    .setForeground(0x004080FF, 0x804080FF);
            for (int j = 0; j < serverOptions.length; j++) {
                int index = j;
                Icon.Icons option = serverOptions[index];
                new TextView(item, item.getRelW() - 35 - (32 * index), 3, 32, item.getRelH() - 6) {
                    @Override
                    public void onMouseDown(MouseEventArgs args) {
                        if (isInteractable() && args.getButton() == 0) {
                            IGuiMultiplayerAccessor guiMP = ((IGuiMultiplayerAccessor) getGuiCurrent());
                            switch (index) {
                                case 0:
                                    ServerList serverList = guiMP.getSavedServerList();
                                    serverList.removeServerData(scrollable.getChildren().indexOf(item));
                                    serverList.saveServerList();
                                    List<GuiButton> buttonList = ((IGuiScreenMixin) getGuiCurrent()).getButtonList();
                                    ((IGuiScreenMixin) getGuiCurrent()).onActionPerformed(buttonList.get(5));
                                    break;
                                case 1:
                                    guiMP.setEditingServer(true);
                                    ServerData serverdata = item.getValue().getServerData();
                                    mc.displayGuiScreen(new GuiScreenAddServer(getGuiCurrent(), serverdata));
                                    break;
                                case 2:
                                    mc.displayGuiScreen(new GuiConnecting(getGuiCurrent(), mc, item.getValue().getServerData()));
                                    break;
                            }
                        }
                    }
                }.setFont(iconFont)
                        .setLabel(option.toString())
                        .setHAlign(DFont.Horizontal.CENTER)
                        .setVAlign(DFont.Vertical.MIDDLE)
                        .setForeground(0x80FFFFFF, 0x80202020)
                        .setBackground(ColorHelper.blend(0xFF4080FF, 0xFF202020, .5f), ColorHelper.blend(0xFF4080FF, 0xFFE0E0E0, .25f));
            }
        }

        Object[][] buttons = {
                {startX, startY + listH, w / 2., h, new Button(I18n.format("selectServer.direct"), () -> mc.displayGuiScreen(new GuiScreenServerList(getGuiParent(), new ServerData(I18n.format("selectServer.defaultName"), "", false))))},
                {startX + (w / 2.), startY + listH, w / 2., h, new Button(I18n.format("selectServer.add"), () -> mc.displayGuiScreen(new GuiScreenAddServer(getGuiCurrent(), new ServerData(I18n.format("selectServer.defaultName"), "", false))))},
                {startX + w, startY + listH, w / 2., h, new Button(I18n.format("selectServer.refresh"), () -> mc.displayGuiScreen(new GuiMultiplayer(getGuiParent())))},
                {startX + w + (w / 2.), startY + listH, w / 2., h, new Button(I18n.format("gui.cancel"), () -> ((IGuiScreenMixin) getGuiCurrent()).onKeyTyped('\0', Keyboard.KEY_ESCAPE))}
        };

        for (Object[] btn : buttons) {
            double _x = (double) btn[0] + getPadding();
            double _y = (double) btn[1] + getPadding();
            double _w = (double) btn[2] - getPadding(2);
            double _h = (double) btn[3] - getPadding(2);

            Button button = (Button) btn[4];

            new TextView(this, _x, _y, _w, _h) {
                @Override
                public void onMouseDown(MouseEventArgs args) {
                    if (isInteractable() && args.getButton() == 0)
                        button.run();
                }
            }.setFont(font)
                    .setLabel(button.getLabel())
                    .setHAlign(DFont.Horizontal.CENTER)
                    .setVAlign(DFont.Vertical.MIDDLE)
                    .setShadow(true)
                    .setOutline(0x40000000, 0x804080FF, 2)
                    .setBackground(0x40000000, 0x804080FF)
                    .setForeground(0x80FFFFFF, -1);
        }
    }

    /*@Override
    public void onAction(String index, Component component) throws InteractionCancellationException {
        super.onAction(index, component);

        if (component == null)
            return;

        int[] indices = component.getIndices();
        if (component instanceof AbstractTextView) {
            switch (indices.length) {
                case 3:
                    ListViewItemServer itemSelected = (ListViewItemServer) scrollable.getChildren().stream().filter(i -> ((ListViewItem) i).isSelected()).findFirst().orElse(null);
                    switch (indices[1]) {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                            List<GuiButton> buttonList = ((IGuiScreenMixin) getGuiCurrent()).getButtonList();
                            ((IGuiScreenMixin) getGuiCurrent()).onActionPerformed(buttonList.get(indices[1] + 3));
                            break;
                    }
                    break;
                case 5:
                    if (indices[1] == -1) {
                        ListViewItemServer itemHovered = (ListViewItemServer) scrollable.getChildren().get(indices[3]);
                        IGuiMultiplayerAccessor guiMP = ((IGuiMultiplayerAccessor) getGuiCurrent());
                        if (itemHovered != null) {
                            switch (indices[4]) {
                                case 0:
                                    ServerList serverList = guiMP.getSavedServerList();
                                    serverList.removeServerData(scrollable.getChildren().indexOf(itemHovered));
                                    serverList.saveServerList();
                                    List<GuiButton> buttonList = ((IGuiScreenMixin) getGuiCurrent()).getButtonList();
                                    ((IGuiScreenMixin) getGuiCurrent()).onActionPerformed(buttonList.get(5));
                                    break;
                                case 1:
                                    guiMP.setEditingServer(true);
                                    ServerData serverdata = itemHovered.getValue().getServerData();
                                    mc.displayGuiScreen(new GuiScreenAddServer(getGuiCurrent(), serverdata));
                                    break;
                                case 2:
                                    mc.displayGuiScreen(new GuiConnecting(getGuiCurrent(), mc, itemHovered.getValue().getServerData()));
                                    break;
                            }
                        }
                    }
                    break;
            }
        } else if (component instanceof ListViewItemWorld && indices.length == 4) {
            ListViewItemWorld item = (ListViewItemWorld) component;
            item.getValue().joinWorld();
        }
    }*/
}
