package com.doubleparallax.metro.gui.ui.action;

import com.doubleparallax.metro.gui.exception.InteractionCancellationException;

public interface Action<T> {
    
    void onAction(T value) throws InteractionCancellationException;
    
}
