package com.doubleparallax.metro.gui.ui.components.metro.listview;

import com.doubleparallax.metro.gui.exception.InteractionCancellationException;
import com.doubleparallax.metro.gui.ui.Component;
import com.doubleparallax.metro.gui.ui.action.MouseEventArgs;
import com.doubleparallax.metro.util.animation.Animation;
import com.doubleparallax.metro.util.render.ColorHelper;
import com.doubleparallax.metro.util.render.font.DFont;
import com.doubleparallax.metro.util.render.font.FontHelper;
import com.doubleparallax.metro.util.render.geometry.Rect;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.opengl.GL11;

public abstract class ListViewItem<T> extends Component {

    protected static final double padding = 2;
    protected static final DFont font = FontHelper.Fonts.CALIBRI_16.getFont();

    private T value;
    private Animation selected = new Animation(false).addTo(ANIMATIONS);
    private long lastClick;

    public ListViewItem(Component parent, double x, double y, double w, double h) {
        super(parent, x, y, w, h);
    }
    
    public <U extends ListViewItem> U setValue(T value) {
        this.value = value;
        return cast();
    }
    
    public <U extends ListViewItem> U setSelected(boolean selected) {
        this.selected.setValue(selected);
        return cast();
    }

    public T getValue() {
        return value;
    }
    
    public Animation getSelected() {
        return selected;
    }
    
    public boolean isSelected() {
        return selected.getValueB();
    }
    
    @Override
    public void mouse(MouseEventArgs args) throws InteractionCancellationException {
        super.mouse(args);
        if (args.getState() && args.getButton() == 0) {
            if (isHovered()) {
                if (Minecraft.getSystemTime() - lastClick < 250) {
                    onSubmit();
                    lastClick = -1;
                } else
                    lastClick = Minecraft.getSystemTime();
            }
            if (parent == null || parent.isHovered())
                selected.setValue(isHovered());
        }
    }

    protected void renderStockBackground(double alpha) {
        double x1 = getRelX() + padding;
        double y1 = getRelY() + padding;
        double x2 = getRelX() + getRelW() - padding;
        double y2 = getRelY() + getRelH() - padding;

        int color = ColorHelper.blend(ColorHelper.blend(this.foreground[0], this.foreground[1], (float) getSelected().getValue()), ColorHelper.blend(this.foreground[1], 0xFF202020, (float) getSelected().getValue() / 4.f), (float) getHovered().getValue());

        Rect.draw_Xy2_Col(x1, y1, x2, y2, color, alpha);
        GlStateManager.glLineWidth(2);
        Rect.draw_Xy2_Col_Mod(x1, y1, x2, y2, color, GL11.GL_LINE_LOOP, alpha);
    }
    
}
