package com.doubleparallax.metro.gui.ui.components.metro.listview;

import com.doubleparallax.metro.gui.ui.Component;
import com.doubleparallax.metro.mixin.accessor.IServerListEntryNormalAccessor;
import com.doubleparallax.metro.util.StringHelper;
import com.doubleparallax.metro.util.render.ColorHelper;
import com.doubleparallax.metro.util.render.RenderHelper;
import com.doubleparallax.metro.util.render.font.DFont;
import com.doubleparallax.metro.util.render.geometry.Rect;
import net.minecraft.client.gui.ServerListEntryNormal;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.resources.I18n;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class ListViewItemServer extends ListViewItem<ServerListEntryNormal> {


    public ListViewItemServer(Component parent, double x, double y, double w, double h) {
        super(parent, x, y, w, h);
    }

    @Override
    public void update() {
        super.update();

        IServerListEntryNormalAccessor accessor = (IServerListEntryNormalAccessor) getValue();
        ServerData server = accessor.getServer();
        if (!server.pinged) {
            server.pinged = true;
            server.pingToServer = -2L;
            server.serverMOTD = "";
            server.populationInfo = "";
            accessor.getExecutor().submit(() -> {
                try {
                    accessor.getOwner().getOldServerPinger().ping(server);
                } catch (UnknownHostException var2) {
                    server.pingToServer = -1L;
                    server.serverMOTD = I18n.format("multiplayer.status.cannot_resolve");
                } catch (Exception var3) {
                    server.pingToServer = -1L;
                    server.serverMOTD = I18n.format("multiplayer.status.cannot_connect");
                }
            });
        }
    }

    @Override
    public void render(double alpha) {
        RenderHelper.preRender();

        renderStockBackground(alpha);

        double x1 = getRelX() + padding;
        double y1 = getRelY() + padding;
        double x2 = getRelX() + getRelW() - padding;
        double y2 = getRelY() + getRelH() - padding;

        double hoverSelectedAlpha = alpha * (0.5 + ((getHovered().getValue() + getSelected().getValue()) / 4.));

        IServerListEntryNormalAccessor accessor = (IServerListEntryNormalAccessor) getValue();
        ServerData server = accessor.getServer();

        Rect.draw_Res_Xy2(accessor.getIcon() != null ? accessor.getServerIcon() : accessor.getUnknownServer(), x1 + padding, y1 + padding, x1 + (y2 - y1) - padding, y2 - padding, hoverSelectedAlpha);
        if (server.version != 335)
            Rect.draw_Xy2_Col(x1 + padding, y1 + padding, x1 + (y2 - y1) - padding, y2 - padding, 0x40FF0000, hoverSelectedAlpha);

        font.drawString(
                String.format("%s (%s)\n%s",
                        server.serverName,
                        server.serverIP,
                        server.pingToServer >= 0 ? StringHelper.wrapString(font, server.serverMOTD, getRelW() / 1.5) : "No connection"
                ),
                x1 + getRelH(), y1 + ((y2 - y1) / 2.),
                ColorHelper.relativeAlpha(-1, hoverSelectedAlpha), true,
                DFont.Horizontal.LEFT, DFont.Vertical.MIDDLE);

        List<String> extrasList = new ArrayList<>();
        extrasList.add(server.gameVersion);
        extrasList.add(server.populationInfo);
        extrasList.add(server.pingToServer < 0 ? "Pinging..." : String.format("Ping: %sms", server.pingToServer));

        StringBuilder extras = new StringBuilder();
        for (int i = 0; i < extrasList.size(); i++) {
            extras.append(extrasList.get(i));
            if (i < extrasList.size() - 1)
                extras.append("\n");
        }

        font.drawString(extras.toString(),
                x2 - 8, y1 + ((y2 - y1) / 2.),
                ColorHelper.relativeAlpha(-1, hoverSelectedAlpha), true,
                DFont.Horizontal.RIGHT, DFont.Vertical.MIDDLE);

        RenderHelper.postRender();

        super.render(alpha * getHovered().getValue());
    }

}
