package com.doubleparallax.metro.gui.ui.impl.deluge;

import com.doubleparallax.metro.gui.GuiDescriptor;
import com.doubleparallax.metro.gui.GuiHandler;
import com.doubleparallax.metro.gui.ui.components.deluge.RadialMenu;
import com.doubleparallax.metro.gui.ui.components.deluge.Segment;
import com.doubleparallax.metro.property.Property;
import com.doubleparallax.metro.util.render.font.Icon;
import org.lwjgl.opengl.Display;

import java.util.Collection;
import java.util.stream.Collectors;

@GuiDescriptor(value = {}, bordered = false)
public class UIDProperties extends UID {
    
    private Collection<? extends Property> properties;
    
    public UIDProperties(Collection<? extends Property> properties) {
        this.properties = properties;
    }
    
    @Override
    public void init(int width, int height) {
        super.init(width, height);
        
        double r = Math.min(Math.hypot(Display.getWidth(), Display.getHeight()) / 4., 512);
        
        RadialMenu menu = new RadialMenu(this, Display.getWidth() / 2., Display.getHeight() / 2., r);
        menu.addSegment(properties.stream()
                .map(p -> new Segment<>(p.getName(), Icon.get(p.getId()))
                        .setMouseDown(e -> e.getButton() == 0, e -> {
                            Collection<? extends Property> properties = p.getChildren().values();
                            if (!properties.isEmpty())
                                GuiHandler.INSTANCE.openUI(new UIDProperties(properties));
                        })).collect(Collectors.toList()).toArray(new Segment[0])
        );
    }
    
}
