package com.doubleparallax.metro.gui.ui.components.metro.listview;

import com.doubleparallax.metro.gui.ui.Component;
import com.doubleparallax.metro.mixin.accessor.IGuiListWorldSelectionEntryAccessor;
import com.doubleparallax.metro.util.render.ColorHelper;
import com.doubleparallax.metro.util.render.RenderHelper;
import com.doubleparallax.metro.util.render.font.DFont;
import com.doubleparallax.metro.util.render.geometry.Rect;
import net.minecraft.client.gui.GuiListWorldSelectionEntry;
import net.minecraft.client.resources.I18n;
import net.minecraft.world.storage.WorldSummary;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListViewItemWorld extends ListViewItem<GuiListWorldSelectionEntry> {
    
    public ListViewItemWorld(Component parent, double x, double y, double w, double h) {
        super(parent, x, y, w, h);
    }
    
    @Override
    public void render(double alpha) {
        RenderHelper.preRender();

        renderStockBackground(alpha);
        
        double x1 = getRelX() + padding;
        double y1 = getRelY() + padding;
        double x2 = getRelX() + getRelW() - padding;
        double y2 = getRelY() + getRelH() - padding;
        
        double hoverSelectedAlpha = alpha * (0.5 + ((getHovered().getValue() + getSelected().getValue()) / 4.));

        IGuiListWorldSelectionEntryAccessor accessor = (IGuiListWorldSelectionEntryAccessor) getValue();
        WorldSummary world = accessor.getWorldSummary();
        
        Rect.draw_Res_Xy2(accessor.getIconFile() != null ? accessor.getIconLocation() : accessor.getIconMissing(), x1 + padding, y1 + padding, x1 + (y2 - y1) - padding, y2 - padding, hoverSelectedAlpha);
        if (world.markVersionInList())
            Rect.draw_Xy2_Col(x1 + padding, y1 + padding, x1 + (y2 - y1) - padding, y2 - padding, 0x40FF0000, hoverSelectedAlpha);
        
        font.drawString(
                String.format("%s\n%s (%s)\n%s",
                        world.getDisplayName(),
                        world.getFileName(),
                        accessor.getDateFormat().format(new Date(world.getLastTimePlayed())),
                        world.isHardcoreModeEnabled() ? I18n.format("gameMode.hardcore") : StringUtils.capitalize(world.getEnumGameType().getName())
                ),
                x1 + getRelH(), y1 + ((y2 - y1) / 2.),
                ColorHelper.relativeAlpha(-1, hoverSelectedAlpha), true,
                DFont.Horizontal.LEFT, DFont.Vertical.MIDDLE);
        
        List<String> extrasList = new ArrayList<>();
        if (world.requiresConversion())
            extrasList.add(I18n.format("selectWorld.conversion"));
        if (world.getCheatsEnabled())
            extrasList.add(String.format("%s Enabled", I18n.format("selectWorld.cheats")));
        extrasList.add(String.format("%s %s", I18n.format("selectWorld.version"), world.getVersionName()));
        
        StringBuilder extras = new StringBuilder();
        for (int i = 0; i < extrasList.size(); i++) {
            extras.append(extrasList.get(i));
            if (i < extrasList.size() - 1)
                extras.append("\n");
        }
        
        font.drawString(extras.toString(),
                x2 - 8, y1 + ((y2 - y1) / 2.),
                ColorHelper.relativeAlpha(-1, hoverSelectedAlpha), true,
                DFont.Horizontal.RIGHT, DFont.Vertical.MIDDLE);
        
        RenderHelper.postRender();
        
        super.render(alpha * getHovered().getValue());
    }
    
}
