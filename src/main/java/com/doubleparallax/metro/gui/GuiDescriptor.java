package com.doubleparallax.metro.gui;

import net.minecraft.client.gui.GuiScreen;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface GuiDescriptor {
    
    Class<? extends GuiScreen>[] value();
    
    boolean bordered() default true;

    boolean popup() default false;
    
}