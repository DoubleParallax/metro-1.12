package com.doubleparallax.metro.hook;

import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.impl.AbstractEventPlayer;
import com.doubleparallax.metro.event.impl.EventChat;
import com.doubleparallax.metro.util.Helper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.stats.RecipeBook;
import net.minecraft.stats.StatisticsManager;
import net.minecraft.world.World;

public class EntityPlayerSPHook extends EntityPlayerSP implements Helper {
    
    private EntityPlayerSPHook(World world) {
        this(Minecraft.getMinecraft(), world, Minecraft.getMinecraft().getConnection(), new StatisticsManager(), new RecipeBook());
    }
    
    public EntityPlayerSPHook(Minecraft p_i47378_1_, World p_i47378_2_, NetHandlerPlayClient p_i47378_3_, StatisticsManager p_i47378_4_, RecipeBook p_i47378_5_) {
        super(p_i47378_1_, p_i47378_2_, p_i47378_3_, p_i47378_4_, p_i47378_5_);
    }
    
    @Override
    public void onUpdate() {
        AbstractEventPlayer.Update event = new AbstractEventPlayer.Update().setPlayer(this).setCall(Event.Call.PRE).fire();
        if (!event.isCancelled())
            super.onUpdate();
        event.setCall(Event.Call.POST).fire();
    }
    
    @Override
    public void sendChatMessage(String message) {
        EventChat event = new EventChat().setMessage(message).setCall(EventChat.Call.SEND_PRE).fire();
        message = event.getMessage();
        if (!event.isCancelled())
            super.sendChatMessage(message);
        event.setCall(EventChat.Call.SEND_POST).fire();
    }
}
