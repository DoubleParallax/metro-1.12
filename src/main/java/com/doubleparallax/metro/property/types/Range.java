package com.doubleparallax.metro.property.types;

public class Range implements Type<Range> {
    
    private double min, max;
    
    public double getMin() {
        return min;
    }
    
    public double getMax() {
        return max;
    }
    
    public Range setMin(double min) {
        this.min = min;
        return this;
    }
    
    public Range setMax(double max) {
        this.max = max;
        return this;
    }
    
    @Override
    public Range parse(Object o) {
        String[] range = o.toString().split("\\-");
        if (range.length != 2)
            return this;
        setMin(Double.parseDouble(range[0]));
        setMax(Double.parseDouble(range[1]));
        return this;
    }
    
    @Override
    public String toString() {
        return String.format("%s-%s", min, max);
    }
    
    
}
