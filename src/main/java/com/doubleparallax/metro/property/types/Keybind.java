package com.doubleparallax.metro.property.types;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class Keybind implements Type<Keybind> {
    
    private int key;
    
    public int getKey() {
        return key;
    }
    
    public Keybind setKey(int key) {
        this.key = key;
        return this;
    }
    
    @Override
    public Keybind parse(Object o) {
        String key = o.toString().toUpperCase();
        int keyboard = Keyboard.getKeyIndex(key);
        int mouse = Mouse.getButtonIndex(key);
        this.key = keyboard == Keyboard.KEY_NONE ? mouse == -1 ? Keyboard.KEY_NONE : mouse - 100 : keyboard;
        return this;
    }
    
    @Override
    public String toString() {
        return key < -1 ? Mouse.getButtonName(key + 100) : Keyboard.getKeyName(key);
    }
    
    
}
