package com.doubleparallax.metro.property.types;

public interface Type<T> {
    
    T parse(Object o);
    
}
