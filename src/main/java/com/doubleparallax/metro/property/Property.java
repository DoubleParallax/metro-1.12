package com.doubleparallax.metro.property;

import com.doubleparallax.metro.event.IEventListener;
import com.doubleparallax.metro.util.ClassHelper;
import com.doubleparallax.metro.util.JsonHelper;
import com.doubleparallax.metro.util.io.FileHelper;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Property implements IEventListener {
    
    private static final String DIRECTORY = "locale/property";
    
    private transient boolean parsedDescription;
    
    protected transient Property globalParent, parent;
    protected transient String id, name, desc = "";
    protected transient String[] alias = new String[0];
    protected transient Field field;
    
    protected final transient Map<String, Property> globalChildren = new HashMap<>();
    protected final transient Map<String, Property> children = new LinkedHashMap<>();
    
    protected Property(String id) {
        this.id = id;
        
        register();
        
        String json = FileHelper.readResource(FileHelper.procRes("%s/%s.json", DIRECTORY, id));
        json = JsonHelper.compileJson(json, DIRECTORY);
        
        if (json == null)
            return;
        
        PropertyBean bean;
        try {
            bean = JsonHelper.OBJECT_MAPPER.readValue(json, PropertyBean.class);
            if (bean == null)
                throw new IOException();
            name = bean.getName();
            desc = bean.getDesc().replaceAll("\\^name\\^", name);
            alias = bean.getAlias();
        } catch (IOException e) {
            System.err.printf("Failed to load property locale for %s\n", id);
            return;
        }
        
        compile(ClassHelper.getFieldsRecursiveSuper(new ArrayList<>(), getClass()).stream().filter(f -> f.isAnnotationPresent(PropertyDescriptor.class)).collect(Collectors.toList()), bean.getChildren(), this, this, f -> f.getAnnotation(PropertyDescriptor.class).parent().isEmpty());
    }
    
    private Property(PropertyBean bean, PropertyDescriptor descriptor, Property globalParent, Property parent, Field field) {
        this.id = name = desc = descriptor.value();
        this.globalParent = globalParent;
        this.parent = parent;
        this.field = field;
        
        if (bean != null) {
            name = bean.getName();
            desc = bean.getDesc().replaceAll("\\^name\\^", name);
            alias = bean.getAlias();
        }
    }
    
    public Property getGlobalParent() {
        return globalParent;
    }
    
    public Property getParent() {
        return parent;
    }
    
    public String getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
    
    public String getDesc() {
        return parseDescription();
    }
    
    public String[] getAlias() {
        return alias;
    }
    
    public Field getField() {
        return field;
    }
    
    public Map<String, Property> getGlobalChildren() {
        return globalChildren;
    }
    
    public Map<String, Property> getChildren() {
        return children;
    }
    
    @JsonIgnore
    public <T> T getFieldValue() {
        try {
            return (T) field.get(globalParent);
        } catch (IllegalAccessException e) {
            System.err.printf("Failed to get value of %s\n", field.getName());
        }
        return null;
    }
    
    @JsonIgnore
    public boolean setFieldValue(Object value) {
        try {
            field.set(globalParent, value);
            globalParent.onChildValueChanged(this, value);
            return true;
        } catch (IllegalAccessException e) {
            System.err.printf("Failed to set value of %s to %s\n", field.getName(), value);
        }
        return false;
    }
    
    protected void onChildValueChanged(Property property, Object value) {
    }
    
    public void save() {
        save("default");
    }
    
    public void save(String profile) {
        try {
            String json = JsonHelper.OBJECT_WRITER.writeValueAsString(this);
            FileHelper.writeFile(FileHelper.procExternalRes("property/%s/%s.json", profile, id), json);
        } catch (IOException e) {
            System.err.printf("Failed to save %s properties\n", id);
        }
    }
    
    public void load() {
        load("default");
    }
    
    public void load(String profile) {
        try {
            String json = FileHelper.readFile(FileHelper.procExternalRes("property/%s/%s.json", profile, id));
            if (json == null)
                throw new FileNotFoundException();
            JsonHelper.OBJECT_MAPPER.readerForUpdating(this).readValue(json);
        } catch (FileNotFoundException e) {
            save(profile);
        } catch (IOException e) {
            System.err.printf("Failed load %s properties\n", id);
        }
    }
    
    public Property getChild(String key) {
        return getChild(key.contains(".") ? key.split("\\.") : new String[] {key}, 0);
    }
    
    private Property getChild(String[] keys, int index) {
        if (index >= keys.length)
            return this;
        if (children.containsKey(keys[index]))
            return children.get(keys[index]).getChild(keys, index + 1);
        for (Property property : children.values())
            if (property.getAlias() != null && Arrays.asList(property.getAlias()).contains(keys[index]))
                return property.getChild(keys, index + 1);
        return null;
    }
    
    private String parseDescription() {
        if (parsedDescription)
            return desc;
        
        parsedDescription = true;
        desc = desc.replaceAll("\\^(?i)name\\^", name);
        if (parent != null && parent.field != null) {
            String[] inner = StringUtils.substringsBetween(desc, "^", "^");
            if (inner != null)
                for (String s : inner) {
                    int i = s.indexOf(":");
                    if (s.toLowerCase().startsWith("parent") && i != -1)
                        try {
                            i = Integer.parseInt(s.substring(i + 1));
                            desc = desc.replaceAll("\\^(?i)" + s + "\\^", getRecursiveParentName(parent, 0, i));
                        } catch (Exception ignored) {
                        }
                }
        }
        return desc;
    }
    
    private String getRecursiveParentName(Property property, int current, int desired) {
        return (current == desired || property.parent == null || property.parent.field == null) ? property.name : getRecursiveParentName(property, current + 1, desired);
    }
    
    private void compile(List<Field> fields, Map<String, PropertyBean> beans, Property globalParent, Property parent, Predicate<Field> filter) {
        fields.stream().filter(filter).forEach(f -> {
            PropertyDescriptor descriptor = f.getAnnotation(PropertyDescriptor.class);
            PropertyBean bean = beans.get(descriptor.value());
            
            if (bean == null)
                return;
            
            Property property = new Property(bean, descriptor, globalParent, parent, f);
            String id = property.id.contains(".") ? property.id.substring(property.id.lastIndexOf(".") + 1) : property.id;
            parent.children.put(id, property);
            globalParent.globalChildren.put(property.id, property);
            
            compile(fields, bean.getChildren(), globalParent, property, g -> !g.getAnnotation(PropertyDescriptor.class).parent().isEmpty() && g.getAnnotation(PropertyDescriptor.class).parent().equalsIgnoreCase(descriptor.value()));
        });
    }
    
}
