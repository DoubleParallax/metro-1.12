package com.doubleparallax.metro.property;

import com.doubleparallax.metro.Client;
import com.doubleparallax.metro.util.ClassHelper;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PropertyHandler {
    
    private static final Map<Class<? extends Property>, Property> PROPERTIES = new HashMap<>();
    
    public static void init() {
        new Reflections(Client.class.getPackage().getName().replaceAll("package ", ""))
                .getSubTypesOf(Property.class).stream()
                .filter(c -> !Modifier.isAbstract(c.getModifiers()))
                .forEach(c -> {
                    Property property;
                    try {
                        property = c.newInstance();
                    } catch (IllegalAccessException | InstantiationException e) {
                        System.err.printf("Failed to initialize property %s\n", c.getName());
                        return;
                    }
                    PROPERTIES.put(c, property);
                    property.load();
                    property.save();
                });
    }
    
    public static List<Property> getProperties() {
        return new ArrayList<>(PROPERTIES.values());
    }
    
    public static <T extends Property> List<T> getSubTypes(Class<T> _class) {
        return (List<T>) PROPERTIES.values().stream().filter(c -> ClassHelper.isRecursive(c.getClass(), _class)).collect(Collectors.toList());
    }
    
    public static <T extends Property> T get(Class<T> _class) {
        return _class.cast(PROPERTIES.get(_class));
    }
    
}
