package com.doubleparallax.metro.property.impl.module;

import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.EventDescriptor;
import com.doubleparallax.metro.event.impl.AbstractEventPlayer;
import com.doubleparallax.metro.property.PropertyDescriptor;
import com.doubleparallax.metro.property.impl.module.impl.InventoryManager;
import com.doubleparallax.metro.util.DeltaTime;
import com.doubleparallax.metro.util.inventory.InventoryHelper;
import com.doubleparallax.metro.util.inventory.InventoryInteraction;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;

@EventDescriptor({AbstractEventPlayer.Update.class})
public class AutoEat extends InventoryManager {

    //@formatter:off

    @PropertyDescriptor("delay")
    public transient Void delay;

        @PropertyDescriptor(parent = "delay", value = "switch", range = {1, 1000, 25})
        public int delaySwitch = 250;

    //@formatter:on

    private transient DeltaTime switchTimer = new DeltaTime();

    public AutoEat() {
        super("autoeat", Category.AUTO, Category.PLAYER);
    }
    
    @Override
    public void onEvent(Event event) {
        super.onEvent(event);
        if (!enabled)
            return;
        if (event instanceof AbstractEventPlayer.Update) {
            AbstractEventPlayer.Update e = (AbstractEventPlayer.Update) event;
            if ((InventoryHelper.inUse != null && InventoryHelper.inUse != this))
                return;
            switch (e.getCall()) {
                case PRE:
                    if (InventoryHelper.inUse == this)
                        InventoryHelper.inUse = null;
                    if ((InventoryHelper.inUse != null && InventoryHelper.inUse != this))
                        break;
                    if (switchTimer.is(delaySwitch)) {
                        InventoryInteraction interaction = getNextInteraction();
                        if (interaction != null && state)
                            InventoryHelper.clickSlot(this, interaction.getWindow(), interaction.getSlot(), interaction.getButton(), interaction.getType(), interaction.getPlayer());
                        else
                            setState(false);
                    }
                    break;
                case POST:
                    if (switchTimer.is(delaySwitch))
                        switchTimer.reset();
                    break;
            }
        } else if (event instanceof AbstractEventPlayer.Mining) {
            AbstractEventPlayer.Mining e = (AbstractEventPlayer.Mining) event;
            if ((InventoryHelper.inUse != null && InventoryHelper.inUse != this))
                return;
            switch (e.getCall()) {
                case PRE:
                    if (InventoryHelper.inUse == this)
                        InventoryHelper.inUse = null;
                    if ((InventoryHelper.inUse != null && InventoryHelper.inUse != this))
                        break;
                    if (switchTimer.is(delaySwitch)) {
                        if (!state || INTERACTIONS.isEmpty()) {
                            Container container = e.getPlayer().inventoryContainer;
                            Slot itemSlot = InventoryHelper.getFoodSlot(e.getPlayer(), container, 9, container.inventorySlots.size() + 1);
                            if (itemSlot != null && itemSlot.slotNumber != 45) {
                                INTERACTIONS.add(new InventoryInteraction(e.getPlayer().inventoryContainer.windowId, itemSlot.slotNumber, mc.player.inventory.currentItem, ClickType.SWAP, e.getPlayer()));
                                setState(true);
                            }
                        }
                    }
                    break;
            }
        }
    }
}
