package com.doubleparallax.metro.property.impl;

import com.doubleparallax.metro.command.CommandHandler;
import com.doubleparallax.metro.command.impl.CommandProperty;
import com.doubleparallax.metro.property.Property;
import com.doubleparallax.metro.property.PropertyDescriptor;

public class Settings extends Property {
    
    //@formatter:off
    
    @PropertyDescriptor("enabled")
    public boolean enabled = true;
    
    @PropertyDescriptor("console")
    public boolean console = true;
    
        @PropertyDescriptor(parent = "console", value = "prefix")
        public String consolePrefix = "!";
        
        @PropertyDescriptor(parent = "console", value = "longPrefix")
        public String consoleLongPrefix = "--";
        
        @PropertyDescriptor(parent = "console", value = "shortPrefix")
        public String consoleShortPrefix = "-";
        
        @PropertyDescriptor(parent = "console", value = "viewValue")
        public String consoleViewValue = "?";
        
        @PropertyDescriptor(parent = "console", value = "negate")
        public String consoleNegate = "\\";
    
    @PropertyDescriptor("animation")
    public boolean animation = false;
    
        @PropertyDescriptor(parent = "animation", value = "speed")
        public double animationSpeed = 3;
    
    //@formatter:on
    
    public Settings() {
        super("settings");
        
        CommandHandler.INSTANCE.register(new CommandProperty(this));
    }
    
}
