package com.doubleparallax.metro.property.impl.module;

import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.EventDescriptor;
import com.doubleparallax.metro.event.impl.AbstractEventPlayer;
import com.doubleparallax.metro.property.PropertyDescriptor;
import com.doubleparallax.metro.property.impl.module.impl.InventoryManager;
import com.doubleparallax.metro.util.DeltaTime;
import com.doubleparallax.metro.util.inventory.InventoryHelper;
import com.doubleparallax.metro.util.inventory.InventoryInteraction;
import net.minecraft.init.PotionTypes;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemSplashPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionUtils;
import net.minecraft.util.EnumHand;

import java.util.Arrays;

@EventDescriptor({AbstractEventPlayer.Update.class, AbstractEventPlayer.Walking.class})
public class AutoPotion extends InventoryManager {
    
    //@formatter:off
    
    @PropertyDescriptor("delay")
    public transient Void delay;
    
        @PropertyDescriptor(parent = "delay", value = "switch", range = {1, 1000, 25})
        public int delaySwitch = 250;
        
        @PropertyDescriptor(parent = "delay", value = "throw", range = {1, 1000, 25})
        public int delayThrow = 750;
    
    @PropertyDescriptor(value = "health", range = {1, 20, 1})
    public int health = 4;
    
    //@formatter:on
    
    private transient float prevPitch;
    private transient DeltaTime switchTimer = new DeltaTime(),
            throwTimer = new DeltaTime();
    
    public AutoPotion() {
        super("autopotion", Category.AUTO, Category.COMBAT);
    }
    
    @Override
    public void onEvent(Event event) {
        super.onEvent(event);
        if (!enabled)
            return;
        if (event instanceof AbstractEventPlayer.Update) {
            AbstractEventPlayer.Update e = (AbstractEventPlayer.Update) event;
            if ((InventoryHelper.inUse != null && InventoryHelper.inUse != this))
                return;
            switch (e.getCall()) {
                case PRE:
                    if (InventoryHelper.inUse == this)
                        InventoryHelper.inUse = null;
                    if ((InventoryHelper.inUse != null && InventoryHelper.inUse != this))
                        break;
                    if (switchTimer.is(delaySwitch)) {
                        if (throwTimer.is(delayThrow)) {
                            if (!state || INTERACTIONS.isEmpty()) {
                                Container container = e.getPlayer().inventoryContainer;
                                Slot itemSlot = InventoryHelper.getPotionSlot(container, 9, container.inventorySlots.size() + 1, PotionTypes.HEALING, PotionTypes.STRONG_HEALING);
                                if (itemSlot != null && itemSlot.slotNumber != 45) {
                                    Slot offHandSlot = e.getPlayer().inventoryContainer.getSlot(45);
                                    if (offHandSlot.getHasStack() && isStackHealingPotion(offHandSlot.getStack()))
                                        break;
                                    INTERACTIONS.add(new InventoryInteraction(e.getPlayer().inventoryContainer.windowId, itemSlot.slotNumber, 0, ClickType.PICKUP, e.getPlayer()));
                                    INTERACTIONS.add(new InventoryInteraction(e.getPlayer().inventoryContainer.windowId, offHandSlot.slotNumber, 0, ClickType.PICKUP, e.getPlayer()));
                                    if (offHandSlot.getHasStack())
                                        INTERACTIONS.add(new InventoryInteraction(e.getPlayer().inventoryContainer.windowId, offHandSlot.slotNumber, 0, ClickType.PICKUP, e.getPlayer()));
                                    setState(true);
                                }
                            }
                        }
                        InventoryInteraction interaction = getNextInteraction();
                        if (interaction != null && state)
                            InventoryHelper.clickSlot(this, interaction.getWindow(), interaction.getSlot(), interaction.getButton(), interaction.getType(), interaction.getPlayer());
                        else
                            setState(false);
                    }
                    break;
                case POST:
                    if (switchTimer.is(delaySwitch))
                        switchTimer.reset();
                    break;
            }
        } else if (event instanceof AbstractEventPlayer.Walking) {
            AbstractEventPlayer.Walking e = (AbstractEventPlayer.Walking) event;
            ItemStack stack = e.getPlayer().getHeldItemOffhand();
            boolean isHealingPotion = isStackHealingPotion(stack);
            switch (e.getCall()) {
                case PRE:
                    prevPitch = e.getPlayer().rotationPitch;
                    
                    if (isHealingPotion)
                        e.getPlayer().rotationPitch = 90;
                    break;
                case POST:
                    if (isHealingPotion && e.getPlayer().getHealth() <= health) {
                        mc.playerController.processRightClick(e.getPlayer(), mc.world, EnumHand.OFF_HAND);
                        throwTimer.reset();
                    }
                    
                    e.getPlayer().rotationPitch = prevPitch;
                    break;
            }
        }
    }
    
    private boolean isStackHealingPotion(ItemStack stack) {
        return !stack.isEmpty() && stack.getItem() instanceof ItemSplashPotion && Arrays.asList(PotionTypes.HEALING, PotionTypes.STRONG_HEALING).contains(PotionUtils.getPotionFromItem(stack));
    }
}
