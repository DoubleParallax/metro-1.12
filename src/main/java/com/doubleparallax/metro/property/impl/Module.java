package com.doubleparallax.metro.property.impl;

import com.doubleparallax.metro.command.CommandHandler;
import com.doubleparallax.metro.command.impl.CommandProperty;
import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.impl.AbstractEventInput;
import com.doubleparallax.metro.event.impl.EventTick;
import com.doubleparallax.metro.property.Property;
import com.doubleparallax.metro.property.PropertyDescriptor;
import com.doubleparallax.metro.property.PropertyHandler;
import com.doubleparallax.metro.property.types.Keybind;
import com.doubleparallax.metro.util.Helper;
import com.doubleparallax.metro.util.filter.FilterSet;
import net.minecraft.client.entity.EntityPlayerSP;
import org.apache.commons.lang3.StringUtils;
import org.lwjgl.input.Keyboard;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Module extends Property implements Helper {
    
    public enum Category {
        ALL, AUTO, BUILD, COMBAT, MISC, MOVEMENT, PLAYER, RENDER, WORLD;
        
        public List<Module> getModules() {
            if (this == ALL)
                return PropertyHandler.getSubTypes(Module.class);
            return PropertyHandler.getSubTypes(Module.class).stream().filter(m -> Arrays.asList(m.category).contains(this)).collect(Collectors.toList());
        }
        
        @Override
        public String toString() {
            return StringUtils.capitalize(super.toString().toLowerCase());
        }
    }
    
    public enum Mode {
        HOLD, TOGGLE
    }
    
    //@formatter:off
    
    @PropertyDescriptor("enabled")
    public boolean enabled = false;
    
    @PropertyDescriptor("category")
    public Category[] category = new Category[] {};
    
    @PropertyDescriptor("bind")
    public Keybind bind = new Keybind().setKey(Keyboard.KEY_NONE);
    
        @PropertyDescriptor(parent = "bind", value = "mode")
        public Mode bindMode = Mode.TOGGLE;
    
    //@formatter:on
    
    private final transient FilterSet<String> EXCLUDED = new FilterSet<>(true, true);
    private transient boolean hasEnabled = enabled;
    
    protected Module(String id, Category... category) {
        super(id);
        
        this.category = category;
        
        CommandHandler.INSTANCE.register(new CommandProperty(this));
    }
    
    public FilterSet<String> getExcluded() {
        return EXCLUDED;
    }
    
    public boolean isExcluded(EntityPlayerSP player) {
        return EXCLUDED.contains(player.getGameProfile().getName());
    }
    
    private void onToggled() {
        hasEnabled = enabled;
        if (enabled)
            onEnabled();
        else
            onDisabled();
    }
    
    protected void onEnabled() {
    }
    
    protected void onDisabled() {
    }
    
    @Override
    public void onEvent(Event event) {
        if (event instanceof EventTick && event.getCall() == EventTick.Call.OUTER_POST) {
            if (enabled != hasEnabled)
                onToggled();
        } else if (event instanceof AbstractEventInput) {
            AbstractEventInput e = (AbstractEventInput) event;
            if (e.getKey() == bind.getKey()) {
                switch (bindMode) {
                    case TOGGLE:
                        if (e.getState())
                            enabled = !enabled;
                        break;
                    case HOLD:
                        enabled = e.getState();
                        break;
                }
            }
        }
    }
    
}
