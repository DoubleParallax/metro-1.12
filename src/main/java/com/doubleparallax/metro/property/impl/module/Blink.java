package com.doubleparallax.metro.property.impl.module;

import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.EventDescriptor;
import com.doubleparallax.metro.event.impl.EventPacket;
import com.doubleparallax.metro.event.impl.EventTick;
import com.doubleparallax.metro.property.impl.Module;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.CPacketPlayer;

import java.util.ArrayList;
import java.util.List;

@EventDescriptor({EventPacket.class})
public class Blink extends Module {
    
    //@formatter:off
    
    
    
    //@formatter:on
    
    private transient final List<Packet> PACKETS = new ArrayList<>();
    
    public Blink() {
        super("blink", Category.MOVEMENT, Category.PLAYER);
    }
    
    @Override
    public void onEnabled() {
        super.onEnabled();
        PACKETS.clear();
    }
    
    @Override
    public void onDisabled() {
        super.onDisabled();
        for (int i = PACKETS.size() - 1; i >= 0; i--)
            mc.player.connection.sendPacket(PACKETS.get(i));
        PACKETS.clear();
    }
    
    @Override
    public void onEvent(Event event) {
        super.onEvent(event);
        if (event instanceof EventTick) {
            EventTick e = (EventTick) event;
            switch (e.getCall()) {
                case OUTER_POST:
                    if (!enabled && mc.player != null) {
                        for (int i = 0; i < 1 && !PACKETS.isEmpty() && mc.getConnection() != null; i++) {
                            Packet packet = PACKETS.get(0);
                            PACKETS.remove(packet);
                            mc.getConnection().sendPacket(packet);
                        }
                    } else if (mc.player == null)
                        PACKETS.clear();
                    break;
            }
        }
        if (!enabled)
            return;
        if (event instanceof EventPacket) {
            EventPacket e = (EventPacket) event;
            switch (e.getCall()) {
                case SEND_PRE:
                    if (e.getPacket() instanceof CPacketPlayer) {
                        PACKETS.add(e.getPacket());
                        e.cancel();
                    }
                    break;
            }
        }
    }
}
