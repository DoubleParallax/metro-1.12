package com.doubleparallax.metro.property.impl.module;

import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.EventDescriptor;
import com.doubleparallax.metro.event.impl.AbstractEventPlayer;
import com.doubleparallax.metro.property.PropertyDescriptor;
import com.doubleparallax.metro.property.impl.Module;
import net.minecraft.client.entity.EntityPlayerSP;

@EventDescriptor({AbstractEventPlayer.Block.Break.class, AbstractEventPlayer.Block.Place.class})
public class AntiDesync extends Module {
    
    //@formatter:off
    
    @PropertyDescriptor("break")
    public boolean _break = true;
    
    @PropertyDescriptor("place")
    public boolean place = true;
    
    //@formatter:on
    
    public AntiDesync() {
        super("antidesync", Category.MISC);
    }
    
    @Override
    public void onEvent(Event event) {
        super.onEvent(event);
        if (!enabled)
            return;
        if (_break && event instanceof AbstractEventPlayer.Block.Break) {
            AbstractEventPlayer.Block.Break e = (AbstractEventPlayer.Block.Break) event;
            switch (e.getCall()) {
                case PRE:
                    EntityPlayerSP player = e.getPlayer();
                    if (!isExcluded(player))
                        e.cancel();
                    break;
            }
        } else if (place && event instanceof AbstractEventPlayer.Block.Place) {
            AbstractEventPlayer.Block.Place e = (AbstractEventPlayer.Block.Place) event;
            switch (e.getCall()) {
                case POST:
                    EntityPlayerSP player = e.getPlayer();
                    if (!isExcluded(player)) {
                        player.swingArm(e.getHand());
                        e.cancel();
                    }
                    break;
            }
        }
    }
}
