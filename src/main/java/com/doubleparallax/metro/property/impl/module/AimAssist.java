package com.doubleparallax.metro.property.impl.module;

import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.EventDescriptor;
import com.doubleparallax.metro.event.impl.AbstractEventInput;
import com.doubleparallax.metro.event.impl.AbstractEventPlayer;
import com.doubleparallax.metro.event.impl.EventTick;
import com.doubleparallax.metro.property.PropertyDescriptor;
import com.doubleparallax.metro.property.impl.Module;
import com.doubleparallax.metro.property.types.Range;
import com.doubleparallax.metro.util.MathHelper;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;

@EventDescriptor({EventTick.class, AbstractEventInput.class, AbstractEventPlayer.Walking.class})
public class AimAssist extends Module {
    
    public enum Direction {
        HORIZONTAL, VERTICAL, BOTH
    }
    
    public enum Turn {
        LINEAR, SMOOTH
    }
    
    //@formatter:off
    
    @PropertyDescriptor(value = "range", range = {1, 14, 0.5})
    public double range = 7;
    
    @PropertyDescriptor("fov")
    public transient Void fov;
    
        @PropertyDescriptor(parent = "fov", value = "horizontal", range = {10, 90, 0})
        public double fovHorizontal = 60;
    
        @PropertyDescriptor(parent = "fov", value = "vertical", range = {10, 90, 0})
        public double fovVertical = 30;
        
    @PropertyDescriptor("direction")
    public Direction direction = Direction.BOTH;
    
    @PropertyDescriptor(value = "jitter", range = {0, 20, 1})
    public Range jitter = new Range().setMin(0).setMax(10);
        
    @PropertyDescriptor("turn")
    public Turn turn = Turn.SMOOTH;
    
        @PropertyDescriptor(parent = "turn", value = "speed", range = {0, 40, 0.5})
        public int turnSpeed = 10;
    
    //@formatter:on
    
    public AimAssist() {
        super("aimassist", Category.COMBAT);
    }
    
    @Override
    public void onEvent(Event event) {
        super.onEvent(event);
        if (!enabled)
            return;
        if (event instanceof AbstractEventPlayer.Walking) {
            AbstractEventPlayer.Walking e = (AbstractEventPlayer.Walking) event;
            switch (e.getCall()) {
                case POST:
                    EntityPlayerSP player = e.getPlayer();
                    if (isExcluded(player))
                        break;
                    
                    EntityLivingBase target = (EntityLivingBase) mc.world.getLoadedEntityList().stream().filter(entity -> {
                        double[] rotation = MathHelper.getDirection(player.getPositionEyes(mc.getRenderPartialTicks()), entity.getPositionVector().addVector(0, entity.height / 2., 0));
                        return entity instanceof EntityLivingBase &&
                                entity != player &&
                                entity.getDistanceToEntity(player) < range &&
                                Math.abs(MathHelper.wrapAngle(player.rotationYaw, rotation[0], 360)) <= fovHorizontal &&
                                Math.abs(MathHelper.wrapAngle(player.rotationPitch, rotation[1], 360)) <= fovVertical;
                    }).sorted((entityA, entityB) -> {
                        double[] rotationA = MathHelper.getDirection(player.getPositionEyes(mc.getRenderPartialTicks()), entityA.getPositionVector().addVector(0, entityA.height / 2., 0));
                        double[] rotationB = MathHelper.getDirection(player.getPositionEyes(mc.getRenderPartialTicks()), entityB.getPositionVector().addVector(0, entityB.height / 2., 0));
                        Vec3d vecA = MathHelper.getVectorForRotation(rotationA[0], rotationA[1]);
                        Vec3d vecB = MathHelper.getVectorForRotation(rotationB[0], rotationB[1]);
                        Vec3d vecC = MathHelper.getVectorForRotation(player.rotationYaw, player.rotationPitch);
                        Double angleA = MathHelper.getVectorDifferenceByAngle(vecC, vecA);
                        Double angleB = MathHelper.getVectorDifferenceByAngle(vecC, vecB);
                        return angleA.compareTo(angleB);
                    }).findFirst().orElse(null);
                    
                    if (target != null) {
                        double[] direction = MathHelper.getDirection(player.getPositionEyes(mc.getRenderPartialTicks()), target.getPositionVector().addVector(0, target.height / 2., 0));
                        switch (this.direction) {
                            case HORIZONTAL:
                                direction[1] = player.rotationPitch;
                                break;
                            case VERTICAL:
                                direction[0] = player.rotationYaw;
                                break;
                        }
                        double yawDif = -MathHelper.wrapAngle(player.rotationYaw, direction[0], 360);
                        double pitchDif = -MathHelper.wrapAngle(player.rotationPitch, direction[1], 360);
                        if (mc.objectMouseOver != null && mc.objectMouseOver.typeOfHit == RayTraceResult.Type.ENTITY && mc.objectMouseOver.entityHit instanceof EntityLivingBase)
                            yawDif = pitchDif = 0;
                        yawDif += (jitter.getMin() + (Math.random() * (jitter.getMax() - jitter.getMin()))) * (Math.random() > 0.5 ? 1 : -1);
                        pitchDif += (jitter.getMin() + (Math.random() * (jitter.getMax() - jitter.getMin()))) * (Math.random() > 0.5 ? 1 : -1);
                        player.rotationYaw += turn == Turn.LINEAR ? (yawDif > turnSpeed ? turnSpeed : yawDif < -turnSpeed ? -turnSpeed : yawDif) : (yawDif / turnSpeed);
                        player.rotationPitch += turn == Turn.LINEAR ? (pitchDif > turnSpeed ? turnSpeed : pitchDif < -turnSpeed ? -turnSpeed : pitchDif) : (pitchDif / turnSpeed);
                    }
                    break;
            }
        }
    }
}
