package com.doubleparallax.metro.property.impl.module;

import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.EventDescriptor;
import com.doubleparallax.metro.event.impl.AbstractEventPlayer;
import com.doubleparallax.metro.property.PropertyDescriptor;
import com.doubleparallax.metro.property.impl.module.impl.InventoryManager;
import com.doubleparallax.metro.util.ItemHelper;
import com.doubleparallax.metro.util.inventory.InventoryHelper;
import com.doubleparallax.metro.util.inventory.InventoryInteraction;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;

@EventDescriptor({AbstractEventPlayer.Update.class})
public class AutoArmor extends InventoryManager {
    
    //@formatter:off
    
    @PropertyDescriptor(value = "delay", range = {1, 20, 1})
    public int delay = 4;
    
    //@formatter:on
    
    private transient int piece, index;
    
    public AutoArmor() {
        super("autoarmor", Category.AUTO, Category.COMBAT);
    }
    
    @Override
    public void onEvent(Event event) {
        super.onEvent(event);
        if (!enabled)
            return;
        if (event instanceof AbstractEventPlayer.Update) {
            AbstractEventPlayer.Update e = (AbstractEventPlayer.Update) event;
            switch (e.getCall()) {
                case PRE:
                    if (InventoryHelper.inUse == this)
                        InventoryHelper.inUse = null;
                    if ((InventoryHelper.inUse != null && InventoryHelper.inUse != this))
                        break;
                    if (index == 0) {
                        if (!state || INTERACTIONS.isEmpty()) {
                            Container container = e.getPlayer().inventoryContainer;
                            Slot itemSlot = InventoryHelper.getBestArmorSlot(container, 5, container.inventorySlots.size() - 1, s -> (Item.getIdFromItem(s.getStack().getItem()) - 2) % 4 == piece, (s1, s2) -> ItemHelper.getArmorValue(s2.getStack()) - ItemHelper.getArmorValue(s1.getStack()));
                            if (itemSlot != null && itemSlot.slotNumber >= 9) {
                                Slot armorSlot = container.getSlot(piece + 5);
                                if (armorSlot.getHasStack()) {
                                    Slot emptySlot = InventoryHelper.getNextEmptySlot(container, 9, container.inventorySlots.size() - 1);
                                    if (emptySlot == null) {
                                        INTERACTIONS.add(new InventoryInteraction(container.windowId, piece + 5, 0, ClickType.PICKUP, e.getPlayer()));
                                        INTERACTIONS.add(new InventoryInteraction(container.windowId, itemSlot.slotNumber, 0, ClickType.QUICK_MOVE, e.getPlayer()));
                                        INTERACTIONS.add(new InventoryInteraction(container.windowId, itemSlot.slotNumber, 0, ClickType.PICKUP, e.getPlayer()));
                                    } else
                                        INTERACTIONS.add(new InventoryInteraction(container.windowId, piece + 5, 0, ClickType.QUICK_MOVE, e.getPlayer()));
                                } else {
                                    INTERACTIONS.add(new InventoryInteraction(container.windowId, itemSlot.slotNumber, 0, ClickType.QUICK_MOVE, e.getPlayer()));
                                    piece++;
                                }
                                setState(true);
                            } else
                                piece++;
                        }
                        InventoryInteraction interaction = getNextInteraction();
                        if (interaction != null && state)
                            InventoryHelper.clickSlot(this, interaction.getWindow(), interaction.getSlot(), interaction.getButton(), interaction.getType(), interaction.getPlayer());
                        else
                            setState(false);
                    }
                    piece %= 4;
                    index++;
                    index %= delay;
                    break;
            }
        }
    }
}
