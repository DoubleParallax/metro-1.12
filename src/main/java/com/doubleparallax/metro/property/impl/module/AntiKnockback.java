package com.doubleparallax.metro.property.impl.module;

import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.EventDescriptor;
import com.doubleparallax.metro.event.impl.AbstractEventEntity;
import com.doubleparallax.metro.property.PropertyDescriptor;
import com.doubleparallax.metro.property.impl.Module;
import com.doubleparallax.metro.property.types.Range;
import net.minecraft.client.entity.EntityPlayerSP;

@EventDescriptor({AbstractEventEntity.NetVelocity.class})
public class AntiKnockback extends Module {
    
    //@formatter:off
    
    @PropertyDescriptor(value = "multiplier", range = {0, 1, 0.05})
    public Range multiplier = new Range().setMin(0).setMax(0.5);
    
    //@formatter:on
    
    public AntiKnockback() {
        super("antiknockback", Category.COMBAT, Category.MOVEMENT);
    }
    
    @Override
    public void onEvent(Event event) {
        super.onEvent(event);
        if (!enabled)
            return;
        if (event instanceof AbstractEventEntity.NetVelocity) {
            AbstractEventEntity.NetVelocity e = (AbstractEventEntity.NetVelocity) event;
            switch (e.getCall()) {
                case PRE:
                    if (e.getEntity() instanceof EntityPlayerSP && !isExcluded((EntityPlayerSP) e.getEntity())) {
                        double mult = multiplier.getMin() + (Math.random() * (multiplier.getMax() - multiplier.getMin()));
                        e.setX(e.getX() * mult);
                        e.setY(e.getY() * mult);
                        e.setZ(e.getZ() * mult);
                        e.cancel();
                    }
                    break;
            }
        }
    }
}
