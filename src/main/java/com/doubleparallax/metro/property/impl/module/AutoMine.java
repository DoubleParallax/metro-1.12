package com.doubleparallax.metro.property.impl.module;

import com.doubleparallax.metro.event.Event;
import com.doubleparallax.metro.event.EventDescriptor;
import com.doubleparallax.metro.event.impl.AbstractEventPlayer;
import com.doubleparallax.metro.property.impl.Module;

@EventDescriptor({AbstractEventPlayer.Update.class})
public class AutoMine extends Module {

    public AutoMine() {
        super("automine", Category.AUTO, Category.BUILD);
    }
    
    @Override
    public void onEvent(Event event) {
        super.onEvent(event);
        if (!enabled)
            return;
        if (event instanceof AbstractEventPlayer.Update) {
            AbstractEventPlayer.Update e = (AbstractEventPlayer.Update) event;
            switch (e.getCall()) {
                case PRE:
                    if (mc.objectMouseOver != null)
                        mc.playerController.onPlayerDamageBlock(mc.objectMouseOver.getBlockPos(), mc.objectMouseOver.sideHit);
                    break;
            }
        }
    }
}
