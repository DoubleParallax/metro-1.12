package com.doubleparallax.metro.property.impl.module;

import com.doubleparallax.metro.gui.GuiHandler;
import com.doubleparallax.metro.property.PropertyDescriptor;
import com.doubleparallax.metro.property.impl.Module;

public class Overhaul extends Module {
    
    //@formatter:off
    
    @PropertyDescriptor("theme")
    public GuiHandler.ThemeOverride theme = GuiHandler.ThemeOverride.METRO;
    
    //@formatter:on
    
    public Overhaul() {
        super("overhaul", Category.RENDER);
    }
}
