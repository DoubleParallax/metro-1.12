package com.doubleparallax.metro.property.impl.module.impl;

import com.doubleparallax.metro.property.impl.Module;
import com.doubleparallax.metro.util.inventory.InventoryInteraction;

import java.util.ArrayList;
import java.util.List;

public abstract class InventoryManager extends Module {
    
    protected transient final List<InventoryInteraction> INTERACTIONS = new ArrayList<>();
    protected transient boolean state;
    
    protected InventoryManager(String id, Category... category) {
        super(id, category);
    }
    
    protected void setState(boolean state) {
        this.state = state;
        if (!state)
            INTERACTIONS.clear();
    }
    
    protected InventoryInteraction getNextInteraction() {
        if (INTERACTIONS.isEmpty())
            return null;
        InventoryInteraction next = INTERACTIONS.get(0);
        INTERACTIONS.remove(next);
        return next;
    }
    
}
