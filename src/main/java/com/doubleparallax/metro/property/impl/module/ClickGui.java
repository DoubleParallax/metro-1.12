package com.doubleparallax.metro.property.impl.module;

import com.doubleparallax.metro.event.EventDescriptor;
import com.doubleparallax.metro.event.impl.EventRenderScreen;
import com.doubleparallax.metro.gui.screen.GuiModules;
import com.doubleparallax.metro.property.PropertyDescriptor;
import com.doubleparallax.metro.property.impl.Module;

@EventDescriptor({EventRenderScreen.class})
public class ClickGui extends Module {
    
    public enum Theme {
        METRO, VATIC, DELUGE;
    }
    
    //@formatter:off
    
    @PropertyDescriptor("theme")
    public Theme theme = Theme.METRO;
    
    //@formatter:on
    
    public ClickGui() {
        super("clickgui", Category.RENDER);
    }
    
    @Override
    public void onEnabled() {
        super.onEnabled();
        mc.displayGuiScreen(new GuiModules());
        enabled = false;
    }
}
