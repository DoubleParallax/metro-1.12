package com.doubleparallax.metro.property;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PropertyDescriptor {
    
    String parent() default "";
    
    String value() default "";
    
    double[] range() default {0, 0, 0};
    
}
