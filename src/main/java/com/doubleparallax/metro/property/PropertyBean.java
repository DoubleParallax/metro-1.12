package com.doubleparallax.metro.property;

import java.util.Map;

public class PropertyBean {
    
    private String name, desc;
    private String[] alias;
    private Map<String, PropertyBean> children;
    
    public String getName() {
        return name;
    }
    
    public String getDesc() {
        return desc;
    }
    
    public String[] getAlias() {
        return alias;
    }
    
    public Map<String, PropertyBean> getChildren() {
        return children;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public void setAlias(String[] alias) {
        this.alias = alias;
    }
    
    public void setChildren(Map<String, PropertyBean> children) {
        this.children = children;
    }
    
}
