package com.doubleparallax.metro;

import com.doubleparallax.metro.command.CommandHandler;
import com.doubleparallax.metro.event.IEventListener;
import com.doubleparallax.metro.gui.GuiHandler;
import com.doubleparallax.metro.property.Property;
import com.doubleparallax.metro.property.PropertyHandler;
import com.doubleparallax.metro.util.Helper;
import com.doubleparallax.metro.util.UpdateHelper;

public enum Client implements Helper, IEventListener {
    
    INSTANCE;
    
    public final String PROJECT = "Metro";
    public final byte[] VERSION = {0, 0, 1};
    public final String DEFAULT_VERSION = String.format("%s.%s.%s", VERSION[0], VERSION[1], VERSION[2]);
    public final String DEFAULT_PROJECT = String.format("%s v%s", PROJECT, DEFAULT_VERSION);
    
    /*
    * TODO
    * Main menu
    * Other menus
    * Hack menu
    *       Tooltips
    *
    * Menu components (textbox, slider etc.)
    *
    * ADDONS (donation perks)
    *       Capes
    *       Ears
    *       Upsidedown
    *       Size
    *       Particles
    *       Custom head
    *
    * Alt manager
    * Bots
    * Commands
    * Events
    * Macros
    *
    * Spice up chat prints
    *
    * COMPLETE: Aim Assist - combat
    * COMPLETE: Anti Desync - misc
    * COMPLETE: Anti Knockback - combat, movement
    * COMPLETE: Auto Armor - auto, combat
    * COMPLETE: Auto Potion - auto, combat
    * SEMI: Auto Soup - auto, combat
    * COMPLETE: Auto Tool - auto, build, player
    * COMPLETE: Auto Mine - auto, build, player
    * Auto Eat - auto, player
    * COMPLETE: Blink - movement, player
    * Chest Stealer - auto, player
    * Click GUI - render
    * ESP - render
    * Damage - player
    * Fast Climb - movement
    * Fast Use - player
    * Fly - movement
    * Follow - auto, movement, player
    * Free Cam - movement, render
    * Friends - combat, misc, player, render
    * Full Bright - render, world
    * Glide - movement
    * Health Display - render
    * Horse Jump - movement
    * Interact - auto, player
    * Inventory Move - movement
    * Jesus - movement, player
    * Kill Aura - auto, combat
    * Memer - misc, render
    * Name Protect - combat, player, render
    * Name Tags - render
    * No Clip - movement
    * No Reset - render
    * No Slow Down - movement
    * Nuker - auto, build, player, world
    * Overhaul - render
    * Parkour - movement
    * Phase - movement
    * Plugins - misc
    * Profanity Filter - misc
    * Regen - player
    * Retard - movement, misc, player
    * Screenshot - misc, render
    * Sneak - movement, player
    * Spam - misc
    * Spectate - misc, player, render
    * Speed - movement
    * Speed Mine - build, player
    * Sprint - movement
    * Status HUD - render
    * Step - movement
    * Survival Control - auto, build, player, world
    * Teleport - movement
    * Timer - movement, world
    * Tower - auto, build, movement, world
    * Tracer - render
    * Vanilla Teleport - movement
    * View Clip - render
    * Wall Hack - render
    * Waypoints - render, world
    * Xray - render, world
    * Zoot - player
    *
    * Look into render layers, multiple layers passed into shader, one determines blurred areas of output
    *
    * Upload to git
    *
    * */
    
    public void init() {
        UpdateHelper.doUpdate();
        
        register();
        
        GuiHandler.INSTANCE.init();
        CommandHandler.INSTANCE.init();
        
        PropertyHandler.init();
        
        Runtime.getRuntime().addShutdownHook(new Thread(() -> PropertyHandler.getProperties().forEach(Property::save)));
    }
    
}
