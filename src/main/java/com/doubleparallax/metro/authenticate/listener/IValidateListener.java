package com.doubleparallax.metro.authenticate.listener;


public interface IValidateListener {
    
    void response(boolean passed);
    
}
