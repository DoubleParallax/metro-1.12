package com.doubleparallax.metro.authenticate.listener;


public interface ISignoutListener {

    void response(boolean passed);

}
