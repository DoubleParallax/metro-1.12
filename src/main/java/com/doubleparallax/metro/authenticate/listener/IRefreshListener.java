package com.doubleparallax.metro.authenticate.listener;

import com.doubleparallax.metro.authenticate.response.RefreshResponse;

public interface IRefreshListener {

    void response(RefreshResponse response);

}
