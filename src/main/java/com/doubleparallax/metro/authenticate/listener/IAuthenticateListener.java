package com.doubleparallax.metro.authenticate.listener;

import com.doubleparallax.metro.authenticate.response.AuthenticateResponse;

public interface IAuthenticateListener {

    void response(AuthenticateResponse response);

}
