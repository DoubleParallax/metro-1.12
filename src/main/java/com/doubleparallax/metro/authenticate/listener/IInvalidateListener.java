package com.doubleparallax.metro.authenticate.listener;


public interface IInvalidateListener {

    void response(boolean passed);

}
