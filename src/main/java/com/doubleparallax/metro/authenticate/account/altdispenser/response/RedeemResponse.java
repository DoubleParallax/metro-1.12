package com.doubleparallax.metro.authenticate.account.altdispenser.response;


public class RedeemResponse {

    private boolean success;
    private String status_message;
    private Alt alt;
    private KeyOwner key_owner;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getStatus_message() {
        return status_message;
    }

    public void setStatus_message(String status_message) {
        this.status_message = status_message;
    }

    public Alt getAlt() {
        return alt;
    }

    public void setAlt(Alt alt) {
        this.alt = alt;
    }

    public KeyOwner getKey_owner() {
        return key_owner;
    }

    public void setKey_owner(KeyOwner key_owner) {
        this.key_owner = key_owner;
    }

    public static class Alt {
        private String username;
        private String client_token;
        private String access_token;
        private String uuid;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getClient_token() {
            return client_token;
        }

        public void setClient_token(String client_token) {
            this.client_token = client_token;
        }

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }
    }

    public static class KeyOwner {
        private String username;
        private int user_id;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }
    }

}
