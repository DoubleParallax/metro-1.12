package com.doubleparallax.metro.authenticate.account.altdispenser.payload;


public class ValidateKeyPayload {

    private String application_name;
    private String application_version;
    private String apikey;
    private String hwid;
    private String os;

    public ValidateKeyPayload(String application_name, String application_version, String apikey, String hwid) {
        this.setApplication_name(application_name);
        this.setApplication_version(application_version);
        this.setApikey(apikey);
        this.setHwid(hwid);
    }

    public ValidateKeyPayload setOs(String os) {
        this.os = os;
        return this;
    }

    public String getApplication_name() {
        return application_name;
    }

    public void setApplication_name(String application_name) {
        this.application_name = application_name;
    }

    public String getApplication_version() {
        return application_version;
    }

    public void setApplication_version(String application_version) {
        this.application_version = application_version;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getHwid() {
        return hwid;
    }

    public void setHwid(String hwid) {
        this.hwid = hwid;
    }

    public String getOs() {
        return os;
    }
}
