package com.doubleparallax.metro.authenticate.account.altdispenser.response;


public class ValidateKeyResponse {

    private boolean success;
    private String status_message;
    private User user;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getStatus_message() {
        return status_message;
    }

    public void setStatus_message(String status_message) {
        this.status_message = status_message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public static class User {
        private int id;
        private String username;
        private String plan;
        private String gravatar_hash;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPlan() {
            return plan;
        }

        public void setPlan(String plan) {
            this.plan = plan;
        }

        public String getGravatar_hash() {
            return gravatar_hash;
        }

        public void setGravatar_hash(String gravatar_hash) {
            this.gravatar_hash = gravatar_hash;
        }
    }

}
