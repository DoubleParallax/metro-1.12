package com.doubleparallax.metro.authenticate.account;

import com.doubleparallax.metro.Client;
import com.doubleparallax.metro.authenticate.Authenticator;
import com.doubleparallax.metro.authenticate.account.altdispenser.AltDispenser;
import com.doubleparallax.metro.authenticate.account.altdispenser.payload.RefreshTokenPayload;
import com.doubleparallax.metro.authenticate.account.altdispenser.payload.ValidateTokenPayload;
import com.doubleparallax.metro.authenticate.account.altdispenser.response.RefreshTokenResponse;
import com.doubleparallax.metro.authenticate.account.altdispenser.response.ValidateTokenResponse;
import com.doubleparallax.metro.authenticate.payload.AuthenticatePayload;
import com.doubleparallax.metro.authenticate.payload.RefreshPayload;
import com.doubleparallax.metro.authenticate.payload.ValidatePayload;
import com.doubleparallax.metro.util.Callback;
import com.doubleparallax.metro.util.Helper;
import com.doubleparallax.metro.util.SystemHelper;
import com.doubleparallax.metro.util.io.Request;
import com.doubleparallax.metro.util.render.alert.AlertHelper;
import net.minecraft.util.Session;


public class Account implements Helper {

    private String login = "", password = "", username = "";
    private boolean generated, pending, offline;
    private StoredSession storedSession;

    public Account() {
        storedSession = new StoredSession();
    }

    public Account setLogin(String login) {
        this.login = login;
        return this;
    }

    public Account setPassword(String password) {
        this.password = password;
        offline = false;
        return this;
    }

    public Account setUsername(String username) {
        this.username = username;
        return this;
    }

    public Account setStoredSession(StoredSession storedSession) {
        this.storedSession = storedSession;
        return this;
    }

    public Account setSession(Session session) {
        this.storedSession.setSession(session);
        return this;
    }

    public Account setClientToken(String clientToken) {
        this.storedSession.setClientToken(clientToken);
        return this;
    }

    public Account setGenerated(boolean generated) {
        this.generated = generated;
        return this;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public StoredSession getStoredSession() {
        return storedSession;
    }

    public boolean isGenerated() {
        return generated;
    }

    public void authenticate(final Callback<Boolean> callback) {
        if (!pending) {
            pending = true;
            final String login = getDisplay();
            if (storedSession.getSession() != null && !offline) {
                AlertHelper.info("Validating user account session '%s'", login);
                internalValidate(response -> {
                    if (!response) {
                        AlertHelper.info("Refreshing user account session '%s'", login);
                        internalRefresh(callback);
                    } else {
                        username = storedSession.getSession().getUsername();
                        pending = false;
                        callback.response(true);
                        AlertHelper.success("Successfully authenticated user '%s'", login);
                        /*try {
                            StorageAccounts.save();
                        } catch (JsonProcessingException e) {
                        }*/
                    }
                });
            } else {
                if (password != null && !password.isEmpty()) {
                    AlertHelper.info("Authenticating user account '%s'", login);
                    internalAuthenticate(callback);
                } else {
                    offline = true;
                    storedSession.setSession(new Session(login, "", "", "legacy"));
                    username = storedSession.getSession().getUsername();
                    pending = false;
                    callback.response(true);
                    AlertHelper.success("Created offline session as '%s'", login);
                    /*try {
                        StorageAccounts.save();
                    } catch (JsonProcessingException e) {
                    }*/
                }
            }
        }
    }

    private void internalAuthenticate(final Callback<Boolean> callback) {
        storedSession.setSession(null);
        new Thread(() -> {
            Authenticator.authenticate(response -> {
                if (response != null) {
                    if (response.getError() == null) {
                        setSession(new Session(response.getSelectedProfile().getName(),
                                response.getSelectedProfile().getId(), response.getAccessToken(), "legacy"));
                        setClientToken(response.getClientToken());

                        callback.response(true);

                        username = storedSession.getSession().getUsername();

                        AlertHelper.success("Successfully authenticated user '%s'", storedSession.getSession().getUsername());
                        /*try {
                            StorageAccounts.save();
                        } catch (JsonProcessingException e) {
                        }*/
                    } else {
                        callback.response(false);
                        AlertHelper.severe("%s: %s", response.getError(), response.getErrorMessage());
                    }
                } else {
                    callback.response(false);
                    AlertHelper.severe("There was an issue with the authentication response");
                }
            }, new AuthenticatePayload(login, password));
            pending = false;
        }).start();
    }

    private void internalValidate(final Callback<Boolean> callback) {
        new Thread(() -> {
            if (generated) {
                AltDispenser.request(Request.AltDispenserApi.Mojang.VALIDATE.getUrl(), response -> {
                    callback.response(response != null);
                }, new ValidateTokenPayload(Client.INSTANCE.PROJECT, Client.INSTANCE.DEFAULT_VERSION,
                        storedSession.getSession().getToken(),
                        SystemHelper.getHWID()).setOs(SystemHelper.getOS()), ValidateTokenResponse.class);
            } else {
                Authenticator.validate(callback::response, new ValidatePayload(storedSession.getSession().getToken()));
            }
            pending = false;
        }).start();
    }

    private void internalRefresh(final Callback<Boolean> callback) {
        new Thread(() -> {
            if (generated) {
                AltDispenser.request(Request.AltDispenserApi.Mojang.REFRESH.getUrl(), response -> {
                    if (response != null) {
                        if (response.getError() == null) {
                            setSession(new Session(response.getSelectedProfile().getName(),
                                    response.getSelectedProfile().getId(), response.getAccessToken(), "legacy"));
                            setClientToken(response.getClientToken());

                            callback.response(true);

                            username = storedSession.getSession().getUsername();

                            AlertHelper.success("Successfully refreshed session for '%s'", storedSession.getSession().getUsername());
                            /*try {
                                StorageAccounts.save();
                            } catch (JsonProcessingException e) {
                            }*/
                        } else {
                            callback.response(false);
                            AlertHelper.severe("%s: %s", response.getError(), response.getErrorMessage());
                        }
                    }
                }, new RefreshTokenPayload(Client.INSTANCE.PROJECT, Client.INSTANCE.DEFAULT_VERSION,
                        storedSession.getSession().getToken(),
                        storedSession.getClientToken(), SystemHelper.getHWID()), RefreshTokenResponse.class);
            } else {
                Authenticator.refresh(response -> {
                    if (response != null) {
                        if (response.getError() == null) {
                            setSession(new Session(response.getSelectedProfile().getName(),
                                    response.getSelectedProfile().getId(), response.getAccessToken(), "legacy"));
                            setClientToken(response.getClientToken());

                            callback.response(true);

                            username = storedSession.getSession().getUsername();

                            AlertHelper.success("Successfully refreshed session for '%s'", storedSession.getSession().getUsername());
                            /*try {
                                StorageAccounts.save();
                            } catch (JsonProcessingException e) {
                            }*/
                        } else {
                            callback.response(false);
                            AlertHelper.severe("%s: %s", response.getError(), response.getErrorMessage());
                            AlertHelper.info("Authenticating user account '%s'", login);
                            internalAuthenticate(callback);
                        }
                    } else {
                        callback.response(false);
                        AlertHelper.severe("There was an issue with the refresh response");
                        if (password != null && !password.isEmpty()) {
                            AlertHelper.info("Authenticating user account '%s'", login);
                            internalAuthenticate(callback);
                        } else {
                            AlertHelper.info("Failed to validate account, proceed with caution", login);
                            callback.response(true);
                        }
                    }
                }, new RefreshPayload(storedSession.getSession().getToken(), storedSession.getClientToken()));
            }
            pending = false;
        }).start();
    }

    public String getDisplay() {
        String out = username;
        if (out.isEmpty()) {
            out = login;
            if (out != null && !out.isEmpty()) {
                int index = out.indexOf("@");
                if (index != -1) {
                    index--;
                    out = String.format("%s*%s", out.substring(0, 1), out.substring(index));
                }
            }
        }
        return out;
    }

}
