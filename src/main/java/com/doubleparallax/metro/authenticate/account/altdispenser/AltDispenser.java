package com.doubleparallax.metro.authenticate.account.altdispenser;

import com.doubleparallax.metro.authenticate.account.altdispenser.listener.IListener;
import com.doubleparallax.metro.property.Property;
import com.doubleparallax.metro.util.io.Request;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AltDispenser extends Property {

    private static transient final ObjectMapper MAPPER =
            new ObjectMapper().enable(MapperFeature.PROPAGATE_TRANSIENT_MARKER).disable(
                    DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    public String key = "";

    public AltDispenser() {
        super("altdispenser");
    }

    public static <T, U> void request(String url, IListener<U> listener, T payload, Class<U> type) {
        Request request;
        try {
            request = new Request(url, Request.Method.POST)
                    .setHeader(Request.Header.ACCEPT, "application/json")
                    .setHeader(Request.Header.CONTENT_TYPE, "application/json")
                    .setPayload(MAPPER.writeValueAsString(payload));
        } catch (JsonProcessingException e) {
            listener.response(null);
            return;
        }

        String post = request.request();

        if (!post.isEmpty()) {
            try {
                U response = MAPPER.readValue(post, type);
                listener.response(response);
            } catch (Exception e) {
                listener.response(null);
            }
        } else {
            listener.response(null);
        }
    }

}
