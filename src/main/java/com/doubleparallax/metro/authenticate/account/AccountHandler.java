package com.doubleparallax.metro.authenticate.account;

import com.google.common.collect.Lists;

import java.util.Comparator;
import java.util.List;


public class AccountHandler {

    private static final List<Account> ACCOUNT_LIST = Lists.newArrayList();

    public static boolean add(Account account) {
        return !contains(account) && ACCOUNT_LIST.add(account);
    }

    public static boolean remove(Account account) {
        return ACCOUNT_LIST.remove(account);
    }

    public static boolean contains(Account account) {
        return ACCOUNT_LIST.contains(account);
    }

    public static Account get(int index) {
        return ACCOUNT_LIST.get(index);
    }

    public static void clear() {
        ACCOUNT_LIST.clear();
    }

    public static void sort() {
        ACCOUNT_LIST.sort(Comparator.comparing(o -> (o.getUsername().isEmpty() ? o.getLogin() : o.getUsername())));
    }

    public static List<Account> getAccountList() {
        return ACCOUNT_LIST;
    }

}
