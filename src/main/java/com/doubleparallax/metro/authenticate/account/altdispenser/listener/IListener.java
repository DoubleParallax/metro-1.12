package com.doubleparallax.metro.authenticate.account.altdispenser.listener;


public interface IListener<T> {

    void response(T response);

}
