package com.doubleparallax.metro.authenticate.account.altdispenser.response;

import java.util.List;


public class GenerateResponse {

    private boolean success;
    private boolean captcha_required;
    private String status_message;
    private Alt alt;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isCaptcha_required() {
        return captcha_required;
    }

    public void setCaptcha_required(boolean captcha_required) {
        this.captcha_required = captcha_required;
    }

    public String getStatus_message() {
        return status_message;
    }

    public void setStatus_message(String status_message) {
        this.status_message = status_message;
    }

    public Alt getAlt() {
        return alt;
    }

    public void setAlt(Alt alt) {
        this.alt = alt;
    }

    public static class Alt {
        private String key;
        private String displayname;
        private String skin_url;
        private boolean mojang_cape;
        private boolean optifine_cape;
        private boolean favourite;
        private List banchecker;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getDisplayname() {
            return displayname;
        }

        public void setDisplayname(String displayname) {
            this.displayname = displayname;
        }

        public String getSkin_url() {
            return skin_url;
        }

        public void setSkin_url(String skin_url) {
            this.skin_url = skin_url;
        }

        public boolean isMojang_cape() {
            return mojang_cape;
        }

        public void setMojang_cape(boolean mojang_cape) {
            this.mojang_cape = mojang_cape;
        }

        public boolean isOptifine_cape() {
            return optifine_cape;
        }

        public void setOptifine_cape(boolean optifine_cape) {
            this.optifine_cape = optifine_cape;
        }

        public boolean isFavourite() {
            return favourite;
        }

        public void setFavourite(boolean favourite) {
            this.favourite = favourite;
        }

        public List getBanchecker() {
            return banchecker;
        }

        public void setBanchecker(List banchecker) {
            this.banchecker = banchecker;
        }
    }

}
