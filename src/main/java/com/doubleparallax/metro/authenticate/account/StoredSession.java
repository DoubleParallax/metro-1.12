package com.doubleparallax.metro.authenticate.account;

import net.minecraft.util.Session;


public class StoredSession {

    private Session session;
    private String clientToken;

    public StoredSession setSession(Session session) {
        this.session = session;
        return this;
    }

    public StoredSession setClientToken(String clientToken) {
        this.clientToken = clientToken;
        return this;
    }

    public Session getSession() {
        return session;
    }

    public String getClientToken() {
        return clientToken;
    }

}
