package com.doubleparallax.metro.authenticate.account.altdispenser.payload;


public class RefreshTokenPayload {

    private String application_name;
    private String application_version;
    private String accessToken;
    private String clientToken;
    private String hwid;
    private String os;

    public RefreshTokenPayload(String application_name, String application_version, String accessToken,
            String clientToken, String hwid) {
        this.setApplication_name(application_name);
        this.setApplication_version(application_version);
        this.setAccessToken(accessToken);
        this.setClientToken(clientToken);
        this.setHwid(hwid);
    }

    public RefreshTokenPayload setOs(String os) {
        this.os = os;
        return this;
    }

    public String getApplication_name() {
        return application_name;
    }

    public void setApplication_name(String application_name) {
        this.application_name = application_name;
    }

    public String getApplication_version() {
        return application_version;
    }

    public void setApplication_version(String application_version) {
        this.application_version = application_version;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getClientToken() {
        return clientToken;
    }

    public void setClientToken(String clientToken) {
        this.clientToken = clientToken;
    }

    public String getHwid() {
        return hwid;
    }

    public void setHwid(String hwid) {
        this.hwid = hwid;
    }

    public String getOs() {
        return os;
    }
}
