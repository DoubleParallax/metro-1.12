package com.doubleparallax.metro.authenticate;

import com.doubleparallax.metro.authenticate.listener.*;
import com.doubleparallax.metro.authenticate.payload.*;
import com.doubleparallax.metro.authenticate.response.AuthenticateResponse;
import com.doubleparallax.metro.authenticate.response.RefreshResponse;
import com.doubleparallax.metro.util.io.Request;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


public class Authenticator {
    
    private static final ObjectMapper MAPPER = new ObjectMapper().enable(MapperFeature.PROPAGATE_TRANSIENT_MARKER).disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    
    public static void authenticate(IAuthenticateListener listener, AuthenticatePayload payload) {
        Request request;
        try {
            request = new Request("https://authserver.mojang.com/authenticate", Request.Method.POST)
                    .setHeader(Request.Header.ACCEPT, "application/json")
                    .setHeader(Request.Header.CONTENT_TYPE, "application/json")
                    .setPayload(MAPPER.writeValueAsString(payload));
        } catch (JsonProcessingException e) {
            listener.response(null);
            return;
        }
        
        String post = request.request();
        
        if (post == null || post.isEmpty()) {
            listener.response(null);
            return;
        }
        
        try {
            AuthenticateResponse response = MAPPER.readValue(post, AuthenticateResponse.class);
            listener.response(response);
        } catch (Exception e) {
            e.printStackTrace();
            listener.response(null);
        }
    }
    
    public static void refresh(IRefreshListener listener, RefreshPayload payload) {
        Request request;
        try {
            request = new Request("https://authserver.mojang.com/refresh", Request.Method.POST)
                    .setHeader(Request.Header.ACCEPT, "application/json")
                    .setHeader(Request.Header.CONTENT_TYPE, "application/json")
                    .setPayload(MAPPER.writeValueAsString(payload));
        } catch (JsonProcessingException e) {
            listener.response(null);
            return;
        }
        
        String post = request.request();
        
        if (post == null || post.isEmpty()) {
            listener.response(null);
            return;
        }
        
        try {
            RefreshResponse response = MAPPER.readValue(post, RefreshResponse.class);
            listener.response(response);
        } catch (Exception e) {
            listener.response(null);
        }
    }
    
    public static void validate(IValidateListener listener, ValidatePayload payload) {
        Request request;
        try {
            request = new Request("https://authserver.mojang.com/validate", Request.Method.POST)
                    .setHeader(Request.Header.ACCEPT, "application/json")
                    .setHeader(Request.Header.CONTENT_TYPE, "application/json")
                    .setPayload(MAPPER.writeValueAsString(payload));
        } catch (JsonProcessingException e) {
            listener.response(false);
            return;
        }
        
        String post = request.request();
        
        listener.response(post != null && post.isEmpty());
    }
    
    public static void signout(ISignoutListener listener, SignoutPayload payload) {
        Request request;
        try {
            request = new Request("https://authserver.mojang.com/signout", Request.Method.POST)
                    .setHeader(Request.Header.ACCEPT, "application/json")
                    .setHeader(Request.Header.CONTENT_TYPE, "application/json")
                    .setPayload(MAPPER.writeValueAsString(payload));
        } catch (JsonProcessingException e) {
            listener.response(false);
            return;
        }
        
        String post = request.request();
        
        listener.response(post != null && post.isEmpty());
    }
    
    public static void invalidate(IInvalidateListener listener, InvalidatePayload payload) throws JsonProcessingException {
        Request request;
        try {
            request = new Request("https://authserver.mojang.com/invalidate", Request.Method.POST)
                    .setHeader(Request.Header.ACCEPT, "application/json")
                    .setHeader(Request.Header.CONTENT_TYPE, "application/json")
                    .setPayload(MAPPER.writeValueAsString(payload));
        } catch (JsonProcessingException e) {
            listener.response(false);
            return;
        }
        
        String post = request.request();
        
        listener.response(post != null && post.isEmpty());
    }
    
}
