package com.doubleparallax.metro.authenticate.payload;


public class SignoutPayload {

    private String username;
    private String password;

    public SignoutPayload(String username) {
        this.setUsername(username);
    }

    public void setUsername(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
