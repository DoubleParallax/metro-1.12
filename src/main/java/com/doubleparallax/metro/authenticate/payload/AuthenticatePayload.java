package com.doubleparallax.metro.authenticate.payload;


public class AuthenticatePayload {

    private Agent agent;
    private String username, password, clientToken;
    private boolean requestUser;

    public AuthenticatePayload(String username, String password) {
        this.agent = new Agent("Minecraft", 1);
        this.username = username;
        this.password = password;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AuthenticatePayload setClientToken(String clientToken) {
        this.clientToken = clientToken;
        return this;
    }

    public AuthenticatePayload setRequestUser(boolean requestUser) {
        this.requestUser = requestUser;
        return this;
    }

    public Agent getAgent() {
        return agent;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getClientToken() {
        return clientToken;
    }

    public boolean isRequestUser() {
        return requestUser;
    }

    private class Agent {
        private String name;
        private int version;

        private Agent(String name, int version) {
            this.name = name;
            this.version = version;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setVersion(int version) {
            this.version = version;
        }

        public String getName() {
            return name;
        }

        public int getVersion() {
            return version;
        }
    }

}
