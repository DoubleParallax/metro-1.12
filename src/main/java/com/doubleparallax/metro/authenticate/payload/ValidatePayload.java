package com.doubleparallax.metro.authenticate.payload;


public class ValidatePayload {

    private String accessToken;
    private String clientToken;

    public ValidatePayload(String accessToken) {
        this.setAccessToken(accessToken);
    }

    public ValidatePayload setClientToken(String clientToken) {
        this.clientToken = clientToken;
        return this;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getClientToken() {
        return clientToken;
    }
}
