package com.doubleparallax.metro.authenticate.payload;


public class RefreshPayload {

    private String accessToken;
    private String clientToken;
    private Profile selectedProfile;
    private boolean requestUser;

    public RefreshPayload(String accessToken, String clientToken) {
        this.setAccessToken(accessToken);
        this.setClientToken(clientToken);
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getClientToken() {
        return clientToken;
    }

    public void setClientToken(String clientToken) {
        this.clientToken = clientToken;
    }

    public Profile getSelectedProfile() {
        return selectedProfile;
    }

    public void setSelectedProfile(Profile selectedProfile) {
        this.selectedProfile = selectedProfile;
    }

    public boolean isRequestUser() {
        return requestUser;
    }

    public void setRequestUser(boolean requestUser) {
        this.requestUser = requestUser;
    }

    private class Profile {
        private String id;
        private String name;

        private Profile(String id, String name) {
            this.setId(id);
            this.setName(name);
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
