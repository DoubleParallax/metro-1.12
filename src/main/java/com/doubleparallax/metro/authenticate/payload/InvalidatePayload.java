package com.doubleparallax.metro.authenticate.payload;


public class InvalidatePayload {

    private String accessToken;
    private String clientToken;

    public InvalidatePayload(String accessToken, String clientToken) {
        this.setAccessToken(accessToken);
        this.setClientToken(clientToken);
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getClientToken() {
        return clientToken;
    }

    public void setClientToken(String clientToken) {
        this.clientToken = clientToken;
    }
}
