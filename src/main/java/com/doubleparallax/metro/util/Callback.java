package com.doubleparallax.metro.util;

public interface Callback<T> {
    
    void response(T t);
    
}
