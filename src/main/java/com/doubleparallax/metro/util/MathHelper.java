package com.doubleparallax.metro.util;

import net.minecraft.entity.Entity;
import net.minecraft.util.math.Vec3d;

public class MathHelper {
    
    public static double getDirection(double x1, double y1, double x2, double y2) {
        return Math.toDegrees(Math.atan2(y2 - y1, x2 - x1));
    }
    
    public static double[] getDirection(double x1, double y1, double z1, double x2, double y2, double z2) {
        double dX = x2 - x1;
        double dZ = z2 - z1;
        double yaw = (Math.toDegrees(Math.atan2(dZ, dX))) - 90;
        double pitch = -(Math.toDegrees(Math.atan2(y2 - y1, Math.hypot(dX, dZ))));
        return new double[] {yaw, pitch};
    }
    
    public static double[] getDirection(Entity from, Entity to) {
        return getDirection(from.posX, from.posY, from.posZ, to.posX, to.posY, to.posZ);
    }
    
    public static double[] getDirection(Vec3d from, Vec3d to) {
        return getDirection(from.x, from.y, from.z, to.x, to.y, to.z);
    }
    
    public static double getDistance(double x1, double y1, double x2, double y2) {
        return Math.hypot(x2 - x1, y2 - y1);
    }
    
    public static boolean isPointInside(double x, double y, double x1, double y1, double x2, double y2) {
        return x >= x1 && x < x2 && y >= y1 && y < y2;
    }
    
    public static boolean isLarger(byte[] larger, byte[] than) {
        for (int i = 0; i < larger.length; i++)
            if (larger[i] < than[i])
                return false;
        return true;
    }
    
    public static int convertFrom(boolean value) {
        return value ? 1 : 0;
    }
    
    public static double wrapAngle(double from, double to, double max) {
        return ((((from - to) % max) + (max * 1.5)) % max) - (max / 2.);
    }
    
    public static double wrapValue(double value, double min, double max) {
        double dif = max - min;
        return (((value - min) % dif) + dif) % dif - min;
    }
    
    public static Vec3d getVectorForRotation(double yaw, double pitch) {
        double x = Math.sin(-yaw * 0.017453292F - Math.PI);
        double y = Math.sin(-pitch * 0.017453292F);
        double z = Math.cos(-yaw * 0.017453292F - Math.PI);
        double len = -Math.cos(-pitch * 0.017453292F);
        return new Vec3d(x * len, y, z * len);
    }
    
    public static double getVectorDifferenceByAngle(Vec3d a, Vec3d b) {
        double dls = dot(a, b) / (a.lengthVector() * b.lengthVector());
        if (dls < -1f)
            dls = -1f;
        else if (dls > 1.0f)
            dls = 1.0f;
        return Math.acos(dls);
    }
    
    public static double dot(Vec3d left, Vec3d right) {
        return left.x * right.x + left.y * right.y + left.z * right.z;
    }
    
    public static double step(double value, double step) {
        double one = 1 / step;
        return Math.round(value * one) / one;
    }
    
    public static double clamp(double value, double min, double max) {
        return Math.min(Math.max(value, min), max);
    }
    
}
