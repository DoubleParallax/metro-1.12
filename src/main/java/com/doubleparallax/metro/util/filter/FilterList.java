package com.doubleparallax.metro.util.filter;

import java.util.ArrayList;

public class FilterList<T> extends ArrayList<T> {
    
    private boolean unique, stringLower;
    
    public FilterList(boolean unique, boolean stringLower) {
        this.unique = unique;
        this.stringLower = stringLower;
    }
    
    @Override
    public boolean add(T t) {
        return !(unique && contains(t)) && super.add(t);
    }
    
    @Override
    public boolean contains(Object o) {
        return super.contains(stringLower ? o.toString().toLowerCase() : o);
    }
    
}
