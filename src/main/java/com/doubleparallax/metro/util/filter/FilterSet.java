package com.doubleparallax.metro.util.filter;

import java.util.HashSet;

public class FilterSet<T> extends HashSet<T> {
    
    private boolean unique, stringLower;
    
    public FilterSet(boolean unique, boolean stringLower) {
        this.unique = unique;
        this.stringLower = stringLower;
    }
    
    @Override
    public boolean add(T t) {
        return !(unique && contains(t)) && super.add(t);
    }
    
    @Override
    public boolean contains(Object o) {
        return super.contains(stringLower ? o.toString().toLowerCase() : o);
    }
    
}
