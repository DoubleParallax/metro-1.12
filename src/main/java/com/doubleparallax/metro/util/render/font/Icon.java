package com.doubleparallax.metro.util.render.font;

import java.util.HashMap;
import java.util.Map;

public class Icon {
    
    public enum Icons {
        PERSON('A'),
        PEOPLE('B'),
        BANNER('C'),
        LOCK('D'),
        SPANNER('E'),
        SHUTDOWN('F'),
        EXCLAIM('G'),
        RECORDER('H'),
        BELL('I'),
        PAINTBRUSH('J'),
        PICTURE('K'),
        MUSIC('L'),
        MENU_4('M'),
        MESSAGE_BUBBLES('N'),
        BRIEFCASE('O'),
        WORLD('P'),
        PRINT('Q'),
        CHECKMARK('R'),
        CAPE('S'),
        JACKET('T'),
        ARM_LEFT('U'),
        ARM_RIGHT('V'),
        LEG_LEFT('W'),
        LEG_RIGHT('X'),
        HAT('Y'),
        HAND('Z'),
        PERSON_PLUS('a'),
        CLOSE('b'),
        PIN('c'),
        PENCIL('d'),
        LOCATION('e'),
        COG('f'),
        DUPLICATE('g'),
        UNKNOWN('?');
        
        private char character;
        
        Icons(char character) {
            this.character = character;
        }
        
        public char getCharacter() {
            return character;
        }
        
        @Override
        public String toString() {
            return String.valueOf(character);
        }
    }
    
    private static final Map<String, Icons> CHAR_MAP = new HashMap<String, Icons>() {{
        //Main Menu
        put("singleplayer", Icons.PERSON);
        put("multiplayer", Icons.PEOPLE);
        put("realms", Icons.BANNER);
        put("accounts", Icons.PERSON_PLUS);
        put("options", Icons.COG);
        put("quit", Icons.SHUTDOWN);
        
        //Properties
        put(String.format("module.category.%s", 0), Icons.UNKNOWN);
        put(String.format("module.category.%s", 1), Icons.MENU_4);
        put(String.format("module.category.%s", 2), Icons.UNKNOWN);
        put(String.format("module.category.%s", 3), Icons.BANNER);
        put(String.format("module.category.%s", 4), Icons.UNKNOWN);
        put(String.format("module.category.%s", 5), Icons.PERSON);
        put(String.format("module.category.%s", 6), Icons.PICTURE);
        put(String.format("module.category.%s", 7), Icons.WORLD);
        put(String.format("module.category.%s", 8), Icons.UNKNOWN);
    }};
    
    public static Icons get(String key) {
        key = key.toLowerCase();
        return CHAR_MAP.containsKey(key) ? CHAR_MAP.get(key.toLowerCase()) : Icons.UNKNOWN;
    }
    
}
