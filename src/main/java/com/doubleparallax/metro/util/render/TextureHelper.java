package com.doubleparallax.metro.util.render;

import com.doubleparallax.metro.Client;
import com.doubleparallax.metro.util.Helper;
import com.doubleparallax.metro.util.io.FileHelper;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.util.ResourceLocation;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class TextureHelper implements Helper {
    
    private static final Map<String, Texture> TEXTURES = new HashMap<>();
    
    public enum Textures {
        LOGO_16("logo/16.png"),
        LOGO_24("logo/24.png"),
        LOGO_48("logo/48.png"),
        LOGO_64("logo/64.png"),
        LOGO_128("logo/128.png"),
        LOGO_256("logo/256.png"),
        LOGO_512("logo/512.png"),
        
        BACKGROUND("background.png"),
        DEFAULT_HEAD_128("heads/default_128.png"),
        DEFAULT_HEAD_256("heads/default_256.png");
        
        private ResourceLocation resource;
        
        Textures(String path) {
            resource = new ResourceLocation(FileHelper.procMinecraftRes(String.format("texture/%s", path)));
        }
        
        public ResourceLocation getResource() {
            return resource;
        }
    }
    
    public static Texture download(String url) {
        if (TEXTURES.containsKey(url)) {
            Texture texture = TEXTURES.get(url);
            if (texture.isReady()) {
                texture.setResource(mc.getTextureManager().getDynamicTextureLocation(Client.INSTANCE.PROJECT.toLowerCase() + "/" + url, new DynamicTexture(texture.getImage())));
                return null;
            }
            return texture;
        }
        Texture texture = new Texture();
        TEXTURES.put(url, texture);
        download(texture, url);
        return texture;
    }
    
    public static void download(final Texture texture, final String url) {
        new Thread(() -> {
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
                connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13");
                if (connection.getResponseCode() == 200) {
                    InputStream stream = connection.getInputStream();
                    texture.setImage(ImageIO.read(stream));
                } else {
                    System.out.println(connection.getResponseCode() + " : " + connection.getResponseMessage());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
    
    public static class Texture {
        private ResourceLocation resource;
        private BufferedImage image;
        private int width, height;
        
        public ResourceLocation getResource() {
            return resource;
        }
        
        public BufferedImage getImage() {
            return image;
        }
        
        public int getWidth() {
            return width;
        }
        
        public int getHeight() {
            return height;
        }
        
        public void setResource(ResourceLocation resource) {
            this.resource = resource;
        }
        
        public void setImage(BufferedImage image) {
            this.image = image;
        }
        
        public void setWidth(int width) {
            this.width = width;
        }
        
        public void setHeight(int height) {
            this.height = height;
        }
        
        public boolean isReady() {
            return image != null && resource == null;
        }
    }
    
}
