package com.doubleparallax.metro.util.render.geometry;

import com.doubleparallax.metro.util.Helper;
import com.doubleparallax.metro.util.render.ColorHelper;
import com.doubleparallax.metro.util.render.TextureHelper;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class Rect implements Helper {
    
    private static final ResourceLocation D_RES = null;
    private static final String D_URL = null;
    private static final double[] D_TEX = {0, 0, 1, 0, 1, 1, 0, 1};
    private static final int[] D_COL = {-1, -1, -1, -1};
    private static final int D_MOD = GL11.GL_TRIANGLE_FAN;
    
    public static void draw_Res_Url_Xyz4_Tex4_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        Tessellator tes = Tessellator.getInstance();
        BufferBuilder buf = tes.getBuffer();
        
        float[][] rgba = {
                ColorHelper.colorToRgbaF(ColorHelper.relativeAlpha(color1, alpha)),
                ColorHelper.colorToRgbaF(ColorHelper.relativeAlpha(color2, alpha)),
                ColorHelper.colorToRgbaF(ColorHelper.relativeAlpha(color3, alpha)),
                ColorHelper.colorToRgbaF(ColorHelper.relativeAlpha(color4, alpha))
        };
        
        double[] x = {x1, x2, x3, x4};
        double[] y = {y1, y2, y3, y4};
        double[] z = {z1, z2, z3, z4};
        double[] tx = {tx1, tx2, tx3, tx4};
        double[] ty = {ty1, ty2, ty3, ty4};
        
        GlStateManager.enableAlpha();
        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
        RenderHelper.disableStandardItemLighting();
        
        if (url != null) {
            TextureHelper.Texture texture = TextureHelper.download(url);
            if (texture != null && texture.getResource() != null)
                resource = texture.getResource();
        }
        
        if (resource == null)
            GlStateManager.disableTexture2D();
        else {
            mc.getTextureManager().bindTexture(resource);
            GlStateManager.enableTexture2D();
        }
        
        int shade = GL11.glGetInteger(GL11.GL_SHADE_MODEL);
        GlStateManager.shadeModel(GL11.GL_SMOOTH);
        
        buf.begin(mode, DefaultVertexFormats.POSITION_TEX_COLOR);
        for (int i = x.length - 1; i >= 0; i--)
            buf.pos(x[i], y[i], z[i]).tex(tx[i], ty[i]).color(rgba[i][0], rgba[i][1], rgba[i][2], rgba[i][3]).endVertex();
        tes.draw();
        
        GlStateManager.shadeModel(shade);
        RenderHelper.enableStandardItemLighting();
    }
    
    public static void draw_Res_Url_Xyz4(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Tex4(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Tex4_Col4(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Tex4_Col(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Tex4_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Tex4_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Tex2(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Tex2_Col4(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Tex2_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Tex2_Col(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Tex2_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Tex2_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Col4(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Col(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Url_Xyz4_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Url_Xy4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Tex4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Tex4_Col4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Tex4_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Tex4_Col(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Tex4_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Tex4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Tex2(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Tex2_Col4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Tex2_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Tex2_Col(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Tex2_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Tex2_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Col4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Col(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Url_Xy4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Url_Xy2(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Tex4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Tex4_Col4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Tex4_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Tex4_Col(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Tex4_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Tex4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Tex2(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Tex2_Col4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Tex2_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Tex2_Col(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Tex2_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Tex2_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Col4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Col(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Url_Xy2_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Xyz4(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Xyz4_Tex4(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Xyz4_Tex4_Col4(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Xyz4_Tex4_Col4_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Xyz4_Tex4_Col(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Xyz4_Tex4_Col_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Xyz4_Tex4_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Xyz4_Tex2(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Xyz4_Tex2_Col4(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Xyz4_Tex2_Col4_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Xyz4_Tex2_Col(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Xyz4_Tex2_Col_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Xyz4_Tex2_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Xyz4_Col4(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Xyz4_Col4_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Xyz4_Col(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Xyz4_Col_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Xyz4_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Xy4(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Xy4_Tex4(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Xy4_Tex4_Col4(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Xy4_Tex4_Col4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Xy4_Tex4_Col(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Xy4_Tex4_Col_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Xy4_Tex4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Xy4_Tex2(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Xy4_Tex2_Col4(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Xy4_Tex2_Col4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Xy4_Tex2_Col(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Xy4_Tex2_Col_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Xy4_Tex2_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Xy4_Col4(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Xy4_Col4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Xy4_Col(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Xy4_Col_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Xy4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Xy2(ResourceLocation resource, double x1, double y1, double x2, double y2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Xy2_Tex4(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Xy2_Tex4_Col4(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Xy2_Tex4_Col4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Xy2_Tex4_Col(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Xy2_Tex4_Col_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Xy2_Tex4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Xy2_Tex2(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Res_Xy2_Tex2_Col4(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Xy2_Tex2_Col4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Xy2_Tex2_Col(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Xy2_Tex2_Col_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Xy2_Tex2_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Res_Xy2_Col4(ResourceLocation resource, double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Res_Xy2_Col4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Res_Xy2_Col(ResourceLocation resource, double x1, double y1, double x2, double y2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Res_Xy2_Col_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }
    
    public static void draw_Res_Xy2_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Url_Xyz4(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Url_Xyz4_Tex4(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Url_Xyz4_Tex4_Col4(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Url_Xyz4_Tex4_Col4_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Url_Xyz4_Tex4_Col(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Url_Xyz4_Tex4_Col_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Url_Xyz4_Tex4_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Url_Xyz4_Tex2(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Url_Xyz4_Tex2_Col4(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Url_Xyz4_Tex2_Col4_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Url_Xyz4_Tex2_Col(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Url_Xyz4_Tex2_Col_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Url_Xyz4_Tex2_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Url_Xyz4_Col4(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Url_Xyz4_Col4_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Url_Xyz4_Col(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Url_Xyz4_Col_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }
    
    public static void draw_Url_Xyz4_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Url_Xy4(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Url_Xy4_Tex4(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Url_Xy4_Tex4_Col4(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Url_Xy4_Tex4_Col4_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Url_Xy4_Tex4_Col(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Url_Xy4_Tex4_Col_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Url_Xy4_Tex4_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Url_Xy4_Tex2(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Url_Xy4_Tex2_Col4(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Url_Xy4_Tex2_Col4_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Url_Xy4_Tex2_Col(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Url_Xy4_Tex2_Col_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Url_Xy4_Tex2_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Url_Xy4_Col4(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Url_Xy4_Col4_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Url_Xy4_Col(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Url_Xy4_Col_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }
    
    public static void draw_Url_Xy4_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Url_Xy2(String url, double x1, double y1, double x2, double y2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Url_Xy2_Tex4(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Url_Xy2_Tex4_Col4(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Url_Xy2_Tex4_Col4_Mod(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Url_Xy2_Tex4_Col(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Url_Xy2_Tex4_Col_Mod(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Url_Xy2_Tex4_Mod(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Url_Xy2_Tex2(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Url_Xy2_Tex2_Col4(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Url_Xy2_Tex2_Col4_Mod(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Url_Xy2_Tex2_Col(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Url_Xy2_Tex2_Col_Mod(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Url_Xy2_Tex2_Mod(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Url_Xy2_Col4(String url, double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Url_Xy2_Col4_Mod(String url, double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Url_Xy2_Col(String url, double x1, double y1, double x2, double y2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Url_Xy2_Col_Mod(String url, double x1, double y1, double x2, double y2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }
    
    public static void draw_Url_Xy2_Mod(String url, double x1, double y1, double x2, double y2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Xyz4(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Xyz4_Tex4(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Xyz4_Tex4_Col4(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Xyz4_Tex4_Col4_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Xyz4_Tex4_Col(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Xyz4_Tex4_Col_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Xyz4_Tex4_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Xyz4_Tex2(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Xyz4_Tex2_Col4(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Xyz4_Tex2_Col4_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Xyz4_Tex2_Col(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Xyz4_Tex2_Col_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Xyz4_Tex2_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Xyz4_Col4(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Xyz4_Col4_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Xyz4_Col(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Xyz4_Col_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }
    
    public static void draw_Xyz4_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Xy4(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Xy4_Tex4(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Xy4_Tex4_Col4(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Xy4_Tex4_Col4_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Xy4_Tex4_Col(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Xy4_Tex4_Col_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Xy4_Tex4_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Xy4_Tex2(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Xy4_Tex2_Col4(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Xy4_Tex2_Col4_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Xy4_Tex2_Col(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Xy4_Tex2_Col_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Xy4_Tex2_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Xy4_Col4(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Xy4_Col4_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Xy4_Col(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Xy4_Col_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }
    
    public static void draw_Xy4_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Xy2(double x1, double y1, double x2, double y2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Xy2_Tex4(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Xy2_Tex4_Col4(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Xy2_Tex4_Col4_Mod(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Xy2_Tex4_Col(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Xy2_Tex4_Col_Mod(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Xy2_Tex4_Mod(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Xy2_Tex2(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }
    
    public static void draw_Xy2_Tex2_Col4(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Xy2_Tex2_Col4_Mod(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Xy2_Tex2_Col(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Xy2_Tex2_Col_Mod(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }
    
    public static void draw_Xy2_Tex2_Mod(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
    
    public static void draw_Xy2_Col4(double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }
    
    public static void draw_Xy2_Col4_Mod(double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }
    
    public static void draw_Xy2_Col(double x1, double y1, double x2, double y2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }
    
    public static void draw_Xy2_Col_Mod(double x1, double y1, double x2, double y2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }
    
    public static void draw_Xy2_Mod(double x1, double y1, double x2, double y2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }
}
