package com.doubleparallax.metro.util.render.geometry;

import com.doubleparallax.metro.util.Helper;
import com.doubleparallax.metro.util.render.ColorHelper;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import org.lwjgl.opengl.GL11;

public class Line implements Helper {
    
    private static final int[] D_COL = {-1, -1, -1, -1};
    private static final float D_WID = 1f;
    
    public static void draw_Xyz_Col2_Wid(double x1, double y1, double z1, double x2, double y2, double z2, int color1, int color2, float width, double alpha) {
        Tessellator tes = Tessellator.getInstance();
        BufferBuilder buf = tes.getBuffer();
        
        if ((width = Math.min(width, 10)) <= 0)
            return;
        
        float[][] rgba = {
                ColorHelper.colorToRgbaF(ColorHelper.relativeAlpha(color1, alpha)),
                ColorHelper.colorToRgbaF(ColorHelper.relativeAlpha(color2, alpha))
        };
        
        double[] x = {x1, x2};
        double[] y = {y1, y2};
        double[] z = {z1, z2};
        
        GlStateManager.enableAlpha();
        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
        RenderHelper.disableStandardItemLighting();
        GlStateManager.disableTexture2D();
        
        float prevWidth = GL11.glGetFloat(GL11.GL_LINE_WIDTH);
        GL11.glLineWidth(width);
        
        int shade = GL11.glGetInteger(GL11.GL_SHADE_MODEL);
        GlStateManager.shadeModel(GL11.GL_SMOOTH);
        
        buf.begin(GL11.GL_LINE_STRIP, DefaultVertexFormats.POSITION_COLOR);
        for (int i = x.length - 1; i >= 0; i--)
            buf.pos(x[i], y[i], z[i]).color(rgba[i][0], rgba[i][1], rgba[i][2], rgba[i][3]).endVertex();
        tes.draw();
        
        GlStateManager.shadeModel(shade);
        
        GL11.glLineWidth(prevWidth);
        RenderHelper.enableStandardItemLighting();
    }
    
    public static void draw_Xyz(double x1, double y1, double z1, double x2, double y2, double z2, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, z1, x2, y2, z2, D_COL[0], D_COL[1], D_WID, alpha);
    }
    
    public static void draw_Xyz_Col2(double x1, double y1, double z1, double x2, double y2, double z2, int color1, int color2, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, z1, x2, y2, z2, color1, color2, D_WID, alpha);
    }
    
    public static void draw_Xyz_Col(double x1, double y1, double z1, double x2, double y2, double z2, int color, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, z1, x2, y2, z2, color, color, D_WID, alpha);
    }
    
    public static void draw_Xyz_Col_Wid(double x1, double y1, double z1, double x2, double y2, double z2, int color, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, z1, x2, y2, z2, color, color, width, alpha);
    }
    
    public static void draw_Xyz_Wid(double x1, double y1, double z1, double x2, double y2, double z2, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, z1, x2, y2, z2, D_COL[0], D_COL[1], width, alpha);
    }
    
    public static void draw_Xy(double x1, double y1, double x2, double y2, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, 0, x2, y2, 0, D_COL[0], D_COL[1], D_WID, alpha);
    }
    
    public static void draw_Xy_Col2(double x1, double y1, double x2, double y2, int color1, int color2, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, 0, x2, y2, 0, color1, color2, D_WID, alpha);
    }
    
    public static void draw_Xy_Col2_Wid(double x1, double y1, double x2, double y2, int color1, int color2, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, 0, x2, y2, 0, color1, color2, width, alpha);
    }
    
    public static void draw_Xy_Col(double x1, double y1, double x2, double y2, int color, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, 0, x2, y2, 0, color, color, D_WID, alpha);
    }
    
    public static void draw_Xy_Col_Wid(double x1, double y1, double x2, double y2, int color, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, 0, x2, y2, 0, color, color, width, alpha);
    }
    
    public static void draw_Xy_Wid(double x1, double y1, double x2, double y2, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, 0, x2, y2, 0, D_COL[0], D_COL[1], width, alpha);
    }
    
    public static void draw_Xz(double x1, double z1, double x2, double z2, double alpha) {
        draw_Xyz_Col2_Wid(x1, 0, z1, x2, 0, z2, D_COL[0], D_COL[1], D_WID, alpha);
    }
    
    public static void draw_Xz_Col2(double x1, double z1, double x2, double z2, int color1, int color2, double alpha) {
        draw_Xyz_Col2_Wid(x1, 0, z1, x2, 0, z2, color1, color2, D_WID, alpha);
    }
    
    public static void draw_Xz_Col2_Wid(double x1, double z1, double x2, double z2, int color1, int color2, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, 0, z1, x2, 0, z2, color1, color2, width, alpha);
    }
    
    public static void draw_Xz_Col(double x1, double z1, double x2, double z2, int color, double alpha) {
        draw_Xyz_Col2_Wid(x1, 0, z1, x2, 0, z2, color, color, D_WID, alpha);
    }
    
    public static void draw_Xz_Col_Wid(double x1, double z1, double x2, double z2, int color, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, 0, z1, x2, 0, z2, color, color, width, alpha);
    }
    
    public static void draw_Xz_Wid(double x1, double z1, double x2, double z2, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, 0, z1, x2, 0, z2, D_COL[0], D_COL[1], width, alpha);
    }
    
    public static void draw_Yz(double y1, double z1, double y2, double z2, double alpha) {
        draw_Xyz_Col2_Wid(0, y1, z1, 0, y2, z2, D_COL[0], D_COL[1], D_WID, alpha);
    }
    
    public static void draw_Yz_Col2(double y1, double z1, double y2, double z2, int color1, int color2, double alpha) {
        draw_Xyz_Col2_Wid(0, y1, z1, 0, y2, z2, color1, color2, D_WID, alpha);
    }
    
    public static void draw_Yz_Col2_Wid(double y1, double z1, double y2, double z2, int color1, int color2, float width, double alpha) {
        draw_Xyz_Col2_Wid(0, y1, z1, 0, y2, z2, color1, color2, width, alpha);
    }
    
    public static void draw_Yz_Col(double y1, double z1, double y2, double z2, int color, double alpha) {
        draw_Xyz_Col2_Wid(0, y1, z1, 0, y2, z2, color, color, D_WID, alpha);
    }
    
    public static void draw_Yz_Col_Wid(double y1, double z1, double y2, double z2, int color, float width, double alpha) {
        draw_Xyz_Col2_Wid(0, y1, z1, 0, y2, z2, color, color, width, alpha);
    }
    
    public static void draw_Yz_Wid(double y1, double z1, double y2, double z2, float width, double alpha) {
        draw_Xyz_Col2_Wid(0, y1, z1, 0, y2, z2, D_COL[0], D_COL[1], width, alpha);
    }
    
}
