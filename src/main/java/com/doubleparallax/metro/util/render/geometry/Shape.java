package com.doubleparallax.metro.util.render.geometry;

import com.doubleparallax.metro.util.Helper;
import org.lwjgl.opengl.GL11;

public abstract class Shape<T extends Shape> implements Helper {
    
    protected int code = GL11.GL_TRIANGLE_FAN;
    
    public T setCode(int code) {
        this.code = code;
        return (T) this;
    }
    
    public abstract T draw(double alpha);
}
