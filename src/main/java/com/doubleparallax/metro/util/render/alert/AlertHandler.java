package com.doubleparallax.metro.util.render.alert;

import org.lwjgl.opengl.Display;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class AlertHandler {
    
    public static final List<Alert> ALERTS = new CopyOnWriteArrayList<>();
    
    public static void add(Alert alert) {
        if (!ALERTS.isEmpty())
            alert.getY().reset(ALERTS.get(ALERTS.size() - 1).getY().getValue());
        ALERTS.add(alert);
    }
    
    public static void remove(Alert alert) {
        ALERTS.remove(alert);
    }
    
    public static void _step() {
        double y = Display.getHeight() - 4;
        for (Alert alert : ALERTS) {
            alert.getX().setValue(Display.getWidth() - 4 - alert.getW().getValue());
            alert.getY().setValue(y - alert.getH().getValue());
            alert.getY().setInterpolated(Math.min(alert.getY().getValue() + alert.getH().getValue(), alert.getY().getValue()));
            alert._step();
            if (alert.isOpen())
                y -= alert.getH().getValue() + 6;
        }
    }
    
    public static void render(double alpha) {
        ALERTS.forEach(a -> a.render(alpha));
    }
    
}
