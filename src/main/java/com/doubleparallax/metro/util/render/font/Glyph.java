package com.doubleparallax.metro.util.render.font;

import java.awt.*;
import java.util.List;

public class Glyph {
    
    private char c;
    private float x, y, w, h, tx, ty, tw, th;
    
    public Glyph(char c, FontMetrics fontMetrics, float charHeight, List<String> lines, float textureWidth, float textureHeight) {
        this.c = c;
        
        String origin = String.valueOf(c);
        
        int index = 0;
        for (String s : lines) {
            if (s.contains(origin)) {
                index = lines.indexOf(s);
            }
        }
        
        for (String s : lines) {
            if (s.contains(origin)) {
                origin = s;
                break;
            }
        }
        
        x = fontMetrics.stringWidth(origin.substring(0, origin.indexOf(c)));
        y = charHeight * index;
        w = fontMetrics.charWidth(c);
        h = charHeight;
        
        tx = 1 / textureWidth * x;
        ty = 1 / textureHeight * y;
        tw = 1 / textureWidth * w;
        th = 1 / textureHeight * h;
    }
    
    public char getC() {
        return c;
    }
    
    public float getX() {
        return x;
    }
    
    public float getY() {
        return y;
    }
    
    public float getW() {
        return w;
    }
    
    public float getH() {
        return h;
    }
    
    public float getTx() {
        return tx;
    }
    
    public float getTy() {
        return ty;
    }
    
    public float getTw() {
        return tw;
    }
    
    public float getTh() {
        return th;
    }
    
}
