package com.doubleparallax.metro.util.render;

import com.doubleparallax.metro.util.Helper;
import net.minecraft.client.renderer.GlStateManager;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import static org.lwjgl.opengl.GL11.*;

public class RenderHelper implements Helper {
    
    public enum State {
        ALPHA(GL_ALPHA_TEST),
        BLEND(GL_BLEND),
        COLOR_LOGIC(GL_COLOR_LOGIC_OP),
        COLOR_MATERIAL(GL_COLOR_MATERIAL),
        CULL(GL_CULL_FACE),
        DEPTH(GL_DEPTH_TEST),
        FOG(GL_FOG),
        LIGHTING(GL_LIGHTING),
        NORMALIZE(GL_NORMALIZE),
        POLYGON_OFFSET_FILL(GL_POLYGON_OFFSET_FILL),
        POLYGON_OFFSET_LINE(GL_POLYGON_OFFSET_LINE),
        RESCALE_NORMAL(32826),
        TEXTURE_2D(GL_TEXTURE_2D),
        LINE_SMOOTH(GL_LINE_SMOOTH),
        POINT_SMOOTH(GL_POINT_SMOOTH);
        
        int gl;
        
        State(int gl) {
            this.gl = gl;
        }
    }
    
    private static Stack<Map<State, Boolean>> prior = new Stack<>();
    private static Stack<int[]> blends = new Stack<>();
    
    public static void preRender(State... states) {
        Map<State, Boolean> map = new HashMap<>();
        for (State state : states.length > 0 ? states : State.values())
            map.put(state, glGetBoolean(state.gl));
        prior.add(map);
        blends.add(new int[] {glGetInteger(GL_BLEND_SRC), glGetInteger(GL_BLEND_DST)});
    }
    
    public static void postRender() {
        Map<State, Boolean> queue = prior.pop();
        for (Map.Entry<State, Boolean> i : queue.entrySet()) {
            switch (i.getKey()) {
                case ALPHA:
                    if (i.getValue())
                        GlStateManager.enableAlpha();
                    else
                        GlStateManager.disableAlpha();
                    break;
                case BLEND:
                    if (i.getValue())
                        GlStateManager.enableBlend();
                    else
                        GlStateManager.disableBlend();
                    break;
                case COLOR_LOGIC:
                    if (i.getValue())
                        GlStateManager.enableColorLogic();
                    else
                        GlStateManager.disableColorLogic();
                    break;
                case COLOR_MATERIAL:
                    if (i.getValue())
                        GlStateManager.enableColorMaterial();
                    else
                        GlStateManager.disableColorMaterial();
                    break;
                case CULL:
                    if (i.getValue())
                        GlStateManager.enableCull();
                    else
                        GlStateManager.disableCull();
                    break;
                case DEPTH:
                    if (i.getValue())
                        GlStateManager.enableDepth();
                    else
                        GlStateManager.disableDepth();
                    break;
                case FOG:
                    if (i.getValue())
                        GlStateManager.enableFog();
                    else
                        GlStateManager.disableFog();
                    break;
                case LIGHTING:
                    if (i.getValue())
                        GlStateManager.enableLighting();
                    else
                        GlStateManager.disableLighting();
                    break;
                case NORMALIZE:
                    if (i.getValue())
                        GlStateManager.enableNormalize();
                    else
                        GlStateManager.disableNormalize();
                    break;
                case POLYGON_OFFSET_FILL:
                    if (i.getValue())
                        GlStateManager.enablePolygonOffset();
                    else
                        GlStateManager.disablePolygonOffset();
                    break;
                case RESCALE_NORMAL:
                    if (i.getValue())
                        GlStateManager.enableRescaleNormal();
                    else
                        GlStateManager.disableRescaleNormal();
                    break;
                case TEXTURE_2D:
                    if (i.getValue())
                        GlStateManager.enableTexture2D();
                    else
                        GlStateManager.disableTexture2D();
                    break;
                case POLYGON_OFFSET_LINE:
                case LINE_SMOOTH:
                case POINT_SMOOTH:
                    if (i.getValue())
                        glEnable(i.getKey().gl);
                    else
                        glDisable(i.getKey().gl);
                    break;
            }
        }
        int[] blend = blends.pop();
        GlStateManager.blendFunc(blend[0], blend[1]);
    }
    
    
}
