package com.doubleparallax.metro.util.render.geometry;

import com.doubleparallax.metro.util.Helper;
import com.doubleparallax.metro.util.render.ColorHelper;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import org.lwjgl.opengl.GL11;

public class Circle implements Helper {
    
    private static final float[] D_CLA = {0, 360};
    private static final float D_PRE = 360;
    private static final boolean D_PIE = false;
    private static final int D_COL = -1;
    private static final int D_MOD = GL11.GL_TRIANGLE_FAN;
    
    public static void draw_Xy_Cla_Pre_Pie_Col_Mod(double x1, double y1, double x2, double y2, double start, double end, double precision, boolean pie, int color, int mode, double alpha) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        
        float[] rgba = ColorHelper.colorToRgbaF(ColorHelper.relativeAlpha(color, alpha));
        double prec = 360. / precision;
        double s = Math.min(start, end);
        double e = Math.max(start, end);
        
        GlStateManager.enableAlpha();
        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
        RenderHelper.disableStandardItemLighting();
        GlStateManager.disableTexture2D();
        
        int shade = GL11.glGetInteger(GL11.GL_SHADE_MODEL);
        GlStateManager.shadeModel(GL11.GL_SMOOTH);
        
        buffer.begin(mode, DefaultVertexFormats.POSITION_COLOR);
        if (pie)
            buffer.pos(x1, y1, 0).color(rgba[0], rgba[1], rgba[2], rgba[3]).endVertex();
        for (double i = s; i < e; i += prec) {
            double rad = Math.toRadians(i);
            double x = x1 + ((Math.cos(rad)) * (x2 / 2.));
            double y = y1 + ((Math.sin(rad)) * (y2 / 2.));
            buffer.pos(x, y, 0).color(rgba[0], rgba[1], rgba[2], rgba[3]).endVertex();
        }
        tessellator.draw();
        
        GlStateManager.shadeModel(shade);
        
        RenderHelper.enableStandardItemLighting();
    }
    
    public static void draw_Xy(double x1, double y1, double x2, double y2, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], D_PRE, D_PIE, D_COL, D_MOD, alpha);
    }
    
    public static void draw_Xy_Cla(double x1, double y1, double x2, double y2, double start, double end, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, D_PRE, D_PIE, D_COL, D_MOD, alpha);
    }
    
    public static void draw_Xy_Cla_Pre(double x1, double y1, double x2, double y2, double start, double end, double precision, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, precision, D_PIE, D_COL, D_MOD, alpha);
    }
    
    public static void draw_Xy_Cla_Pre_Pie(double x1, double y1, double x2, double y2, double start, double end, double precision, boolean pie, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, precision, pie, D_COL, D_MOD, alpha);
    }
    
    public static void draw_Xy_Cla_Pre_Pie_Col(double x1, double y1, double x2, double y2, double start, double end, double precision, boolean pie, int color, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, precision, pie, color, D_MOD, alpha);
    }
    
    public static void draw_Xy_Cla_Pre_Pie_Mod(double x1, double y1, double x2, double y2, double start, double end, double precision, boolean pie, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, precision, pie, D_COL, mode, alpha);
    }
    
    public static void draw_Xy_Cla_Pre_Col(double x1, double y1, double x2, double y2, double start, double end, double precision, int color, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, precision, D_PIE, color, D_MOD, alpha);
    }
    
    public static void draw_Xy_Cla_Pre_Col_Mod(double x1, double y1, double x2, double y2, double start, double end, double precision, int color, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, precision, D_PIE, color, mode, alpha);
    }
    
    public static void draw_Xy_Cla_Pre_Mod(double x1, double y1, double x2, double y2, double start, double end, double precision, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, precision, D_PIE, D_COL, mode, alpha);
    }
    
    public static void draw_Xy_Cla_Pie(double x1, double y1, double x2, double y2, double start, double end, boolean pie, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, D_PRE, pie, D_COL, D_MOD, alpha);
    }
    
    public static void draw_Xy_Cla_Pie_Col(double x1, double y1, double x2, double y2, double start, double end, boolean pie, int color, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, D_PRE, pie, color, D_MOD, alpha);
    }
    
    public static void draw_Xy_Cla_Pie_Col_Mod(double x1, double y1, double x2, double y2, double start, double end, boolean pie, int color, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, D_PRE, pie, color, mode, alpha);
    }
    
    public static void draw_Xy_Cla_Pie_Mod(double x1, double y1, double x2, double y2, double start, double end, boolean pie, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, D_PRE, pie, D_COL, mode, alpha);
    }
    
    public static void draw_Xy_Cla_Col(double x1, double y1, double x2, double y2, double start, double end, int color, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, D_PRE, D_PIE, color, D_MOD, alpha);
    }
    
    public static void draw_Xy_Cla_Col_Mod(double x1, double y1, double x2, double y2, double start, double end, int color, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, D_PRE, D_PIE, color, mode, alpha);
    }
    
    public static void draw_Xy_Cla_Mod(double x1, double y1, double x2, double y2, double start, double end, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, start, end, D_PRE, D_PIE, D_COL, mode, alpha);
    }
    
    public static void draw_Xy_Pre(double x1, double y1, double x2, double y2, double precision, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], precision, D_PIE, D_COL, D_MOD, alpha);
    }
    
    public static void draw_Xy_Pre_Pie(double x1, double y1, double x2, double y2, double precision, boolean pie, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], precision, pie, D_COL, D_MOD, alpha);
    }
    
    public static void draw_Xy_Pre_Pie_Col(double x1, double y1, double x2, double y2, double precision, boolean pie, int color, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], precision, pie, color, D_MOD, alpha);
    }
    
    public static void draw_Xy_Pre_Pie_Col_Mod(double x1, double y1, double x2, double y2, double precision, boolean pie, int color, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], precision, pie, color, mode, alpha);
    }
    
    public static void draw_Xy_Pre_Pie_Mod(double x1, double y1, double x2, double y2, double precision, boolean pie, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], precision, pie, D_COL, mode, alpha);
    }
    
    public static void draw_Xy_Pre_Col(double x1, double y1, double x2, double y2, double precision, int color, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], precision, D_PIE, color, D_MOD, alpha);
    }
    
    public static void draw_Xy_Pre_Col_Mod(double x1, double y1, double x2, double y2, double precision, int color, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], precision, D_PIE, color, mode, alpha);
    }
    
    public static void draw_Xy_Pre_Mod(double x1, double y1, double x2, double y2, double precision, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], precision, D_PIE, D_COL, mode, alpha);
    }
    
    public static void draw_Xy_Pie(double x1, double y1, double x2, double y2, boolean pie, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], D_PRE, pie, D_COL, D_MOD, alpha);
    }
    
    public static void draw_Xy_Pie_Col(double x1, double y1, double x2, double y2, boolean pie, int color, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], D_PRE, pie, color, D_MOD, alpha);
    }
    
    public static void draw_Xy_Pie_Col_Mod(double x1, double y1, double x2, double y2, boolean pie, int color, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], D_PRE, pie, color, mode, alpha);
    }
    
    public static void draw_Xy_Pie_Mod(double x1, double y1, double x2, double y2, boolean pie, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], D_PRE, pie, D_COL, mode, alpha);
    }
    
    public static void draw_Xy_Col(double x1, double y1, double x2, double y2, int color, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], D_PRE, D_PIE, color, D_MOD, alpha);
    }
    
    public static void draw_Xy_Col_Mod(double x1, double y1, double x2, double y2, int color, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], D_PRE, D_PIE, color, mode, alpha);
    }
    
    public static void draw_Xy_Mod(double x1, double y1, double x2, double y2, int mode, double alpha) {
        draw_Xy_Cla_Pre_Pie_Col_Mod(x1, y1, x2, y2, D_CLA[0], D_CLA[1], D_PRE, D_PIE, D_COL, mode, alpha);
    }
    
    
}
