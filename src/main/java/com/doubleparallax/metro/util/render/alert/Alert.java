package com.doubleparallax.metro.util.render.alert;

import com.doubleparallax.metro.util.*;
import com.doubleparallax.metro.util.animation.Animation;
import com.doubleparallax.metro.util.render.ColorHelper;
import com.doubleparallax.metro.util.render.RenderHelper;
import com.doubleparallax.metro.util.render.font.DFont;
import com.doubleparallax.metro.util.render.font.FontHelper;
import com.doubleparallax.metro.util.render.geometry.Rect;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.List;

public class Alert implements TimeHelper.ITimer {
    
    public enum Type {
        SUCCESS, INFO, WARNING, SEVERE
    }
    
    private static final DFont FONT = FontHelper.Fonts.CALIBRI_14.getFont();
    private static final double PADDING = 10;
    private static final double PADDING2 = PADDING * 2;
    
    private List<Animation> animations = new ArrayList<>();
    private Type type;
    private String label;
    private Animation x = new Animation().addTo(animations),
            y = new Animation().addTo(animations),
            w = new Animation().addTo(animations),
            h = new Animation().addTo(animations),
            open = new Animation(true).addTo(animations),
            hover = new Animation().addTo(animations);
    private DeltaTime dtime = new DeltaTime();
    private long life = 5000;
    
    public Alert(Type type, String label) {
        this.type = type;
        this.label = label = StringHelper.wrapString(FONT, label, Display.getWidth() - PADDING2);
        w.reset(FONT.getWidth(label) + PADDING2);
        h.reset(FONT.getHeight(label) + PADDING2);
        x.reset(Display.getWidth() - w.getValue());
        y.reset(Display.getHeight() - h.getValue());
        dtime.reset();
    }
    
    public Animation getX() {
        return x;
    }
    
    public Animation getY() {
        return y;
    }
    
    public Animation getW() {
        return w;
    }
    
    public Animation getH() {
        return h;
    }
    
    public boolean isOpen() {
        return open.getValueB();
    }
    
    @Override
    public void _step() {
        animations.forEach(Animation::_step);
        if (life != -1 && dtime.is(life)) {
            dtime.reset();
            open.setValue(false);
        }
        if (open.getValue() < 0.1 && !open.getValueB())
            AlertHandler.remove(this);
        double margin = 24;
        hover.setValue(MathHelper.isPointInside(InputHelper.getX(), InputHelper.getY(), x.getValue() - margin, y.getValue() - margin, x.getValue() + w.getValue() + margin, y.getValue() + h.getValue() + margin));
    }
    
    public void render(double alpha) {
        GlStateManager.pushMatrix();
        GlStateManager.translate(x.getValue(), y.getValue(), 0);

        double transparency = open.getValue() - (hover.getValue() / 1.15);
        int color = -1;
        switch (type) {
            case SUCCESS:
                color = 0xA096DF73;
                break;
            case INFO:
                color = 0xA0ABDAE2;
                break;
            case WARNING:
                color = 0xA0FED557;
                break;
            case SEVERE:
                color = 0xA0EF403D;
                break;
        }

        RenderHelper.preRender();
        Rect.draw_Xy2_Col(0, 0, w.getValue(), h.getValue(), 0xA0202020, alpha * transparency);
        Rect.draw_Xy2_Col_Mod(0, 0, w.getValue(), h.getValue(), 0xA0202020, GL11.GL_LINE_LOOP, alpha * transparency);
        FONT.drawString(label, PADDING, PADDING, ColorHelper.relativeAlpha(-1, transparency));
        RenderHelper.postRender();
        
        GlStateManager.popMatrix();
    }
}
