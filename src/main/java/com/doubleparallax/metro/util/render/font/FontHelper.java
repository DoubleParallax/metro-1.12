package com.doubleparallax.metro.util.render.font;

public class FontHelper {
    
    public enum Fonts {
        ICON_14("icon", 14, false, true),
        ICON_24("icon", 24, false, true),
        ICON_32("icon", 32, false, true),
        ICON_48("icon", 48, false, true),
        
        CALIBRI_14("calibri", 14, true, false),
        CALIBRI_16("calibri", 16, true, false, 0, 4),
        CALIBRI_24("calibri", 24, true, false),
        CALIBRI_32("calibri", 32, true, false),
        CALIBRI_48("calibri", 48, true, false);
        
        private DFont font;
        
        Fonts(String font, int size, boolean bold, boolean custom) {
            this(font, size, bold, custom, 0, 0);
        }
        
        Fonts(String font, int size, boolean bold, boolean custom, double offsetX, double offsetY) {
            this.font = DFont.build(font, size, bold, custom).setOffset(offsetX, offsetY);
        }
        
        public DFont getFont() {
            return font;
        }
    }
}
