package com.doubleparallax.metro.util.render;

import net.minecraft.client.renderer.GlStateManager;

import java.util.Stack;

import static org.lwjgl.opengl.GL11.*;

public class ClippingHelper {
    
    public static class Scissor {
        public static void start(int x1, int y1, int x2, int y2) {
            glEnable(GL_SCISSOR_TEST);
            glScissor(x1, y1, x2 - x1, y2 - y1);
        }
        
        public static void end() {
            glDisable(GL_SCISSOR_TEST);
        }
    }
    
    public static class Depth {
        private static final Stack<Integer> flags = new Stack<>();
        
        public static void start() {
            if (flags.isEmpty()) {
                GlStateManager.clearDepth(1.);
                GlStateManager.clear(GL_DEPTH_BUFFER_BIT);
            }
        }
        
        public static void mask() {
            flags.push(glGetInteger(GL_DEPTH_FUNC));
            GlStateManager.enableDepth();
            GlStateManager.depthMask(true);
            GlStateManager.depthFunc(GL_LESS);
            GlStateManager.colorMask(false, false, false, false);
        }
        
        public static void render() {
            render(GL_EQUAL);
        }
        
        public static void render(int flag) {
            GlStateManager.depthFunc(flag);
            GlStateManager.colorMask(true, true, true, true);
        }
        
        public static void end() {
            GlStateManager.depthFunc(flags.pop());
        }
    }
    
    public static class Stencil {
        public static void write() {
            glClearStencil(0);
            glClear(GL_STENCIL_BUFFER_BIT);
            glEnable(GL_STENCIL_TEST);
            
            glStencilFunc(GL_ALWAYS, 1, 0xFFFF);
            glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
            GlStateManager.colorMask(false, false, false, false);
        }
        
        public static void erase() {
            glStencilFunc(GL_NOTEQUAL, 1, 0xFFFF);
            glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
            GlStateManager.colorMask(true, true, true, true);
        }
        
        public static void dispose() {
            glDisable(GL_STENCIL_TEST);
        }
    }
    
}
