package com.doubleparallax.metro.util.render.font;

import com.doubleparallax.metro.Client;
import com.doubleparallax.metro.util.Helper;
import com.doubleparallax.metro.util.io.FileHelper;
import com.doubleparallax.metro.util.render.ColorHelper;
import com.doubleparallax.metro.util.render.RenderHelper;
import com.doubleparallax.metro.util.render.geometry.Rect;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mojang.realmsclient.gui.ChatFormatting;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.util.ResourceLocation;
import org.apache.commons.lang3.StringUtils;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static java.awt.RenderingHints.*;

public class DFont implements Helper {
    
    public enum Horizontal {
        LEFT, CENTER, RIGHT;
        
        public double getAlign(double x, double w) {
            return x + (w * (this == CENTER ? .5 : this == RIGHT ? 1 : 0));
        }
    }
    
    public enum Vertical {
        TOP, MIDDLE, BOTTOM;
        
        public double getAlign(double x, double w) {
            return x + (w * (this == MIDDLE ? .5 : this == BOTTOM ? 1 : 0));
        }
    }
    
    private static final Font DEFAULT = new Font("Arial", Font.PLAIN, 12);
    private static final int[] COLORS = new int[32];
    private static final String COLOR_CODES = "0123456789abcdefklmnor";
    
    private static final HashBasedTable<String, Integer, DFont> FONT_CACHE = HashBasedTable.create();
    
    static {
        for (int i = 0; i < COLORS.length; i++) {
            int v = (i >> 3 & 1) * 85;
            int r = (i >> 2 & 1) * 170 + v;
            int g = (i >> 1 & 1) * 170 + v;
            int b = (i & 1) * 170 + v;
            if (i == 6) {
                r += 85;
            }
            if (i >= 16) {
                r /= 4;
                g /= 4;
                b /= 4;
            }
            
            COLORS[i] = (r & 255) << 16 | (g & 255) << 8 | b & 255 | 255 << 24;
        }
    }
    
    public static DFont build(String name, int size, boolean bold) {
        return build(name, size, bold, false);
    }
    
    public static DFont build(String name, int size, boolean bold, boolean custom) {
        if (FONT_CACHE.contains(name, size))
            return FONT_CACHE.get(name, size);
        Font font = DEFAULT;
        String asset = FileHelper.procRes("font/%s", name);
        if (custom)
            try {
                font = Font.createFont(Font.TRUETYPE_FONT, DFont.class.getResourceAsStream(asset + ".ttf")).deriveFont(bold ? Font.BOLD : Font.PLAIN, size);
            } catch (FontFormatException | IOException e) {
            }
        else
            font = new Font(name, bold ? Font.BOLD : Font.PLAIN, size);
        
        GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
        GraphicsConfiguration graphicsConfiguration = graphicsDevice.getDefaultConfiguration();
        Graphics2D graphics = graphicsConfiguration.createCompatibleImage(1, 1, Transparency.TRANSLUCENT).createGraphics();
        graphics.setFont(font);
        
        FontMetrics fontMetrics = graphics.getFontMetrics();
        
        List<String> lines = Lists.newArrayList();
        
        int i = 0;
        StringBuilder builder = new StringBuilder();
        for (int j = 0; j < 256; j++) {
            int index = (256 * i) + j;
            builder.append("  ").append((char) index).append("  ");
            if (fontMetrics.stringWidth(builder.toString() + String.valueOf((char) index)) >= 1024) {
                lines.add(builder.toString());
                lines.add(" ");
                builder = new StringBuilder();
            }
        }
        lines.add(builder.toString());
        
        float charHeight = fontMetrics.getMaxAscent() + fontMetrics.getMaxDescent();
        float textureWidth = 0;
        float textureHeight = lines.size() * charHeight;
        
        for (String line : lines)
            textureWidth = Math.max(textureWidth, fontMetrics.stringWidth(line));
        
        Map<Character, Glyph> glyphMap = Maps.newHashMap();
        
        BufferedImage bufferedImage = graphics.getDeviceConfiguration().createCompatibleImage((int) textureWidth, (int) textureHeight, Transparency.TRANSLUCENT);
        
        Graphics2D imageGraphics = (Graphics2D) bufferedImage.getGraphics();
        imageGraphics.setFont(font);
        imageGraphics.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_OFF);
        imageGraphics.setRenderingHint(KEY_TEXT_ANTIALIASING, VALUE_TEXT_ANTIALIAS_ON);
        imageGraphics.setRenderingHint(KEY_RENDERING, VALUE_RENDER_QUALITY);
        imageGraphics.setColor(Color.WHITE);
        
        for (String line : lines) {
            imageGraphics.drawString(line, 0, fontMetrics.getMaxAscent() + (charHeight * lines.indexOf(line)));
            for (char character : line.toCharArray())
                glyphMap.put(character, new Glyph(character, fontMetrics, charHeight, lines, textureWidth, textureHeight));
        }
        
        DynamicTexture dynamicTexture = new DynamicTexture(bufferedImage);
        DFont DFont = new DFont(mc.getTextureManager().getDynamicTextureLocation(Client.INSTANCE.PROJECT.toLowerCase() + "/fonts/" + name + "." + size, dynamicTexture), charHeight, glyphMap, fontMetrics);
        
        FONT_CACHE.put(font.getName(), size, DFont);
        
        return DFont;
    }
    
    private final Rect RECT = new Rect();
    private ResourceLocation resource;
    private float charHeight;
    private Map<Character, Glyph> glyphMap = Maps.newHashMap();
    private FontMetrics fontMetrics;
    private Random random = new Random();
    private double offsetX, offsetY;
    
    private DFont(ResourceLocation resource, float charHeight, Map<Character, Glyph> glyphMap, FontMetrics fontMetrics) {
        this.resource = resource;
        this.charHeight = charHeight;
        this.glyphMap = glyphMap;
        this.fontMetrics = fontMetrics;
    }
    
    public DFont setOffset(double offsetX, double offsetY) {
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        return this;
    }
    
    public int getWidth(String text) {
        text = ChatFormatting.stripFormatting(text);
        int width = 0;
        String[] split = text.split("\n");
        for (String s : split)
            width = Math.max(width, fontMetrics.stringWidth(s));
        return width;
    }
    
    public double getHeight() {
        return charHeight;
    }
    
    public double getHeight(Object text) {
        text = ChatFormatting.stripFormatting(text.toString());
        return fontMetrics.getStringBounds(text.toString(), null).getHeight() * (StringUtils.countMatches(text.toString(), "\n") + 1);
    }
    
    public void drawString(Object obj, double x, double y, int color) {
        drawString(obj, x, y, color, false);
    }
    
    public void drawString(Object obj, double x, double y, int color, boolean shadow) {
        drawString(obj, x, y, color, shadow, Horizontal.LEFT, Vertical.TOP);
    }
    
    public void drawString(Object obj, double x, double y, int color, boolean shadow, Horizontal horAlign, Vertical verAlign) {
        String text = obj == null ? "null" : obj.toString();
        
        if (text.contains("\n")) {
            String[] lines = text.split("\n");
            double _h = getHeight(text) - 16;
            double _y = y - (_h / 2.);
            for (String line : lines) {
                drawString(line, x, _y, color, shadow, horAlign, verAlign);
                _y+= getHeight(line);
            }
            return;
        }
        
        double offsetX = this.offsetX;
        double offsetY = this.offsetY;
        
        double width = getWidth(text);
        double height = getHeight(text);
        
        offsetX += horAlign == Horizontal.CENTER ? -width / 2. : horAlign == Horizontal.RIGHT ? -width : 0;
        offsetY += verAlign == Vertical.MIDDLE ? (-height / 2.) - 2 : verAlign == Vertical.BOTTOM ? -height : 0;
        
        float[] rgb = ColorHelper.colorToRgbaF(color);
        
        GlStateManager.pushMatrix();
        GlStateManager.translate(Math.round(x + offsetX), Math.round(offsetY), 0);
        
        RenderHelper.preRender();
        
        boolean magic, bold, strike, underline, italic;
        magic = bold = strike = underline = italic = false;
        int fontColor = 15;
        
        double _x = 0;
        char[] chars = text.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if (c == '\n') {
                _x = 0;
                y += charHeight;
                continue;
            }
            if (c == '\247' && i < chars.length - 1) {
                int index = COLOR_CODES.indexOf(chars[i + 1]);
                if (index >= 0 && index <= 21) {
                    switch (index) {
                        case 16:
                            magic = true;
                            break;
                        case 17:
                            bold = true;
                            break;
                        case 18:
                            strike = true;
                            break;
                        case 19:
                            underline = true;
                            break;
                        case 20:
                            italic = true;
                            break;
                        case 21:
                            magic = bold = strike = underline = italic = false;
                            fontColor = 15;
                            break;
                        default:
                            magic = bold = strike = underline = italic = false;
                            fontColor = index;
                            break;
                    }
                    int _color = COLORS[fontColor];
                    float a = rgb[3];
                    rgb = ColorHelper.colorToRgbaF(_color);
                    rgb[3] = a;
                    i++;
                    continue;
                }
            }
            Glyph g = glyphMap.get(c);
            boolean square = false;
            if (g == null) {
                g = glyphMap.get('A');
                square = true;
            }
            if (g != null) {
                Glyph o = g;
                if (magic && !square) {
                    List<Glyph> list = Lists.newArrayList();
                    list.addAll(glyphMap.values());
                    int a;
                    do {
                        a = random.nextInt(glyphMap.size());
                    }
                    while (list.get(a).getW() != g.getW());
                    g = list.get(a);
                }
                for (int j = shadow ? 1 : 0; j >= 0; j--) {
                    for (int k = 0; k < (bold ? 2 : 1); k++) {
                        GlStateManager.pushMatrix();
                        int renderColor = ColorHelper.rgbaFToColor(rgb);
                        if (j == 1) {
                            renderColor = COLORS[fontColor + 16];
                            float[] _renderColor = ColorHelper.colorToRgbaF(renderColor);
                            _renderColor[3] = rgb[3];
                            renderColor = ColorHelper.blend(ColorHelper.rgbaFToColor(_renderColor), ColorHelper.absoluteAlpha(0xFF000000, rgb[3]), 0.5f);
                        }
                        GlStateManager.translate(j + k, j, 0);
                        float w = g.getW();
                        float h = g.getH();
                        float tw = g.getTw();
                        float th = g.getTh();
                        float tx = g.getTx();
                        float ty = g.getTy();
                        float italicSlant = (italic ? 4 : 0);
                        if (square) {
                            int a = 1;
                            Rect.draw_Xy4_Col_Mod(_x + italicSlant + a, y + a, _x + w + italicSlant - a, y + a, _x + w - a, y + h - a, _x + a, y + h - a, renderColor, GL11.GL_LINE_LOOP, 1);
                        } else
                            Rect.draw_Res_Xy4_Tex2_Col(resource, _x + italicSlant, y, _x + w + italicSlant, y, _x + w, y + h, _x, y + h, tx, ty, tx + tw, ty + th, renderColor, 1);
                        if (strike)
                            Rect.draw_Xy2_Col(_x, y + (h / 2.0) - 1, _x + w, y + (h / 2.0) + 1, renderColor, 1);
                        if (underline)
                            Rect.draw_Xy2_Col(_x, y + (h * 0.85) - 2, _x + w, y + (h * 0.85), renderColor, 1);
                        GlStateManager.popMatrix();
                    }
                }
                _x += o.getW();
            }
        }
        RenderHelper.postRender();
        GlStateManager.popMatrix();
    }
    
}
