package com.doubleparallax.metro.util.render.geometry;

import com.doubleparallax.metro.util.Helper;
import com.doubleparallax.metro.util.render.ColorHelper;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import org.lwjgl.opengl.GL11;

public class Donut implements Helper {
    
    private static final float[] D_CLA = {0, 360};
    private static final float D_PRE = 360;
    private static final int D_COL = -1;
    private static final int D_MOD = GL11.GL_TRIANGLE_FAN;
    
    public static void draw_Xyr_Cla_Pre_Col_Mod(double x, double y, double rOuter, double rInner, double start, double end, double precision, int color, int mode, double alpha) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder buffer = tessellator.getBuffer();
        
        float[] rgba = ColorHelper.colorToRgbaF(ColorHelper.relativeAlpha(color, alpha));
        double prec = 360. / precision;
        double s = Math.min(start, end);
        double e = Math.max(start, end);
        
        GlStateManager.enableAlpha();
        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
        RenderHelper.disableStandardItemLighting();
        GlStateManager.disableTexture2D();
        
        int shade = GL11.glGetInteger(GL11.GL_SHADE_MODEL);
        GlStateManager.shadeModel(GL11.GL_SMOOTH);
        
        buffer.begin(mode, DefaultVertexFormats.POSITION_COLOR);
        if (mode == GL11.GL_LINE_LOOP || mode == GL11.GL_LINE_STRIP) {
            for (double i = s; i <= e; i += prec) {
                double rad = Math.toRadians(i);
                double _x = x + (Math.cos(rad) * (rInner / 2.));
                double _y = y + (Math.sin(rad) * (rInner / 2.));
                buffer.pos(_x, _y, 0).color(rgba[0], rgba[1], rgba[2], rgba[3]).endVertex();
            }
            tessellator.draw();
            
            buffer.begin(mode, DefaultVertexFormats.POSITION_COLOR);
            for (double i = e; i >= s; i -= prec) {
                double rad = Math.toRadians(i);
                double _x = x + (Math.cos(rad) * (rOuter / 2.));
                double _y = y + (Math.sin(rad) * (rOuter / 2.));
                buffer.pos(_x, _y, 0).color(rgba[0], rgba[1], rgba[2], rgba[3]).endVertex();
            }
        } else
            for (double i = s; i <= e; i += prec) {
                double rad = Math.toRadians(i);
                double _x = x + (Math.cos(rad) * (rOuter / 2.));
                double _y = y + (Math.sin(rad) * (rOuter / 2.));
                buffer.pos(_x, _y, 0).color(rgba[0], rgba[1], rgba[2], rgba[3]).endVertex();
                
                _x = x + (Math.cos(rad) * (rInner / 2.));
                _y = y + (Math.sin(rad) * (rInner / 2.));
                buffer.pos(_x, _y, 0).color(rgba[0], rgba[1], rgba[2], rgba[3]).endVertex();
            }
        tessellator.draw();
        
        GlStateManager.shadeModel(shade);
        
        RenderHelper.enableStandardItemLighting();
    }
    
    public static void draw_Xyr(double x, double y, double rOuter, double rInner, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, D_CLA[0], D_CLA[1], D_PRE, D_COL, D_MOD, alpha);
    }
    
    public static void draw_Xyr_Cla(double x, double y, double rOuter, double rInner, double start, double end, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, start, end, D_PRE, D_COL, D_MOD, alpha);
    }
    
    public static void draw_Xyr_Cla_Pre(double x, double y, double rOuter, double rInner, double start, double end, double precision, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, start, end, precision, D_COL, D_MOD, alpha);
    }
    
    public static void draw_Xyr_Cla_Pre_Col(double x, double y, double rOuter, double rInner, double start, double end, double precision, int color, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, start, end, precision, color, D_MOD, alpha);
    }
    
    public static void draw_Xyr_Cla_Pre_Mod(double x, double y, double rOuter, double rInner, double start, double end, double precision, int mode, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, start, end, precision, D_COL, mode, alpha);
    }
    
    public static void draw_Xyr_Cla_Col(double x, double y, double rOuter, double rInner, double start, double end, int color, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, start, end, D_PRE, color, D_MOD, alpha);
    }
    
    public static void draw_Xyr_Cla_Col_Mod(double x, double y, double rOuter, double rInner, double start, double end, int color, int mode, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, start, end, D_PRE, color, mode, alpha);
    }
    
    public static void draw_Xyr_Cla_Mod(double x, double y, double rOuter, double rInner, double start, double end, int mode, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, start, end, D_PRE, D_COL, mode, alpha);
    }
    
    public static void draw_Xyr_Pre(double x, double y, double rOuter, double rInner, double precision, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, D_CLA[0], D_CLA[1], precision, D_COL, D_MOD, alpha);
    }
    
    public static void draw_Xyr_Pre_Col(double x, double y, double rOuter, double rInner, double precision, int color, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, D_CLA[0], D_CLA[1], precision, color, D_MOD, alpha);
    }
    
    public static void draw_Xyr_Pre_Col_Mod(double x, double y, double rOuter, double rInner, double precision, int color, int mode, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, D_CLA[0], D_CLA[1], precision, color, mode, alpha);
    }
    
    public static void draw_Xyr_Pre_Mod(double x, double y, double rOuter, double rInner, double precision, int mode, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, D_CLA[0], D_CLA[1], precision, D_COL, mode, alpha);
    }
    
    public static void draw_Xyr_Col(double x, double y, double rOuter, double rInner, int color, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, D_CLA[0], D_CLA[1], D_PRE, color, D_MOD, alpha);
    }
    
    public static void draw_Xyr_Col_Mod(double x, double y, double rOuter, double rInner, int color, int mode, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, D_CLA[0], D_CLA[1], D_PRE, color, mode, alpha);
    }
    
    public static void draw_Xyr_Mod(double x, double y, double rOuter, double rInner, int mode, double alpha) {
        draw_Xyr_Cla_Pre_Col_Mod(x, y, rOuter, rInner, D_CLA[0], D_CLA[1], D_PRE, D_COL, mode, alpha);
    }
}
