package com.doubleparallax.metro.util.render;

import net.minecraft.util.math.MathHelper;

import java.awt.*;

public class ColorHelper {
    
    public static float[] colorToRgbaF(int color) {
        return new float[] {
                ((color >> 16) & 255) / 255f,
                ((color >> 8) & 255) / 255f,
                (color & 255) / 255f,
                ((color >> 24) & 255) / 255f
        };
    }
    
    public static int rgbaFToColor(float[] color) {
        return ((int) (color[0] * 255) << 16) | ((int) (color[1] * 255) << 8) | ((int) (color[2] * 255)) | ((int) (color[3] * 255) << 24);
    }
    
    public static int fromHue(int hue) {
        return fromHue(hue != 0 ? hue / 360.f : hue);
    }
    
    public static int fromHueSat(int hue, int sat) {
        return fromHueSat(hue != 0 ? hue / 100.f : hue, sat != 0 ? sat / 100.f : sat);
    }
    
    public static int fromHueSatBri(int hue, int sat, int bri) {
        return Color.HSBtoRGB(hue != 0 ? hue / 360.f : hue, sat != 0 ? sat / 100.f : sat, bri != 0 ? bri / 100.f : bri);
    }
    
    public static int fromHue(float hue) {
        return fromHueSat(hue, 1.f);
    }
    
    public static int fromHueSat(float hue, float sat) {
        return fromHueSatBri(hue, sat, 1.f);
    }
    
    public static int fromHueSatBri(float hue, float sat, float bri) {
        while (hue > 1)
            hue -= 1;
        while (hue < 0)
            hue += 1;
        return Color.HSBtoRGB(hue, MathHelper.clamp(sat, 0, 1), MathHelper.clamp(bri, 0, 1));
    }
    
    public static int theme(int hue) {
        return theme(hue / 360.f);
    }
    
    public static int theme(float hue) {
        return fromHueSatBri(hue, .42f, .8f);
    }
    
    public static int darken(int color, int amount) {
        Color c = new Color(color);
        for (int i = 0; i < amount; i++)
            c = c.darker();
        return c.getRGB();
    }
    
    public static int brighter(int color, int amount) {
        Color c = new Color(color);
        for (int i = 0; i < amount; i++)
            c = c.brighter();
        return c.getRGB();
    }
    
    public static float getRainbowHue(float offset) {
        return ((System.nanoTime() / 50000000.f) + offset) / 1000.f;
    }
    
    public static int absoluteAlpha(int color, double newAlpha) {
        return (color & 0x00FFFFFF) | ((int) (MathHelper.clamp(newAlpha, 0, 1) * 255) << 24);
    }
    
    public static int relativeAlpha(int color, double newAlpha) {
        return absoluteAlpha(color, MathHelper.clamp(newAlpha * ((color >> 24 & 255) / 255.), 0, 1));
    }
    
    public static int blend(int ca, int cb, float am) {
        float ar = (ca >> 16 & 255) / 255f;
        float ag = (ca >> 8 & 255) / 255f;
        float ab = (ca & 255) / 255f;
        float aa = (ca >> 24 & 255) / 255f;
        
        float br = (cb >> 16 & 255) / 255f;
        float bg = (cb >> 8 & 255) / 255f;
        float bb = (cb & 255) / 255f;
        float ba = (cb >> 24 & 255) / 255f;
        
        am = MathHelper.clamp(am, 0, 1);
        float am2 = 1 - am;
        
        float r = (ar * am2) + (br * am);
        float g = (ag * am2) + (bg * am);
        float b = (ab * am2) + (bb * am);
        float a = (aa * am2) + (ba * am);
        
        return (((int) (r * 255) & 255) << 16) | (((int) (g * 255) & 255) << 8) | (((int) (b * 255) & 255)) | (((int) (a * 255) & 255) << 24);
    }
    
    public static int additive(int ca, int cb) {
        float ar = (ca >> 16 & 255) / 255f;
        float ag = (ca >> 8 & 255) / 255f;
        float ab = (ca & 255) / 255f;
        float aa = (ca >> 24 & 255) / 255f;
        
        float br = (cb >> 16 & 255) / 255f;
        float bg = (cb >> 8 & 255) / 255f;
        float bb = (cb & 255) / 255f;
        float ba = (cb >> 24 & 255) / 255f;
        
        return (((int) (ar * br) & 255) << 16) | (((int) (ag * bg) & 255) << 8) | ((int) (ab * bb) & 255) | (((int) (aa * ba) & 255) << 24);
    }
    
}
