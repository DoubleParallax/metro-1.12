package com.doubleparallax.metro.util.render.alert;

public class AlertHelper {
    
    private static void send(Alert.Type type, String message, Object... args) {
        if (message == null)
            return;
        send(type, String.format(message, args));
    }
    
    private static void send(Alert.Type type, String label) {
        AlertHandler.add(new Alert(type, label));
    }
    
    public static void success(String message, Object... args) {
        send(Alert.Type.SUCCESS, message, args);
    }
    
    public static void info(String message, Object... args) {
        send(Alert.Type.INFO, message, args);
    }
    
    public static void warning(String message, Object... args) {
        send(Alert.Type.WARNING, message, args);
    }
    
    public static void severe(String message, Object... args) {
        send(Alert.Type.SEVERE, message, args);
    }
    
}
