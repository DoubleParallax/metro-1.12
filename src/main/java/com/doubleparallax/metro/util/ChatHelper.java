package com.doubleparallax.metro.util;

import com.doubleparallax.metro.Client;
import net.minecraft.util.text.ITextComponent;

public class ChatHelper implements Helper {
    
    private static final String PREFIX = String.format("\247u[\247v%s\247u]\247t", Client.INSTANCE.PROJECT);
    
    public static void sendComponent(ITextComponent component) {
        if (component != null && mc.ingameGUI != null)
            mc.ingameGUI.getChatGUI().printChatMessage(component);
    }
    
    public static void sendMessageRaw(Object message) {
        if (message != null)
            sendComponent(StringHelper.format(message.toString()));
    }
    
    public static void sendMessage(Object message) {
        sendMessageRaw(String.format("%s: %s", PREFIX, message));
    }
    
    public static void sendMessageRaw(String message, boolean preFormat, Object... args) {
        sendMessageRaw(String.format(message, preFormat(preFormat, args)));
    }
    
    public static void sendMessage(String message, boolean preFormat, Object... args) {
        sendMessage(String.format(message, preFormat(preFormat, args)));
    }
    
    private static Object[] preFormat(boolean preFormat, Object... args) {
        if (!preFormat)
            return args;
        Object[] alternateArgs = new Object[args.length];
        for (int i = 0; i < args.length; i++)
            alternateArgs[i] = String.format("\247v%s\247t", args[i]);
        return alternateArgs;
    }
    
}
