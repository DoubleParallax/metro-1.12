package com.doubleparallax.metro.util.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ClickType;

public class InventoryInteraction {
    
    private int window, slot, button;
    private ClickType type;
    private EntityPlayer player;
    
    public InventoryInteraction(int window, int slot, int button, ClickType type, EntityPlayer player) {
        this.window = window;
        this.slot = slot;
        this.button = button;
        this.type = type;
        this.player = player;
    }
    
    public int getWindow() {
        return window;
    }
    
    public int getSlot() {
        return slot;
    }
    
    public int getButton() {
        return button;
    }
    
    public ClickType getType() {
        return type;
    }
    
    public EntityPlayer getPlayer() {
        return player;
    }
    
}
