package com.doubleparallax.metro.util.inventory;

import com.doubleparallax.metro.util.Helper;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Enchantments;
import net.minecraft.init.MobEffects;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.*;
import net.minecraft.potion.PotionType;
import net.minecraft.potion.PotionUtils;
import net.minecraft.util.math.BlockPos;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Predicate;

public class InventoryHelper implements Helper {
    
    public static Object inUse;
    
    public static void clickSlot(Object object, int window, int slot, int button, ClickType type, EntityPlayer player) {
        inUse = object;
        mc.playerController.windowClick(window, slot, button, type, player);
    }
    
    public static Slot getBestFitSlot(Container container, Predicate<Slot> predicate, Comparator<Slot> comparator) {
        return getBestFitSlot(container, 0, container.inventorySlots.size(), predicate, comparator);
    }
    
    public static Slot getBestFitSlot(Container container, int min, int max, Predicate<Slot> predicate, Comparator<Slot> comparator) {
        return container.inventorySlots.stream().filter(s -> s.slotNumber >= min && s.slotNumber < max && predicate.test(s)).sorted(comparator).findFirst().orElse(null);
    }
    
    public static Slot getBestArmorSlot(Container container, Predicate<Slot> predicate, Comparator<Slot> comparator) {
        return getBestArmorSlot(container, 0, container.inventorySlots.size(), predicate, comparator);
    }
    
    public static Slot getBestArmorSlot(Container container, int min, int max, Predicate<Slot> predicate, Comparator<Slot> comparator) {
        return getBestFitSlot(container, min, max, s -> {
            return s.getHasStack() &&
                    s.getStack().getItem() instanceof ItemArmor &&
                    predicate.test(s);
        }, comparator);
    }
    
    public static Slot getPotionSlot(Container container, PotionType... potions) {
        return getPotionSlot(container, 0, container.inventorySlots.size(), potions);
    }
    
    public static Slot getPotionSlot(Container container, int min, int max, PotionType... potions) {
        return getBestFitSlot(container, min, max, s -> {
            return s.getHasStack() &&
                    s.getStack().getItem() instanceof ItemSplashPotion &&
                    Arrays.asList(potions).contains(PotionUtils.getPotionFromItem(s.getStack()));
        }, (s1, s2) -> Boolean.compare(s1.getHasStack(), s2.getHasStack()));
    }
    
    public static Slot getToolSlot(EntityPlayer player, Container container, int min, int max, BlockPos pos) {
        IBlockState state = mc.world.getBlockState(pos);
        Slot handSlot = container.getSlot(36 + player.inventory.currentItem);
        float digSpeed = handSlot.getHasStack() ? getDigSpeed(player, state, handSlot.getStack()) : 1;
        return getBestFitSlot(container, min, max, s -> {
            return s.getHasStack() &&
                    s.getStack().getItem() instanceof ItemTool &&
                    getDigSpeed(player, state, s.getStack()) > digSpeed;
        }, (s1, s2) -> Float.compare(getDigSpeed(player, state, s2.getStack()), getDigSpeed(player, state, s1.getStack())));
    }

    public static Slot getFoodSlot(EntityPlayer player, Container container, int min, int max) {
        return getBestFitSlot(container, min, max, s -> {
            return s.getHasStack() &&
                    s.getStack().getItem() instanceof ItemFood;
        }, (s1, s2) -> Boolean.compare(s1.getHasStack(), s2.getHasStack()));
    }
    
    public static Slot getNextEmptySlot(Container container) {
        return getNextEmptySlot(container, 0, container.inventorySlots.size());
    }
    
    public static Slot getNextEmptySlot(Container container, int min, int max) {
        return container.inventorySlots.stream().filter(s -> s.slotNumber >= min && s.slotNumber < max && !s.getHasStack()).findFirst().orElse(null);
    }
    
    private static float getDigSpeed(EntityPlayer player, IBlockState state, ItemStack stack) {
        float f = getStrVsBlock(state, stack);
        if (f > 1.0F)
            f += Math.pow(getEfficiencyModifier(stack), 2) + 1;
        if (player.isPotionActive(MobEffects.HASTE))
            f *= 1.0F + (float) (player.getActivePotionEffect(MobEffects.HASTE).getAmplifier() + 1) * 0.2F;
        if (player.isPotionActive(MobEffects.MINING_FATIGUE)) {
            float f1;
            switch (player.getActivePotionEffect(MobEffects.MINING_FATIGUE).getAmplifier()) {
                case 0:
                    f1 = 0.3F;
                    break;
                case 1:
                    f1 = 0.09F;
                    break;
                case 2:
                    f1 = 0.0027F;
                    break;
                case 3:
                default:
                    f1 = 8.1E-4F;
            }
            f *= f1;
        }
        
        if (player.isInsideOfMaterial(Material.WATER) && !EnchantmentHelper.getAquaAffinityModifier(player))
            f /= 5.0F;
        if (!player.onGround)
            f /= 5.0F;
        return f;
    }
    
    private static float getStrVsBlock(IBlockState state, ItemStack stack) {
        return stack == null ? 1 : (stack.getStrVsBlock(state));
    }
    
    private static float getEfficiencyModifier(ItemStack stack) {
        return EnchantmentHelper.getEnchantmentLevel(Enchantments.EFFICIENCY, stack);
    }
}
