package com.doubleparallax.metro.util;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class URLHelper {

    public static boolean openBrowser(String url) {
        if (Desktop.isDesktopSupported())
            try {
                Desktop.getDesktop().browse(new URI(url));
                return true;
            } catch (IOException | URISyntaxException e) {
            }
        return false;
    }

}
