package com.doubleparallax.metro.util;

import com.doubleparallax.metro.util.render.font.DFont;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class StringHelper {
    
    private enum Format {
        BLACK(TextFormatting.BLACK, '0', true),
        DARK_BLUE(TextFormatting.DARK_BLUE, '1', true),
        DARK_GREEN(TextFormatting.DARK_GREEN, '2', true),
        DARK_AQUA(TextFormatting.DARK_AQUA, '3', true),
        DARK_RED(TextFormatting.DARK_RED, '4', true),
        DARK_PURPLE(TextFormatting.DARK_PURPLE, '5', true),
        GOLD(TextFormatting.GOLD, '6', true),
        GRAY(TextFormatting.GRAY, '7', true),
        DARK_GRAY(TextFormatting.DARK_GRAY, '8', true),
        BLUE(TextFormatting.BLUE, '9', true),
        GREEN(TextFormatting.GREEN, 'a', true),
        AQUA(TextFormatting.AQUA, 'b', true),
        RED(TextFormatting.RED, 'c', true),
        LIGHT_PURPLE(TextFormatting.LIGHT_PURPLE, 'd', true),
        YELLOW(TextFormatting.YELLOW, 'e', true),
        WHITE(TextFormatting.WHITE, 'f', true),
        OBFUSCATED(TextFormatting.OBFUSCATED, 'k', false),
        BOLD(TextFormatting.BOLD, 'l', false),
        STRIKETHROUGH(TextFormatting.STRIKETHROUGH, 'm', false),
        UNDERLINE(TextFormatting.UNDERLINE, 'n', false),
        ITALIC(TextFormatting.ITALIC, 'o', false),
        RESET(TextFormatting.RESET, 'r', false),
        
        CUSTOM_TEXT(TextFormatting.GRAY, 't', true),
        CUSTOM_BRACE(TextFormatting.DARK_GRAY, 'u', true),
        CUSTOM_VALUE(TextFormatting.RED, 'v', true),
        CUSTOM_ERROR(TextFormatting.DARK_RED, 'w', true);
        
        TextFormatting format;
        char code;
        boolean isColor;
        
        Format(TextFormatting format, char code, boolean isColor) {
            this.format = format;
            this.code = code;
            this.isColor = isColor;
        }
        
        public void appendStyle(Style style) {
            if (isColor) {
                style.setColor(format);
            } else {
                switch (this) {
                    case OBFUSCATED:
                        style.setObfuscated(true);
                        break;
                    case BOLD:
                        style.setBold(true);
                        break;
                    case STRIKETHROUGH:
                        style.setStrikethrough(true);
                        break;
                    case UNDERLINE:
                        style.setUnderlined(true);
                        break;
                    case ITALIC:
                        style.setItalic(true);
                        break;
                    case RESET:
                        style.setColor(WHITE.format).setObfuscated(false).setBold(false).setStrikethrough(false).setUnderlined(false).setItalic(false);
                        break;
                }
            }
        }
        
        public static Format fromCode(char code) {
            for (Format format : values()) {
                if (format.code == code) {
                    return format;
                }
            }
            return RESET;
        }
    }
    
    private static final String WITH_DELIMITER = "(?=[%s])";
    
    public static ITextComponent format(String message) {
        message = "\247r" + message;
        String[] split = message.split(String.format(WITH_DELIMITER, "\247"));
        ITextComponent parent = new TextComponentString("");
        
        Style style = new Style().setColor(TextFormatting.RESET);
        for (String s : split) {
            if (s.startsWith("\247") && s.length() > 1) {
                char code = s.charAt(1);
                
                style = style.createDeepCopy();
                Format format = Format.fromCode(code);
                format.appendStyle(style);
            }
            
            ITextComponent sibling = new TextComponentString(s.substring(2)).setStyle(style);
            parent.appendSibling(sibling);
        }
        return parent;
    }
    
    public static String wrapString(DFont font, String base, double width) {
        if (base == null)
            return null;
        StringBuilder wrapped = new StringBuilder();
        String[] split = base.split(" ");
        StringBuilder line = new StringBuilder();
        for (int i = 0; i < split.length; i++) {
            if (font.getWidth(line + split[i]) >= width) {
                wrapped.append(line).append("\n");
                line = new StringBuilder();
            }
            line.append(split[i]).append(i == split.length - 1 ? "" : " ");
        }
        wrapped.append(line);
        return wrapped.toString();
    }
    
    public static String trim(DFont font, String base, double width) {
        return base;
    }
    
    public static String insert(String base, String add, int index) {
        index = MathHelper.clamp(index, 0, base.length());
        return String.format("%s%s%s", base.substring(0, index), add, base.substring(index));
    }
    
    public static String replace(String base, String add, int start, int end) {
        start = MathHelper.clamp(start, 0, base.length());
        end = MathHelper.clamp(end, 0, base.length());
        return String.format("%s%s%s", base.substring(0, Math.min(start, end)), add, base.substring(Math.max(start, end)));
    }
    
    public static String remove(String base, int start, int end) {
        return replace(base, "", start, end);
    }
    
}
