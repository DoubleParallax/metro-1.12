package com.doubleparallax.metro.util;

import java.util.HashMap;
import java.util.Map;

public class TimeHelper {
    
    private static final Map<ITimer, Thread> TIMERS = new HashMap<>();
    
    public static void register(ITimer timer) {
        TIMERS.put(timer, run(timer));
    }
    
    private static Thread run(ITimer timer) {
        Thread thread = new Thread(() -> runThread(timer));
        thread.start();
        return thread;
    }
    
    private static void runThread(ITimer timer) {
        while (true) {
            long nano = System.nanoTime();
            
            timer._step();
            
            long dif = (1000000000 / 60) - (System.nanoTime() - nano);
            if (dif > 0) {
                try {
                    Thread.sleep(dif / 1000000);
                } catch (InterruptedException e) {
                }
            }
        }
    }
    
    public interface ITimer {
        default void _step() {}
    }
    
}
