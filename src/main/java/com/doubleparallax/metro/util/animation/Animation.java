package com.doubleparallax.metro.util.animation;

import com.doubleparallax.metro.property.PropertyHandler;
import com.doubleparallax.metro.property.impl.Settings;
import com.doubleparallax.metro.util.MathHelper;
import com.doubleparallax.metro.util.TimeHelper;

import java.util.List;

public class Animation implements TimeHelper.ITimer {
    
    public enum Type {
        LINEAR {
            @Override
            public void interpolate(Animation animation) {
                double inc = Math.min(1, animation.increment / 10.);
                animation.interpolated += animation.interpolated > animation.value + inc ? -inc : animation.interpolated < animation.value - inc ? inc : animation.value - animation.interpolated;
            }
        },
        PARABOLIC {
            @Override
            public void interpolate(Animation animation) {
                animation.interpolated += (animation.isAngular() ? MathHelper.wrapAngle(animation.value, animation.interpolated, animation.wrap) : animation.value - animation.interpolated) / Math.max(animation.increment, 1);
            }
        };
        
        void interpolate(Animation animation) {}
    }
    
    private double value, interpolated, increment, wrap;
    private boolean angular;
    private Type type;
    
    public Animation() {
        this(0);
    }
    
    public Animation(boolean value) {
        this(MathHelper.convertFrom(value));
    }
    
    public Animation(boolean value, double increment) {
        this(MathHelper.convertFrom(value), increment);
    }
    
    public Animation(boolean value, double increment, Type type) {
        this(MathHelper.convertFrom(value), increment, type);
    }
    
    public Animation(boolean value, double interpolated, double increment, Type type) {
        this(MathHelper.convertFrom(value), interpolated, increment, type);
    }
    
    public Animation(double value) {
        this(value, PropertyHandler.get(Settings.class).animationSpeed);
    }
    
    public Animation(double value, double increment) {
        this(value, increment, Type.PARABOLIC);
    }
    
    public Animation(double value, double increment, Type type) {
        this(value, value, increment, type);
    }
    
    public Animation(double value, double interpolated, double increment, Type type) {
        this.value = value;
        this.interpolated = interpolated;
        this.increment = increment;
        this.type = type;
    }

    public boolean getValueB() {
        return value == 1;
    }
    
    public double getValue() {
        return interpolated;
    }

    public double getAbsoluteValue() {
        return value;
    }
    
    /*public double getRoundedInterpolation() {
        return value > interpolated ? Math.ceil(interpolated) : Math.floor(interpolated);
    }
    
    public double getInterpolated() {
        return interpolated;
    }*/
    
    public double getIncrement() {
        return getIncrement();
    }
    
    public double getWrap() {
        return wrap;
    }
    
    public boolean isAngular() {
        return angular;
    }
    
    public Animation setValue(boolean value) {
        return setValue(MathHelper.convertFrom(value));
    }
    
    public Animation setValue(double value) {
        this.value = value;
        return this;
    }
    
    public Animation setInterpolated(double interpolated) {
        this.interpolated = interpolated;
        return this;
    }
    
    public Animation setIncrement(double increment) {
        this.increment = increment;
        return this;
    }
    
    public Animation setWrap(double wrap) {
        this.wrap = wrap;
        return this;
    }
    
    public Animation setAngular(boolean angular) {
        this.angular = angular;
        return this;
    }
    
    public Animation setType(Type type) {
        this.type = type;
        return this;
    }
    
    public Animation reset(double value) {
        this.value = interpolated = value;
        return this;
    }
    
    public Animation addTo(List<Animation> list) {
        list.add(this);
        return this;
    }
    
    @Override
    public void _step() {
        type.interpolate(this);
    }
    
}
