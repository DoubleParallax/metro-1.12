package com.doubleparallax.metro.util;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

public class InputHelper {
    
    public static int getX() {
        return Mouse.getX();
    }
    
    public static int getY() {
        return Display.getHeight() - Mouse.getY() - 1;
    }
    
    public static boolean isMouseInside(double x1, double y1, double x2, double y2) {
        return MathHelper.isPointInside(getX(), getY(), x1, y1, x2, y2);
    }
    
}
