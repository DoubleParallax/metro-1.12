package com.doubleparallax.metro.util;

import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class ItemHelper {
    
    private static final Map<ItemArmor.ArmorMaterial, Integer> ARMOR_VALUES = new HashMap<ItemArmor.ArmorMaterial, Integer>() {
        {put(ItemArmor.ArmorMaterial.LEATHER, 100);}
        
        {put(ItemArmor.ArmorMaterial.GOLD, 100);}
        
        {put(ItemArmor.ArmorMaterial.CHAIN, 1000);}
        
        {put(ItemArmor.ArmorMaterial.IRON, 2000);}
        
        {put(ItemArmor.ArmorMaterial.DIAMOND, 3000);}
    };
    
    private static final Map<Integer, Integer> ENCHANT_VALUES = new HashMap<Integer, Integer>() {
        {put(0, 100);} //protection
        
        {put(1, 75);} //fire_protection
        
        {put(2, 80);} //feather_falling
        
        {put(3, 50);} //blast_protection
        
        {put(4, 60);} //projectile_protection
        
        {put(5, 40);} //respiration
        
        {put(6, 40);} //aqua_affinity
        
        {put(7, 30);} //thorns
        
        {put(8, 50);} //depth_strider
        
        {put(9, 50);} //frost_walker
        
        {put(10, 0);} //binding_curse
        
        {put(16, 100);} //sharpness
        
        {put(17, 50);} //smite
        
        {put(18, 50);} //bane_of_arthropods
        
        {put(19, 30);} //knockback
        
        {put(20, 80);} //fire_aspect
        
        {put(21, 60);} //looting
        
        {put(22, 70);} //sweeping
        
        {put(32, 50);} //efficiency
        
        {put(33, 0);} //silk_touch
        
        {put(34, 40);} //unbreaking
        
        {put(35, 30);} //fortune
        
        {put(48, 60);} //power
        
        {put(49, 30);} //punch
        
        {put(50, 60);} //flame
        
        {put(51, 40);} //infinity
        
        {put(61, 0);} //luck_of_the_sea
        
        {put(62, 0);} //lure
        
        {put(70, 10);} //mending
        
        {put(71, 0);} //vanishing_curse
    };
    
    public static int getArmorValue(ItemStack stack) {
        if (stack != null && stack.getItem() instanceof ItemArmor) {
            ItemArmor item = (ItemArmor) stack.getItem();
            ItemArmor.ArmorMaterial material = item.getArmorMaterial();
            int materialValue = ARMOR_VALUES.get(material);
            for (int i = 0; i < stack.getEnchantmentTagList().tagCount(); i++) {
                int level = stack.getEnchantmentTagList().getCompoundTagAt(i).getInteger("lvl");
                int id = stack.getEnchantmentTagList().getCompoundTagAt(i).getInteger("id");
                materialValue += ENCHANT_VALUES.get(id) * level;
            }
            return materialValue;
        }
        return 0;
    }
    
}
