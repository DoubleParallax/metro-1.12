package com.doubleparallax.metro.util;

import com.doubleparallax.metro.Client;
import com.doubleparallax.metro.util.io.FileHelper;
import com.doubleparallax.metro.util.io.Request;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

public class UpdateHelper {
    
    public static void doUpdate() {
        Thread thread = new Thread(() -> {
            try {
                Version version = getVersion();
                if (version != null)
                    prepareUpdate(version);
            } catch (IOException ignored) {
            }
        });
        thread.setDaemon(true);
        thread.start();
    }
    
    private static Version getVersion() throws IOException {
        String update = new Request("https://gist.github.com/DoubleParallax/3a5dcd215d5894bf0e51c6e5f2368199", Request.Method.POST).request();
        if (update != null) {
            Document doc = Jsoup.parse(update);
            Elements tables = doc.getElementsByTag("table");
            if (tables.isEmpty())
                return null;
            
            Version version = JsonHelper.OBJECT_MAPPER.readValue(tables.get(0).text(), Version.class);
            return MathHelper.isLarger(version.getVersion(), Client.INSTANCE.VERSION) ? version : null;
        }
        return null;
    }
    
    private static void prepareUpdate(Version version) {
        
        Runtime.getRuntime().addShutdownHook(new Thread(() -> FileHelper.downloadFile(version.getFile(), version.getFile())));
    }
    
    public static class Version {
        private byte[] version;
        private String file;
        
        public byte[] getVersion() {
            return version;
        }
        
        public String getFile() {
            return file;
        }
        
        public void setVersion(byte[] version) {
            this.version = version;
        }
        
        public void setFile(String file) {
            this.file = file;
        }
    }
    
}
