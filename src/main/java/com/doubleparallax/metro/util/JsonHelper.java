package com.doubleparallax.metro.util;

import com.doubleparallax.metro.util.io.FileHelper;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

public class JsonHelper {
    
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().enable(MapperFeature.PROPAGATE_TRANSIENT_MARKER).disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    public static final ObjectWriter OBJECT_WRITER = OBJECT_MAPPER.writerWithDefaultPrettyPrinter();
    
    public static String compileJson(String json, String directory) {
        if (json == null)
            return null;
        String[] imports;
        while ((imports = StringUtils.substringsBetween(json, "//[", "]//")) != null) {
            for (String i : imports) {
                if (i.startsWith("@import")) {
                    String[] split = i.split(";");
                    if (split.length >= 2) {
                        String imported = FileHelper.readResource(FileHelper.procRes("%s/%s.json", directory, split[1]));
                        if (imported == null) {
                            System.err.printf("Failed to import %s\r\n", split[1]);
                            return null;
                        }
                        for (int j = 2; j < split.length; j++)
                            imported = imported.replaceAll("\"@data:" + (j - 2) + "\"", split[j]);
                        json = json.replaceAll("//\\[" + Pattern.quote(i) + "]//", imported.substring(1, imported.length() - 1));
                    }
                }
            }
        }
        return json;
    }
    
}
