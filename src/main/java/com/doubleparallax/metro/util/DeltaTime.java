package com.doubleparallax.metro.util;

import net.minecraft.client.Minecraft;

public class DeltaTime {
    
    private long time;
    
    public DeltaTime() {
        this(0);
    }
    
    public DeltaTime(long offset) {
        reset(offset);
    }
    
    public void reset() {
        reset(0);
    }
    
    public void reset(long offset) {
        time = Minecraft.getSystemTime() + offset;
    }
    
    public boolean is(long time) {
        return Minecraft.getSystemTime() - this.time >= time;
    }
    
    public long get() {
        return Minecraft.getSystemTime() - time;
    }
    
    
}
