package com.doubleparallax.metro.util.io;

import com.doubleparallax.metro.Client;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class FileHelper {
    
    public enum DisplayIcon {
        METRO(1, 2, 2.5f, 3, 4, 8, 16);
        
        private float[] sizes;
        
        DisplayIcon(float... sizes) {
            this.sizes = sizes;
        }
        
        public float[] getSizes() {
            return sizes;
        }
    }
    
    public static String procMinecraftRes(String path, String... args) {
        return String.format("%s/%s", Client.INSTANCE.PROJECT.toLowerCase(),
                String.format(path.toLowerCase(), (Object[]) args));
    }
    
    public static String procRes(String path, String... args) {
        return String.format("/assets/minecraft/%s", procMinecraftRes(path, args));
    }
    
    public static String procExternalRes(String path, String... args) {
        return String.format("./%s", procMinecraftRes(path, args));
    }
    
    public static String readResource(String path) {
        InputStream stream = FileHelper.class.getResourceAsStream(path);
        return stream == null ? null : readBuffer(new InputStreamReader(stream));
    }
    
    public static String readFile(String path) throws FileNotFoundException {
        return readBuffer(new FileReader(path));
    }
    
    public static void writeFile(String path, String content) throws IOException {
        if (procFile(path))
            Files.write(Paths.get(path), content.getBytes());
    }
    
    public static boolean procFile(String path) throws IOException {
        File file = new File(path);
        if (file.exists())
            return true;
        else if (file.isDirectory())
            return file.mkdirs();
        else if (file.getParentFile() != null && file.getParentFile().exists())
            return file.createNewFile();
        else
            return file.getParentFile().mkdirs() && file.createNewFile();
    }
    
    public static ByteBuffer[] procIcon(DisplayIcon icon) throws IOException {
        float[] sizes = icon.sizes;
        int count = sizes.length;
        
        ByteBuffer[] buffer = new ByteBuffer[count];
        for (int i = 0; i < count; i++) {
            InputStream stream = FileHelper.class.getResourceAsStream(procRes(String.format("icon/%s/%s.png", icon, sizes[i])));
            if (stream != null)
                buffer[i] = readImage(stream);
        }
        return buffer;
    }
    
    public static boolean downloadFile(String addr, String path) {
        try {
            URL url = new URL(addr);
            ReadableByteChannel channel = Channels.newChannel(url.openStream());
            FileOutputStream output = new FileOutputStream(path);
            output.getChannel().transferFrom(channel, 0, Long.MAX_VALUE);
            output.flush();
            output.close();
            channel.close();
            return true;
        } catch (IOException ignored) {
        }
        return false;
    }
    
    public static void downloadFileAsync(String addr, String path, Consumer<Boolean> callback) {
        Thread thread = new Thread(() -> callback.accept(downloadFile(addr, path)));
        thread.setDaemon(true);
        thread.start();
    }
    
    private static String readBuffer(InputStreamReader reader) {
        return reader == null ? null : new BufferedReader(reader).lines().collect(Collectors.joining("\n"));
    }
    
    private static ByteBuffer readImage(InputStream stream) throws IOException {
        BufferedImage image = ImageIO.read(stream);
        int[] rgb = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
        ByteBuffer buffer = ByteBuffer.allocate(4 * rgb.length);
        for (int i : rgb)
            buffer.putInt(i << 8 | i >> 24 & 255);
        return (ByteBuffer) buffer.flip();
    }
    
}
