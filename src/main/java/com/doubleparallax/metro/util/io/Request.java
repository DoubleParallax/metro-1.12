package com.doubleparallax.metro.util.io;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class Request {
    
    public enum Method {
        GET("GET"), POST("POST");
        
        private String name;
        
        Method(String name) {
            this.name = name;
        }
        
        @Override
        public String toString() {
            return name;
        }
    }
    
    public enum Header {
        USER_AGENT("User-Agent"),
        ACCEPT("Accept"),
        CONTENT_TYPE("Content-Type");
        
        private String key;
        
        Header(String key) {
            this.key = key;
        }
        
        @Override
        public String toString() {
            return key;
        }
    }
    
    public enum AltDispenserApi {
        VALIDATE("validate"),
        GENERATE("generate"),
        REDEEM("redeem_key");
        
        String url;
        
        AltDispenserApi(String suffix) {
            url = String.format(ALT_DISPENSER_API, suffix);
        }
        
        public String getUrl() {
            return url;
        }
        
        public enum Mojang {
            VALIDATE("validate"),
            REFRESH("refresh"),
            JOINSERVER("joinserver");
            
            String url;
            
            Mojang(String suffix) {
                url = String.format(ALT_DISPENSER_API, String.format("mojang/%s", suffix));
            }
            
            public String getUrl() {
                return url;
            }
        }
    }
    
    public static final String DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13";
    private static final String ALT_DISPENSER_API = "https://api.altdispenser.net/client/%s";
    
    private String url, payload;
    private Method method;
    private Map<String, String> headers = new HashMap<String, String>() {{
        put(Header.USER_AGENT.toString(), DEFAULT_USER_AGENT);
    }};
    private Map<String, String> params = new HashMap<>();
    
    public Request(String url, Method method) {
        this.url = url;
        this.method = method;
    }
    
    public Request setPayload(String payload) {
        this.payload = payload;
        return this;
    }
    
    public Request setHeader(Header key, String value) {
        headers.put(key.toString(), value);
        return this;
    }
    
    public Request setParam(String key, String value) {
        params.put(key, value);
        return this;
    }
    
    private String getPayload() {
        if (payload != null)
            return payload;
        
        StringBuilder payload = new StringBuilder();
        params.forEach((k, v) -> payload.append(String.format("%s=%s&", k, v)));
        return payload.toString();
    }
    
    private String readStream(HttpURLConnection connection) throws IOException {
        InputStream stream = connection.getResponseCode() == 200 ? connection.getInputStream() : connection.getErrorStream();
        if (stream == null)
            throw new IOException("Failed to get connection stream");
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null)
            builder.append(String.format("%s\n", line));
        reader.close();
        return builder.toString();
    }
    
    public String request() {
        try {
            String payload = getPayload();
            URL url = new URL(this.url + (method == Method.GET ? (payload.isEmpty() ? "" : String.format("?%s", payload)) : ""));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            
            connection.setRequestMethod(method.toString());
            headers.forEach(connection::setRequestProperty);
            
            switch (method) {
                case POST:
                    connection.setDoInput(true);
                    connection.setDoOutput(true);
                    
                    DataOutputStream output = new DataOutputStream(connection.getOutputStream());
                    output.writeBytes(payload);
                    output.flush();
                    output.close();
                    break;
            }
            
            if (connection.getResponseCode() == 204)
                return null;
            
            return readStream(connection);
        } catch (IOException ignored) {
        }
        return null;
    }
}
