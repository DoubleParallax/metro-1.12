package com.doubleparallax.metro.util;

import org.apache.commons.lang3.SystemUtils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

public class SystemHelper {
    
    private enum OperatingSystem {
        LINUX, WINDOWS
    }
    
    private static final String OS, HWID;
    
    static {
        OS = System.getProperty("os.name");
        HWID = getHWID();
    }
    
    public static String getOS() {
        return OS;
    }
    
    public static String getHWID() {
        return HWID != null ? HWID : getHWID(SystemUtils.IS_OS_WINDOWS ? OperatingSystem.WINDOWS : OperatingSystem.LINUX);
    }
    
    private static String getHWID(OperatingSystem os) {
        byte[] bytes = null;
        switch (os) {
            case LINUX:
                try {
                    Enumeration<NetworkInterface> nis = NetworkInterface.getNetworkInterfaces();
                    while (nis.hasMoreElements()) {
                        NetworkInterface ni = nis.nextElement();
                        if (ni != null) {
                            bytes = ni.getHardwareAddress();
                            break;
                        }
                    }
                } catch (SocketException e) {
                }
                break;
            case WINDOWS:
                try {
                    NetworkInterface ni = NetworkInterface.getByInetAddress(InetAddress.getLocalHost());
                    bytes = ni.getHardwareAddress();
                } catch (UnknownHostException | SocketException e) {
                }
                break;
        }
        return buildMAC(bytes);
    }
    
    private static String buildMAC(byte[] bytes) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < bytes.length; i++)
            builder.append(String.format("%02X%s", bytes[i], (i < bytes.length - 1) ? "-" : ""));
        return builder.toString();
    }
    
}
